package com.angcyo.canvas2.laser.pecker.engrave.newflow

import androidx.lifecycle.Observer
import com.angcyo.canvas.render.renderer.BaseRenderer
import com.angcyo.canvas.render.renderer.CanvasElementRenderer
import com.angcyo.canvas.render.renderer.CanvasGroupRenderer
import com.angcyo.canvas2.laser.pecker.util.lpElementBean
import com.angcyo.http.rx.doBack
import com.angcyo.laserpacker.device.EngraveHelper
import com.angcyo.library.annotation.CallPoint
import com.angcyo.library.component.TaskManager
import com.angcyo.library.ex.nowTime
import com.angcyo.library.ex.toListOf
import com.angcyo.library.ex.uuid
import com.angcyo.viewmodel.updateThis
import com.angcyo.viewmodel.updateValue
import com.angcyo.viewmodel.vmData

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/18
 *
 * 雕刻流程控制
 */
class FlowEngraveControl {

    /**雕刻状态通知, 所有数据的状态通知, 包含传输/雕刻*/
    var engraveStateData = vmData(EngraveState())

    /**需要雕刻元素*/
    var engraveRendererList: List<BaseRenderer>? = null

    /**雕刻任务管理
     * [FlowEngraveTask]*/
    val engraveTaskManger = TaskManager()

    /**具体元素的雕刻数据状态提供*/
    val engraveDataStateProvider = EngraveDataStateProvider()

    /**雕刻任务监听*/
    val engraveTaskListener = object : TaskManager.TaskListener() {
        override fun onTaskStart(task: TaskManager.ITask) {
            engraveStateData.value?.error = null
            engraveStateData.value?.state = EngraveState.ENGRAVE_STATE_ENGRAVING
            engraveStateData.updateThis()
        }

        override fun onTaskEnd(task: TaskManager.ITask) {
            engraveStateData.value?.error = null
            engraveStateData.value?.state = EngraveState.ENGRAVE_STATE_ENGRAVING
            engraveStateData.updateThis()
        }

        override fun onTaskError(task: TaskManager.ITask, error: Throwable) {
            engraveStateData.value?.error = error
            engraveStateData.value?.state = EngraveState.ENGRAVE_STATE_ERROR
            engraveStateData.updateThis()
        }

        override fun onTaskAllEnd() {
            engraveStateData.value?.error = null
            engraveStateData.value?.state = EngraveState.ENGRAVE_STATE_FINISH
            engraveStateData.updateThis()
        }
    }

    /**雕刻任务状态监听*/
    val engraveTaskObserver: Observer<EngraveState?> = Observer { engraveState ->
        if (engraveState != null) {
            (engraveTaskManger.currentTask as? FlowEngraveTask)?.let {
                engraveDataStateProvider.updateState(it.elementUuid) {
                    state = engraveState.state
                    progress = engraveState.progress
                    error = engraveState.error
                }
                if (engraveState.state != EngraveState.ENGRAVE_STATE_FINISH) {
                    //单个完成状态, 不在此通知
                    engraveStateData.value?.state = engraveState.state
                }
                engraveStateData.value?.error = engraveState.error
                engraveStateData.updateThis()
            }
        }
    }

    /**初始化*/
    @CallPoint
    fun initControl(rendererList: List<BaseRenderer>?) {
        this.engraveRendererList = rendererList
        engraveTaskManger.taskListener.add(engraveTaskListener)

        if (rendererList.isNullOrEmpty()) {
            engraveStateData.updateValue(EngraveState(EngraveState.ENGRAVE_STATE_FINISH))
        } else {
            rendererList.forEach { renderer ->
                addEngraveTask(renderer)
            }
            engraveStateData.updateValue(EngraveState(EngraveState.ENGRAVE_STATE_NORMAL))
        }
    }

    fun addEngraveTask(renderer: BaseRenderer) {
        if (renderer is CanvasElementRenderer) {
            val elementBean = renderer.lpElementBean()
            if (elementBean != null) {
                val uuid = /*renderer.lpElementBean()?.uuid ?:*/ uuid()
                elementBean.uuid = uuid
                elementBean.index = EngraveHelper.generateEngraveIndex()

                engraveDataStateProvider.updateState(uuid) {
                    state = EngraveState.ENGRAVE_STATE_NORMAL
                }
                // task
                engraveTaskManger.addTask(FlowEngraveTask().apply {
                    initEngraveTask(renderer.toListOf())
                    engraveTaskStateData.observeForever(engraveTaskObserver)
                })
            }
        } else if (renderer is CanvasGroupRenderer) {
            renderer.rendererList.forEach {
                if (it.isVisible) {
                    addEngraveTask(it)
                }
            }
        }
    }

    var _isRelease = false

    /**释放资源*/
    @CallPoint
    fun release() {
        cancelEngraveTask()
        _isRelease = true
        engraveTaskManger.cancel()
        engraveTaskManger.taskList.forEach {
            if (it is FlowEngraveTask) {
                it.engraveTaskStateData.removeObserver(engraveTaskObserver)
                it.release()
            }
        }
        engraveDataStateProvider.release()
    }

    /**获取雕刻元素的雕刻状态*/
    fun getElementEngraveState(elementUuid: String?): EngraveState? {
        return engraveDataStateProvider.engraveStateMap[elementUuid]
    }

    //region ---雕刻相关---

    /**开始雕刻任务
     * [FlowEngraveTask]*/
    fun startEngraveTask(restart: Boolean = false) {
        engraveDataStateProvider.updateAllState(EngraveState.ENGRAVE_STATE_READY)
        doBack {
            if (restart) {
                engraveStateData.value?.startTime = nowTime()
                engraveTaskManger.taskList.forEach {
                    if (it is FlowEngraveTask) {
                        it.updateTaskId()
                    }
                }
            }
            engraveTaskManger.restartTask()
        }
    }

    /**重启当前的雕刻任务
     * [startEngraveTask]*/
    fun restartEngraveTask() {
        doBack {
            engraveTaskManger.startTask()
        }
    }

    /**暂停雕刻*/
    fun pauseEngraveTask() {
        (engraveTaskManger.currentTask as? FlowEngraveTask)?.pauseEngraveTask()
    }

    /**恢复雕刻*/
    fun resumeEngraveTask() {
        (engraveTaskManger.currentTask as? FlowEngraveTask)?.resumeEngraveTask()
    }

    /**取消雕刻*/
    fun cancelEngraveTask() {
        (engraveTaskManger.currentTask as? FlowEngraveTask)?.cancelEngraveTask()
        engraveStateData.value?.state = EngraveState.ENGRAVE_STATE_CANCEL
        engraveStateData.value?.endTime = nowTime()
        //清空所有状态到正常
        engraveDataStateProvider.updateAllState(EngraveState.ENGRAVE_STATE_NORMAL)
        //notify
        engraveStateData.updateThis()
    }

    //endregion ---雕刻相关---
}

