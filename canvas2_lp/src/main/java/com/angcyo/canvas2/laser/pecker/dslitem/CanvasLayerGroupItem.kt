package com.angcyo.canvas2.laser.pecker.dslitem

import com.angcyo.canvas.render.renderer.CanvasGroupRenderer
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.library.ex.size
import com.angcyo.widget.DslViewHolder

/**
 * 图层item, group
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024-4-16
 */
open class CanvasLayerGroupItem : CanvasLayerItem() {

    /**是否是组合元素*/
    val isGroupRenderer get() = itemRenderer is CanvasGroupRenderer

    /**子元素的数量*/
    val childCount get() = (itemRenderer as? CanvasGroupRenderer)?.rendererList.size()

    /**是否是半选状态*/
    var itemIsHalfChecked = false

    init {
        itemShowEngraveParams = false

        itemClick = {

        }

        //长按重命名
        /*itemLongClick = { view ->
            true
        }*/
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        if (!itemIsHalfChecked) {
            itemHolder.img(R.id.lib_check_view)?.setImageResource(R.drawable.canvas_check_selector)
        }
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)
        //半选状态支持
        if (itemIsHalfChecked) {
            itemHolder.img(R.id.lib_check_view)?.setImageResource(R.drawable.canvas_half_checked)
        }
    }

}