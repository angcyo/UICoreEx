package com.angcyo.canvas2.laser.pecker.dialog.dslitem

import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.dialog.ParameterComparisonTableDialogConfig
import com.angcyo.canvas2.laser.pecker.dialog.updateTablePreview
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.base.clearListeners
import com.angcyo.widget.base.onTextChange
import com.angcyo.widget.base.setInputText

/**
 * 指定材质名称
 * [com.angcyo.canvas2.laser.pecker.dialog.ParameterComparisonTableDialogConfig.parseParameterComparisonTable]
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024-8-15
 */
class PCTMaterialTextItem : DslAdapterItem() {

    init {
        itemLayoutId = R.layout.item_pct_material_text_layout
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)
        itemHolder.ev(R.id.lib_edit_view)?.apply {
            clearListeners()
            setInputText(ParameterComparisonTableDialogConfig.materialText, false)
            onTextChange {
                ParameterComparisonTableDialogConfig.materialText = "$it"
                itemDslAdapter.updateTablePreview()
            }
        }
    }
}