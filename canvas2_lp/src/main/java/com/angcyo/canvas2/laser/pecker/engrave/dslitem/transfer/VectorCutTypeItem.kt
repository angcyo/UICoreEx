package com.angcyo.canvas2.laser.pecker.engrave.dslitem.transfer

import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerModel
import com.angcyo.bluetooth.fsc.laserpacker._deviceSettingBean
import com.angcyo.bluetooth.fsc.laserpacker.bean.matchesProductVersion
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.EngraveSegmentScrollItem
import com.angcyo.core.vmApp
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.item.style.itemCurrentIndex
import com.angcyo.laserpacker.bean.LPLaserOptionsBean
import com.angcyo.library.ex._string
import com.angcyo.library.ex.clamp
import com.angcyo.tablayout.DslTabLayout
import com.angcyo.widget.DslViewHolder

/**
 * 数据分辨率选择, Dpi
 *
 * [com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper.PX_1K]
 * [com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper.PX_1_3K]
 * [com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper.PX_2K]
 * [com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper.PX_4K]
 *
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2022/07/01
 */
class VectorCutTypeItem : EngraveSegmentScrollItem() {

    companion object {
        /**未开放切割时使用*/
        val only_line = listOf("line")
        val lp_valueList = listOf("line", "metalCut")
        val lx_valueList = listOf("line", "lineCut", "metalCut")
    }

    var valueList = listOf<String>()

    /**数据配置信息*/
    var itemLaserOptionsBean: LPLaserOptionsBean? = null
        set(value) {
            field = value
            itemCurrentIndex =
                clamp(value?.cutType?.let { valueList.indexOf(it) } ?: 0, 0, valueList.lastIndex)
            value?.cutType = valueList[itemCurrentIndex]
        }

    init {
        itemText = _string(R.string.engrave_type)
        if (vmApp<LaserPeckerModel>().isC1() || vmApp<LaserPeckerModel>().isL5()) {
            valueList = lx_valueList
            itemSegmentList = listOf(
                _string(R.string.engrave_layer_line),
                _string(R.string.engrave_layer_cut),
                _string(R.string.metal_cut),
            )
        } else {
            if (_deviceSettingBean?.showMetalCutRange.matchesProductVersion()) {
                valueList = lp_valueList
                itemSegmentList =
                    listOf(_string(R.string.engrave_layer_line), _string(R.string.metal_cut))
            } else {
                valueList = only_line
                itemSegmentList = listOf(_string(R.string.engrave_layer_line))
            }
        }
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)
        itemHolder.v<DslTabLayout>(tabLayoutItemConfig.itemTabLayoutViewId)?.apply {
            itemEquWidthCountRange = null
        }
    }

    override fun onItemChangeListener(item: DslAdapterItem) {
        //super.onItemChangeListener(item)
        itemLaserOptionsBean?.cutType = valueList[itemCurrentIndex]
    }
}

class ImageReliefTypeItem : EngraveSegmentScrollItem() {

    /**对应的值列表
     * [itemSegmentList]*/
    var itemValueList = listOf(LaserPeckerHelper.RELIEF_NONE, LaserPeckerHelper.RELIEF_GRAY_SLICE)

    /**数据配置信息*/
    var itemLaserOptionsBean: LPLaserOptionsBean? = null
        set(value) {
            field = value
            itemCurrentIndex = value?.reliefType?.let { itemValueList.indexOf(it) } ?: 0
            value?.reliefType = itemValueList[itemCurrentIndex]
        }

    init {
        itemText = _string(R.string.engrave_type)
        itemSegmentList =
            listOf(_string(R.string.engrave_layer_bitmap), _string(R.string.slice_relief))
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)
        itemHolder.v<DslTabLayout>(tabLayoutItemConfig.itemTabLayoutViewId)?.apply {
            itemEquWidthCountRange = null
        }
    }

    override fun onItemChangeListener(item: DslAdapterItem) {
        //super.onItemChangeListener(item)
        itemLaserOptionsBean?.reliefType = itemValueList[itemCurrentIndex]
    }
}

class DataTypeItem : EngraveSegmentScrollItem() {

    /**数据配置信息*/
    var itemLaserOptionsBean: LPLaserOptionsBean? = null
        set(value) {
            field = value
            itemCurrentIndex = 0
            if (value != null) {
                itemSegmentList = listOf(
                    when (value._layerId) {
                        LaserPeckerHelper.LAYER_PICTURE -> _string(R.string.engrave_layer_bitmap)
                        LaserPeckerHelper.LAYER_FILL -> _string(R.string.engrave_layer_fill)
                        else -> "${value._layerId}"
                    }
                )
            }
        }

    init {
        itemText = _string(R.string.engrave_type)
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)
        itemHolder.v<DslTabLayout>(tabLayoutItemConfig.itemTabLayoutViewId)?.apply {
            itemEquWidthCountRange = null
        }
    }

    override fun onItemChangeListener(item: DslAdapterItem) {
        //super.onItemChangeListener(item)
    }
}