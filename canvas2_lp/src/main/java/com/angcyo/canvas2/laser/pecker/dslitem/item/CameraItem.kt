package com.angcyo.canvas2.laser.pecker.dslitem.item

import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import com.angcyo.base.dslFHelper
import com.angcyo.bluetooth.fsc.CameraApiModel
import com.angcyo.bluetooth.fsc.laserpacker.DeviceStateModel
import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.bluetooth.fsc.laserpacker._deviceSettingBean
import com.angcyo.bluetooth.fsc.laserpacker.bean.matchesProductVersion
import com.angcyo.canvas.render.renderer.BaseRenderer
import com.angcyo.canvas.render.renderer.CanvasGroupRenderer
import com.angcyo.canvas2.laser.pecker.CameraPictureRenderer
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.dialog.cameraSmartFillHintDialogConfig
import com.angcyo.canvas2.laser.pecker.dslitem.CanvasIconItem
import com.angcyo.canvas2.laser.pecker.util.CameraHelper
import com.angcyo.canvas2.laser.pecker.util.LPRendererHelper
import com.angcyo.core.CoreApplication
import com.angcyo.core.tgStrokeLoading
import com.angcyo.core.vmApp
import com.angcyo.dialog.itemsDialog
import com.angcyo.dialog.messageDialog
import com.angcyo.laserpacker.LPDataConstant
import com.angcyo.laserpacker.bean.LPElementBean
import com.angcyo.laserpacker.device.AddCameraFragment
import com.angcyo.laserpacker.device.ble.DeviceSettingFragment
import com.angcyo.laserpacker.device.cameraSearchListDialog
import com.angcyo.laserpacker.device.cameraTemplateDialogConfig
import com.angcyo.laserpacker.toPaintStyleInt
import com.angcyo.library.annotation.Pixel
import com.angcyo.library.canvas.core.Reason
import com.angcyo.library.component.Strategy
import com.angcyo.library.component.hawk.HawkProperty
import com.angcyo.library.component.lastActivityCaller
import com.angcyo.library.component.onMainDelay
import com.angcyo.library.ex._color
import com.angcyo.library.ex._drawable
import com.angcyo.library.ex._string
import com.angcyo.library.ex.computePathBounds
import com.angcyo.library.ex.toHexColorString
import com.angcyo.library.ex.toPath
import com.angcyo.library.toast
import com.angcyo.library.toastQQ
import com.angcyo.library.unit.toMm
import com.angcyo.widget.span.span

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/05
 *
 * 拍照定位
 */
class CameraItem : CanvasIconItem() {

    val cameraApi = vmApp<CameraApiModel>()
    val deviceStateApi = vmApp<DeviceStateModel>()

    init {
        itemIco = R.drawable.canvas_camera_ico
        itemText = _string(R.string.canvas_camera)
        itemClick = { view ->
            if (!deviceStateApi.isDeviceConnect()) {
                //设备未连接
                toast(_string(R.string.camera_no_host_tip), icon = R.drawable.toast_warn)
            } else if (!cameraApi.isHostConnectCamera) {
                //主机未连接摄像头
                view.context.messageDialog {
                    val helpUrl = DeviceSettingFragment.getHelpUrl(
                        _deviceSettingBean?.cameraNoDeviceHelpUrl,
                        _deviceSettingBean?.cameraNoDeviceHelpUrlZh
                    )
                    dialogMessage = span {
                        append(_string(R.string.camera_no_device_tip))
                        if (helpUrl != null) {
                            append("\n\n")
                            append(_string(R.string.camera_no_device_help_tip)) {
                                foregroundColor = _color(R.color.lib_link)
                            }
                        }
                    }
                    if (helpUrl != null) {
                        dialogInitOverride = { dialog, dialogViewHolder ->
                            dialogViewHolder.click(R.id.dialog_message_view) {
                                CoreApplication.onOpenUrlAction?.invoke(helpUrl)
                            }
                        }
                    }
                }
            } else if (!cameraApi.isCameraConnectedNetwork) {
                //摄像头未联网, 则进行配网操作
                lastActivityCaller?.dslFHelper {
                    show(AddCameraFragment::class)
                }
            } else if (cameraApi.isSelectedCamera) {
                //选择了摄像头
                view.context.itemsDialog {
                    useStyle2 = true
                    addDialogIconItem {
                        itemIcon = _drawable(R.drawable.camera_template_ico)
                        itemText = _string(R.string.camera_template)
                        itemClick = {
                            view.context.cameraTemplateDialogConfig {
                                onTemplateConfirmAction = { template ->
                                    cameraApi.clearCameraTemplateCache()
                                    itemRenderDelegate?.renderManager?.let {
                                        val find =
                                            it.beforeRendererList.find { it is CameraPictureRenderer }
                                        if (find is CameraPictureRenderer) {
                                            find.cameraTemplateBean = template
                                            find.cameraPicture = cameraApiModel.cameraTemplateBitmap
                                            itemRenderDelegate?.refresh()
                                        }
                                    }
                                    //2024-11-9 模版选择后自动刷新
                                    if (cameraApiModel.cameraTemplateBitmap == null) {
                                        view.context.tgStrokeLoading { isCancel, loadEnd ->
                                            cameraApi.fetchCameraPicture { bitmap, error ->
                                                loadEnd(bitmap, error)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        itemTintRightIcon = false
                        itemRightIcon = _drawable(R.drawable.canvas_invert_help_svg)
                        itemRightIconClick = {
                            view.context.messageDialog {
                                val helpUrl = DeviceSettingFragment.getHelpUrl(
                                    _deviceSettingBean?.cameraTakePictureHelpUrl,
                                    _deviceSettingBean?.cameraTakePictureHelpUrlZh
                                )
                                dialogMessage = span {
                                    append(_string(R.string.camera_take_picture_tip))
                                    if (helpUrl != null) {
                                        append("\n\n")
                                        append(_string(R.string.camera_take_picture_help_tip)) {
                                            foregroundColor = _color(R.color.lib_link)
                                        }
                                    }
                                }
                                if (helpUrl != null) {
                                    dialogInitOverride = { dialog, dialogViewHolder ->
                                        dialogViewHolder.click(R.id.dialog_message_view) {
                                            CoreApplication.onOpenUrlAction?.invoke(helpUrl)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    addDialogIconItem {
                        itemIcon = _drawable(R.drawable.camera_refresh_ico)
                        itemText = _string(R.string.camera_refresh)
                        itemEnable = cameraApi.isSelectedCameraTemplate
                        itemDisabledColor = itemIcoDisableColor
                        itemClick = {
                            view.context.tgStrokeLoading { isCancel, loadEnd ->
                                cameraApi.fetchCameraPicture { bitmap, error ->
                                    loadEnd(bitmap, error)
                                }
                            }
                        }
                    }

                    val isRenderBitmap = CameraHelper.isRenderCameraBitmap(itemRenderDelegate)
                    val isHideBitmap =
                        !isRenderBitmap && cameraApi.isSelectedCameraTemplate && cameraApi._cameraTemplateBitmap != null
                    addDialogIconItem {
                        itemIcon =
                            if (isHideBitmap) _drawable(R.drawable.camera_show_background_ico) else _drawable(
                                R.drawable.camera_hide_background_ico
                            )
                        itemText =
                            if (isHideBitmap) _string(R.string.camera_show_background) else _string(
                                R.string.camera_hide_background
                            )
                        itemEnable =
                            cameraApi.isSelectedCameraTemplate && (CameraHelper.isRenderCameraBitmap(
                                itemRenderDelegate
                            ) || isHideBitmap)
                        itemDisabledColor = itemIcoDisableColor
                        itemClick = {
                            itemRenderDelegate?.renderManager?.let {
                                if (isHideBitmap) {
                                    it.addBeforeRendererList(CameraPictureRenderer().apply {
                                        cameraPicture = cameraApi.cameraTemplateBitmap
                                        cameraTemplateBean =
                                            cameraApi.selectedCameraTemplateData.value
                                    })
                                } else {
                                    it.beforeRendererList.removeAll { it is CameraPictureRenderer }
                                }
                                itemRenderDelegate?.refresh()
                            }
                        }
                    }
                    //智能填充
                    addDialogIconItem {
                        itemIcon = _drawable(R.drawable.camera_smart_fill_ico)
                        itemText = _string(R.string.camera_smart_fill)
                        itemEnable =
                            itemRenderDelegate?.selectorManager?.isSelectorElement == true && cameraApi.isSelectedCameraTemplate && CameraHelper.isRenderCameraBitmap(
                                itemRenderDelegate
                            )
                        itemDisabledColor = itemIcoDisableColor
                        itemClick = {
                            view.context.cameraSmartFillHintDialogConfig {
                                positiveButtonListener = { dialog, dialogViewHolder ->
                                    dialog.dismiss()
                                    //填充
                                    val resultSvgPath = CameraHelper.getCameraPicturePath(1)

                                    @Pixel val cameraTemplateBitmapRect =
                                        cameraApi.cameraTemplateBitmapRect

                                    if (resultSvgPath.isNullOrBlank()) {
                                        toastQQ("no data")
                                    } else if (itemRenderDelegate != null && cameraTemplateBitmapRect != null) {
                                        //L.d(svgPath)
                                        val lines = resultSvgPath.lines()
                                        val resultList = mutableListOf<BaseRenderer>()
                                        val rendererList =
                                            itemRenderDelegate!!.selectorManager.getSelectorRendererList(
                                                false
                                            )
                                        lines.forEach { svgPath ->
                                            val copyRendererList = LPRendererHelper.copyRenderer(
                                                itemRenderDelegate!!, rendererList, false, false
                                            )
                                            val renderer =
                                                CanvasGroupRenderer.fromIfNeed(copyRendererList)
                                            if (renderer != null) {
                                                val bounds = svgPath.toPath()?.computePathBounds()
                                                if (bounds != null) {
                                                    renderer.translateCenterTo(
                                                        bounds.centerX() + cameraTemplateBitmapRect.left,
                                                        bounds.centerY() + cameraTemplateBitmapRect.top,
                                                        Reason.code,
                                                        Strategy.preview,
                                                        null
                                                    )
                                                    resultList.add(renderer)
                                                }
                                            }
                                        }
                                        if (resultList.isNotEmpty()) {
                                            itemRenderDelegate!!.renderManager.replaceElementRenderer(
                                                rendererList,
                                                resultList,
                                                true,
                                                Reason.user,
                                                Strategy.normal
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //轮廓提取
                    addDialogIconItem {
                        itemIcon = _drawable(R.drawable.camera_outline_extract_ico)
                        itemText = _string(R.string.camera_outline_extract)
                        itemEnable =
                            cameraApi.isSelectedCameraTemplate && CameraHelper.isRenderCameraBitmap(
                                itemRenderDelegate
                            )
                        itemDisabledColor = itemIcoDisableColor
                        itemClick = {
                            val svgPath = CameraHelper.getCameraPicturePath(0)
                            if (svgPath.isNullOrBlank()) {
                                toastQQ("no data")
                            } else {
                                val lines = svgPath.lines()
                                val resultBeanList = mutableListOf<LPElementBean>()
                                lines.forEach { line ->
                                    val pathBounds =
                                        line.toPath()?.computePathBounds() ?: RectF(0f, 0f, 0f, 0f)
                                    val elementBean = LPElementBean().apply {
                                        mtype = LPDataConstant.DATA_TYPE_SVG
                                        data = line
                                        paintStyle = Paint.Style.STROKE.toPaintStyleInt()
                                        dataMode = LPDataConstant.DATA_MODE_GCODE
                                        left = (cameraApi.cameraTemplateBitmapRect?.left?.toMm()
                                            ?: 0f) + pathBounds.left.toMm()
                                        top = (cameraApi.cameraTemplateBitmapRect?.top?.toMm()
                                            ?: 0f) + pathBounds.top.toMm()

                                        isCut = vmApp<DeviceStateModel>().haveCutLayer()/*layerId = if (vmApp<DeviceStateModel>().haveCutLayer()) {
                                        LaserPeckerHelper.LAYER_CUT
                                    } else {
                                        LaserPeckerHelper.LAYER_LINE
                                    }*/
                                        //切割颜色
                                        stroke = Color.MAGENTA.toHexColorString()
                                    }
                                    resultBeanList.add(elementBean)
                                }

                                val svgRendererList = LPRendererHelper.parseElementRendererList(
                                    resultBeanList, false, false
                                )
                                if (svgRendererList.isNotEmpty()) {
                                    itemRenderDelegate?.renderManager?.addElementRenderer(
                                        svgRendererList, true, Reason.user, Strategy.normal
                                    )
                                }
                            }
                        }
                    }

                    if (_deviceSettingBean?.supportCameraDirectConnectionRange.matchesProductVersion()) {
                        //自动连接摄像头
                    } else {
                        addDialogIconItem {
                            itemIcon = _drawable(R.drawable.camera_list_ico)
                            itemText = _string(R.string.camera_list)
                            itemClick = {
                                view.context.cameraSearchListDialog {
                                    //no op
                                }
                            }
                        }
                    }
                }
            } else if (!_deviceSettingBean?.supportCameraDirectConnectionRange.matchesProductVersion()) {
                //不支持直连的主机
                view.context.cameraSearchListDialog {

                }
            }
        }

        //动态控制显示和隐藏
        itemHidden = !HawkEngraveKeys.enableCameraItem
        HawkProperty.hawkPropertyChangeActionList.add {
            if (it == HawkEngraveKeys::enableCameraItem.name) {
                itemHidden = !HawkEngraveKeys.enableCameraItem
                if (!itemHidden) {
                    onMainDelay {
                        itemDslAdapter?._recyclerView?.scrollToPosition(0)
                    }
                }
            }
        }
    }
}