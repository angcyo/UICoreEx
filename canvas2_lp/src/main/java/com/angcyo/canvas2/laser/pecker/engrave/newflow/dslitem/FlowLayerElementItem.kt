package com.angcyo.canvas2.laser.pecker.engrave.newflow.dslitem

import com.angcyo.bluetooth.fsc.laserpacker.bean.LayerColorBean
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.dslitem.CanvasLayerGroupItem
import com.angcyo.canvas2.laser.pecker.dslitem.CanvasLayerItem
import com.angcyo.canvas2.laser.pecker.engrave.newflow.LayerColorView
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.dsladapter.updateItemSelected
import com.angcyo.library.ex.toStr
import com.angcyo.widget.DslViewHolder

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/16
 *
 * [FlowLayerElementItem]
 * [FlowLayerGroupItem]
 * [CanvasLayerItem]
 *
 * [FlowEngraveElementItem]
 * [FlowEngraveGroupElementItem]
 */
open class FlowLayerElementItem : CanvasLayerGroupItem() {

    /**是否需要缩进*/
    var itemIndent = false

    /**是否进入了编辑模式*/
    var itemIsEditModel = false

    init {
        itemLayoutId = R.layout.item_flow_layer_element
        itemAutoSelectState = false
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)
        //缩进
        itemHolder.visible(R.id.indent_view, itemIndent)

        //group
        itemHolder.visible(R.id.layer_group_arrow_view, isGroupRenderer)
        itemHolder.invisible(R.id.layer_item_power_view, isGroupRenderer || itemIsEditModel)
        itemHolder.invisible(R.id.layer_item_depth_view, isGroupRenderer || itemIsEditModel)
        itemHolder.invisible(R.id.layer_item_invisible_view, isGroupRenderer)
        itemHolder.invisible(R.id.layer_color_view, isGroupRenderer)
        itemHolder.gone(R.id.layer_item_drawable_view, isGroupRenderer)
        if (isGroupRenderer) {

        } else {
            //base
            itemHolder.tv(R.id.layer_item_power_view)?.text =
                _elementBean?.printPower?.toStr() ?: "--"
            itemHolder.tv(R.id.layer_item_depth_view)?.text =
                _elementBean?.printDepth?.toStr() ?: "--"
            itemHolder.v<LayerColorView>(R.id.layer_color_view)?.apply {
                showLayerName = false
                enableSelectState = false
                layerColor =
                    if (_elementBean?.layerColor == null) null else LayerColorBean(_elementBean?.layerColor)
                invalidate()
            }
        }

        //edit
        itemHolder.gone(R.id.lib_check_view, !itemIsEditModel)
        itemHolder.gone(R.id.layer_item_sort_view, !itemIsEditModel || itemSortAction == null)

        //click
        itemHolder.click(R.id.lib_check_view) {
            updateItemSelected(!itemIsSelected)
        }
    }

    override fun onSetItemSelected(select: Boolean) {
        itemRenderer?.isSelected = select
        super.onSetItemSelected(select)
    }

}