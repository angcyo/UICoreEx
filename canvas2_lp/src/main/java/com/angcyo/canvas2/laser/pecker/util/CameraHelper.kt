package com.angcyo.canvas2.laser.pecker.util

import com.angcyo.bluetooth.fsc.CameraApiModel
import com.angcyo.canvas.render.core.CanvasRenderDelegate
import com.angcyo.canvas2.laser.pecker.CameraPictureRenderer
import com.angcyo.core.vmApp
import com.angcyo.rust.handle.RustBitmapHandle

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/09
 */
object CameraHelper {

    /**
     * 是否渲染了摄像头图片
     * [CameraApiModel.cameraBitmapData]*/
    fun isRenderCameraBitmap(delegate: CanvasRenderDelegate?): Boolean {
        return delegate?.renderManager?.beforeRendererList?.find { it is CameraPictureRenderer } != null
    }

    /**
     * @return 返回SVG 路径数据, 多个用 换行隔开
     * */
    fun getCameraPicturePath(outlineType: Int = 1): String? {
        val bitmap = vmApp<CameraApiModel>().cameraTemplateBitmap
        if (bitmap != null) {
            return RustBitmapHandle.bitmapContoursPath(bitmap, outlineType)
        }
        return null
    }
}
