package com.angcyo.canvas2.laser.pecker.engrave.newflow

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.text.style.DynamicDrawableSpan
import com.angcyo.bluetooth.fsc.laserpacker.DeviceStateModel
import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerModel
import com.angcyo.bluetooth.fsc.laserpacker._deviceConfigBean
import com.angcyo.bluetooth.fsc.laserpacker._showLaserFrequencyConfig
import com.angcyo.bluetooth.fsc.laserpacker._showPumpConfig
import com.angcyo.bluetooth.fsc.laserpacker.bean.LayerColorBean
import com.angcyo.bluetooth.fsc.laserpacker.bean._isGCodeUsePathData
import com.angcyo.bluetooth.fsc.laserpacker.bean._showRefVelocity
import com.angcyo.bluetooth.fsc.laserpacker.command.EngraveCmd
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.dslitem.CanvasSeekBarItem
import com.angcyo.canvas2.laser.pecker.engrave.LPEngraveHelper
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.EngraveSegmentScrollItem
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave.EngraveLaserFrequencySeekItem
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave.EngraveMaterialWheelItem
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave.EngraveOptionWheelItem
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave.EngravePropertyItem.Companion.getReferenceVelocity
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave.EngravePropertyItem.Companion.needShowRefVelocityLayer
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave.EngravePumpItem
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.transfer.DataTypeItem
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.transfer.ImageReliefTypeItem
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.transfer.TransferDataPxItem
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.transfer.VectorCutTypeItem
import com.angcyo.core.vmApp
import com.angcyo.dialog.BaseRecyclerDialogConfig
import com.angcyo.dialog.configBottomDialog
import com.angcyo.dialog2.dslitem.getSelectedWheelIntData
import com.angcyo.dialog2.dslitem.itemSelectedIndex
import com.angcyo.dialog2.dslitem.itemWheelList
import com.angcyo.dsladapter.DslAdapter
import com.angcyo.dsladapter.renderEmptyItem
import com.angcyo.item.style.itemCurrentIndex
import com.angcyo.item.style.itemInfoText
import com.angcyo.item.style.itemLabelText
import com.angcyo.laserpacker.LPDataConstant
import com.angcyo.laserpacker.bean.LPElementBean
import com.angcyo.laserpacker.bean.LPLaserOptionsBean
import com.angcyo.laserpacker.bean.toLaserOptionsBean
import com.angcyo.laserpacker.bean.toMaterialEntity
import com.angcyo.laserpacker.bean.updateFromLaserOptionsBean
import com.angcyo.laserpacker.device.EngraveHelper
import com.angcyo.laserpacker.device.LayerHelper
import com.angcyo.laserpacker.device.MaterialHelper
import com.angcyo.laserpacker.device.ensurePrintPrecision
import com.angcyo.laserpacker.toPaintStyleInt
import com.angcyo.library.annotation.DSL
import com.angcyo.library.ex._color
import com.angcyo.library.ex._drawable
import com.angcyo.library.ex._string
import com.angcyo.library.ex.clamp
import com.angcyo.library.ex.connect
import com.angcyo.library.ex.dpi
import com.angcyo.library.ex.gone
import com.angcyo.library.ex.isDebug
import com.angcyo.library.toastQQ
import com.angcyo.objectbox.laser.pecker.entity.EngraveConfigEntity
import com.angcyo.objectbox.laser.pecker.entity.MaterialEntity
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.span.span
import com.hingin.umeng.UMEvent
import com.hingin.umeng.umengEventValue
import kotlin.math.roundToInt

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/17
 * 参数设置弹窗
 *  最终参数在
 *  [FlowEngraveTask.bindEngraveTaskConfig]
 *  [LPEngraveHelper.generateEngraveConfig]
 */
class LaserOptionsConfigDialog : BaseRecyclerDialogConfig() {

    var control: FlowElementControl? = null

    /**如果是直接选中元素唤醒的*/
    var elementBean: LPElementBean? = null

    /**如果是选中图层颜色唤醒的*/
    var layerColorBean: LayerColorBean? = null

    //产品模式
    val laserPeckerModel = vmApp<LaserPeckerModel>()
    val deviceStateModel = vmApp<DeviceStateModel>()

    /**最终修改的参数数据结构*/
    lateinit var laserOptionsBean: LPLaserOptionsBean

    /**图层id*/
    val _layoutId: String?
        get() = if (elementBean == null) laserOptionsBean._layerId else elementBean?._layerId

    private val defaultSeekBarColors = "#34FF00,#F1E804,#FF8E00,#FF0108"

    init {
        dialogMaxHeight = "0.9sh"
        dialogTitleLayoutId = R.layout.dialog_laser_options_config_title_layout
        dialogTitle = _string(R.string.preview_parameter_setting)
        positiveButtonText = null
    }

    override fun initDialogView(dialog: Dialog, dialogViewHolder: DslViewHolder) {
        val colorBean =
            layerColorBean/*?: _deviceSettingBean?.layerColorList?.find { it.color == elementBean?.layerColor }*/
        val bean =
            elementBean?.toLaserOptionsBean() ?: control?.layerOptionMap?.get(layerColorBean?.color)
        if (bean == null) {
            toastQQ("data error!")
            dialog.cancel()
        } else {
            laserOptionsBean = bean
            super.initDialogView(dialog, dialogViewHolder)
            dialogViewHolder.v<LayerColorView>(R.id.layer_color_view)?.apply {
                layerColor = colorBean ?: layerColorBean
                enableSelectState = false
                gone(layerColor == null)
            }
            if (isDebug() && (_layoutId != null)) {
                dialogTitle = span {
                    append(dialogTitle)
                    append("$_layoutId") {
                        fontSize = 8 * dpi
                        foregroundColor = _color(R.color.text_place_color)
                    }
                }
            }
        }
    }

    var _isOptionsChanged = false

    /**当有值发生过改变*/
    fun onSelfOptionsChanged() {
        _isOptionsChanged = true
        //单独修改颜色图层内的元素, 需要清除图层颜色
        elementBean?.layerColor = null
        elementBean?.updateFromLaserOptionsBean(laserOptionsBean)
        if (!HawkEngraveKeys.useSpeedConfig) {
            //清空速度参数
            laserOptionsBean.printSpeed = null
            elementBean?.printSpeed = null
        }
        control?.updateLayerColorOptions(layerColorBean?.color, laserOptionsBean)
    }

    override fun DslAdapter.onSelfRenderAdapter() {
        val layerId = _layoutId ?: LaserPeckerHelper.LAYER_LINE

        //是否是矢量浮雕元素
        val isVectorReliefElement =
            elementBean?.reliefType != null && elementBean?.isPathElement == true && elementBean?.paintStyle == Paint.Style.STROKE.toPaintStyleInt()
        //是否是要浮雕的元素
        val isReliefElement =
            elementBean?.reliefType != null && (elementBean?.mtype == LPDataConstant.DATA_TYPE_BITMAP || isVectorReliefElement)
        //是否是切片浮雕
        val isReliefSlice =
            isReliefElement && elementBean?.reliefType == LaserPeckerHelper.RELIEF_GRAY_SLICE

        if (isReliefElement) {
            ImageReliefTypeItem()() {
                if (isVectorReliefElement) {
                    itemSegmentList = listOf(_string(R.string.slice_relief))
                    itemValueList = listOf(LaserPeckerHelper.RELIEF_GRAY_SLICE)
                    UMEvent.CANVAS_IMAGE_GRAY_RELIEF.umengEventValue()
                }
                itemLaserOptionsBean = laserOptionsBean
                observeItemChange {
                    onSelfOptionsChanged()
                    refreshDslAdapter()
                }
            }
        } else if (layerId == LaserPeckerHelper.LAYER_LINE || layerId == LaserPeckerHelper.LAYER_CUT) {
            //2024-4-22 切割类型
            VectorCutTypeItem()() {
                itemLaserOptionsBean = laserOptionsBean
                observeItemChange {
                    onSelfOptionsChanged()
                    refreshDslAdapter()
                }
            }
        } else {
            DataTypeItem()() {
                itemLaserOptionsBean = laserOptionsBean
            }
        }

        val dataDpi = laserOptionsBean.dpi

        val layerInfo = LayerHelper.getEngraveLayerInfo(layerId)
        if (layerInfo?.showDpiConfig == true) {
            //图层分辨率配置
            TransferDataPxItem()() {
                val layerSupportPxList =
                    LaserPeckerHelper.findProductLayerSupportPxList(layerInfo.layerId)
                itemPxList = if (!HawkEngraveKeys.enableZFlagPx && laserPeckerModel.isZOpen()) {
                    //L3 C1 z轴打开的情况下, 取消4k 2023-1-4 / 2023-3-10
                    layerSupportPxList.filter { it.px > LaserPeckerHelper.PX_4K } //2023-4-6 z轴不支持4K及以上
                } else {
                    layerSupportPxList
                }
                itemLayerInfo = layerInfo
                //2023-12-18
                selectorCurrentDpi(dataDpi)
                observeItemChange {
                    val dpi = itemPxList?.get(itemCurrentIndex)?.dpi ?: LaserPeckerHelper.DPI_254
                    laserOptionsBean.dpi = dpi
                    onSelfOptionsChanged()
                    refreshDslAdapter()
                }
            }
        }

        var materialList = MaterialHelper.getLayerMaterialList(layerId, dataDpi)/*val materialEntity = MaterialHelper.getLayerMaterialList(layerId).last()
        "[$layerId]材质:$materialEntity".writeToLog(logLevel = L.INFO)*/
        var materialEntity = laserOptionsBean?.toMaterialEntity() ?: materialList.first()
        if (laserOptionsBean.cutType == LaserPeckerHelper.DATA_METAL_CUT) {
            materialEntity = materialList.find { it.key == "custom" } ?: materialList.first()
            materialList = listOf(materialEntity)
        }

        //材质选择
        EngraveMaterialWheelItem()() {
            itemTag = MaterialEntity::name.name
            itemLabelText = _string(R.string.custom_material)
            itemWheelList = materialList
            itemSelectedIndex = MaterialHelper.indexOfMaterial(
                itemWheelList as List<MaterialEntity>, materialEntity
            )
            itemLaserOptionsBean = laserOptionsBean
            //刷新界面
            observeItemChange {
                onSelfOptionsChanged()
                refreshDslAdapter()
            }
        }

        // 激光光源选择
        val typeList = LaserPeckerHelper.findProductSupportLaserTypeList()
        if (laserPeckerModel.productInfoData.value?.isCSeries() != true && typeList.isNotEmpty()) {
            EngraveSegmentScrollItem()() {
                itemText = _string(R.string.laser_type)
                itemSegmentList = typeList
                itemCurrentIndex =
                    typeList.indexOfFirst { it.type.toInt() == laserOptionsBean.lightSource }
                observeItemChange {
                    val type = typeList[itemCurrentIndex].type
                    HawkEngraveKeys.lastType = type.toInt()
                    laserOptionsBean.lightSource = HawkEngraveKeys.lastType
                    onSelfOptionsChanged()
                    refreshDslAdapter()
                }
            }
        }

        //LP5 白光 出光频率
        laserOptionsBean.useLaserFrequency = false
        val laserFrequencyList = _deviceConfigBean?.laserFrequencyList
        if (_showLaserFrequencyConfig && laserPeckerModel.isL5() && laserOptionsBean.lightSource == LaserPeckerHelper.LASER_TYPE_WHITE.toInt() && !laserFrequencyList.isNullOrEmpty()) {
            laserOptionsBean.useLaserFrequency = true/*EngraveLaserFrequencyItem()() {
                itemLaserOptionsBean = laserOptionsBean
                initLaserFrequencyIfNeed()
                itemWheelList = laserFrequencyList
                itemUpdateAction(EngraveLaserFrequencyItem.PAYLOAD_UPDATE_LASER_FREQUENCY)
            }*/
            EngraveLaserFrequencySeekItem()() {
                itemLaserOptionsBean = laserOptionsBean
                initLaserFrequencyIfNeed()
                initItemValue(
                    laserOptionsBean?.laserFrequency,
                    laserFrequencyList.first(),
                    laserFrequencyList.last()
                )
                observeItemChange {
                    onSelfOptionsChanged()
                }
            }
        }

        if (laserPeckerModel.isCSeries()) {
            //C1 加速级别选择 加速级别
            if (laserOptionsBean.precision < 0) {
                laserOptionsBean.precision = 1
                onSelfOptionsChanged()
            }
            EngraveOptionWheelItem()() {
                itemTag = EngraveConfigEntity::precision.name
                itemLabelText = _string(R.string.engrave_precision)
                itemWheelList = EngraveHelper.percentList(5)
                itemSelectedIndex = EngraveHelper.findOptionIndex(
                    itemWheelList, laserOptionsBean.precision
                )
                observeItemChange {
                    laserOptionsBean.precision =
                        getSelectedWheelIntData(def = laserOptionsBean.precision).ensurePrintPrecision()
                    onSelfOptionsChanged()
                }
            }
        }

        //风速等级/气泵
        if (_showPumpConfig) {
            val pumpList = _deviceConfigBean?.pumpMap?.get(layerId)
            if (!pumpList.isNullOrEmpty()) {
                EngravePumpItem()() {
                    itemLaserOptionsBean = laserOptionsBean
                    initPumpIfNeed()
                    itemSegmentList = pumpList
                    itemUpdateAction(EngravePumpItem.PAYLOAD_UPDATE_PUMP)

                    observeItemChange {
                        onSelfOptionsChanged()
                    }
                }
            }
        }

        if (deviceStateModel.isPenMode()) {
            //握笔模块, 雕刻速度, 非雕刻深度
            laserOptionsBean.printPower = 100 //功率必须100%
            onSelfOptionsChanged()
            //雕刻速度
            EngraveOptionWheelItem()() {
                itemTag = MaterialEntity.SPEED
                itemShowRefVelocity = _showRefVelocity
                itemLabelText = _string(R.string.engrave_speed)
                itemWheelList = EngraveHelper.percentList()
                itemLaserOptionsBean = laserOptionsBean
                itemSelectedIndex = EngraveHelper.findOptionIndex(
                    itemWheelList, EngraveCmd.depthToSpeed(laserOptionsBean.printDepth)
                )
                observeItemChange {
                    onSelfOptionsChanged()
                }
            }
        } else {
            //功率/深度/次数
            /*EngravePropertyItem()() {
                itemLaserOptionsBean = laserOptionsBean
                observeItemChange {
                    onSelfOptionsChanged()
                }
            }*/

            //2024-4-24
            if (isReliefSlice) {
                //灰度切片
                CanvasSeekBarItem()() {
                    itemInfoText = _string(R.string.slice_count)
                    initItemValue(elementBean?.sliceCount, 1, 255)
                    observeItemChange {
                        elementBean?.sliceCount = _itemTypeValue as Int
                        onSelfOptionsChanged()
                    }
                }
            }
            //功率
            CanvasSeekBarItem()() {
                itemInfoText = _string(R.string.custom_power) + "(%)"
                itemSeekBgColors = defaultSeekBarColors
                itemSeekBarColor = Color.TRANSPARENT
                initItemValue(laserOptionsBean?.printPower, 1, 100)
                observeItemChange {
                    laserOptionsBean?.printPower = _itemTypeValue as Int
                    onSelfOptionsChanged()
                }
            }

            var supportMaxSpeed = _deviceConfigBean?.supportMaxSpeed
            val canUsePathData =
                (layerId == LaserPeckerHelper.LAYER_LINE && _isGCodeUsePathData) || layerId == LaserPeckerHelper.LAYER_FILL

            /// 2024-7-8 是否可以使用速度输入
            var needShowSpeedConfig = supportMaxSpeed != null && canUsePathData
            if (laserPeckerModel.haveExDevice()) {
                needShowSpeedConfig = false
            }

            if (supportMaxSpeed == null) {
                laserOptionsBean.printSpeed = null
            } else if (canUsePathData) {
                supportMaxSpeed =
                    (supportMaxSpeed / (dataDpi / LaserPeckerHelper.DPI_254)).roundToInt()
            } else {
                //不支持0x30数据
                supportMaxSpeed = null
            }
            if (HawkEngraveKeys.useSpeedConfig && needShowSpeedConfig) {
                //速度
                var speed = laserOptionsBean.printSpeed
                    ?: depthToBigSpeedIfSupport(laserOptionsBean.printDepth)
                    ?: laserOptionsBean.printDepth
                speed = clamp(1, speed, supportMaxSpeed!!)
                laserOptionsBean.printSpeed = speed
                CanvasSeekBarItem()() {
                    itemInfoText = span {
                        append(_string(R.string.custom_speed) + "(mm/s)")
                        appendSpace(4 * dpi)
                        appendDrawable(
                            _drawable(R.drawable.depth_speed_switch_svg),
                            DynamicDrawableSpan.ALIGN_BOTTOM
                        )
                    }
                    itemSeekBgColors = defaultSeekBarColors.split(",").reversed().connect(",")
                    itemSeekBarColor = Color.TRANSPARENT
                    initItemValue(speed, 1, supportMaxSpeed)
                    itemTextClickAction = {
                        HawkEngraveKeys.useSpeedConfig = false
                        laserOptionsBean.printSpeed = null
                        refreshDslAdapter()
                    }
                    observeItemChange {
                        laserOptionsBean.printSpeed = _itemTypeValue as Int

                        val tempSpeed =
                            (laserOptionsBean.printSpeed!! / supportMaxSpeed.toFloat() * 100).roundToInt()
                        laserOptionsBean.printDepth = EngraveCmd.speedToDepth(tempSpeed)

                        onSelfOptionsChanged()
                    }
                }
            } else {
                //深度
                CanvasSeekBarItem()() {
                    itemInfoText = span {
                        append(_string(R.string.custom_depth) + "(%)")
                        appendSpace(4 * dpi)
                        if (needShowSpeedConfig) {
                            appendDrawable(
                                _drawable(R.drawable.depth_speed_switch_svg),
                                DynamicDrawableSpan.ALIGN_BOTTOM
                            )
                        }
                    }

                    if (!laserPeckerModel.haveExDevice(false) && _showRefVelocity && needShowRefVelocityLayer(
                            layerId
                        )
                    ) {
                        //速度
                        val velocity = getReferenceVelocity(
                            layerId, laserOptionsBean.printDepth, dataDpi
                        )

                        //显示速度参考值
                        itemNumberLeading = if (velocity.isNullOrEmpty()) {
                            null
                        } else {
                            velocity
                        }
                    }
                    itemSeekBgColors = defaultSeekBarColors
                    itemSeekBarColor = Color.TRANSPARENT
                    initItemValue(laserOptionsBean?.printDepth, 1, 100)
                    if (supportMaxSpeed != null) {
                        itemTextClickAction = {
                            HawkEngraveKeys.useSpeedConfig = true
                            refreshDslAdapter()
                        }
                    }
                    observeItemChange {
                        laserOptionsBean?.printDepth = _itemTypeValue as Int
                        onSelfOptionsChanged()
                    }
                    itemSeekTouchEnd = { value, fraction ->
                        refreshDslAdapter()
                    }
                }
            }

            if (isReliefSlice) {
                CanvasSeekBarItem()() {
                    itemInfoText = _string(R.string.slice_height)
                    initItemValue(
                        elementBean?.sliceHeight,
                        HawkEngraveKeys.minSliceHeight,
                        HawkEngraveKeys.maxSliceHeight
                    )
                    itemDecimalCount = 2
                    observeItemChange {
                        elementBean?.sliceHeight = _itemTypeValue as Float
                        onSelfOptionsChanged()
                    }
                }
            }

            //次数
            CanvasSeekBarItem()() {
                itemInfoText = _string(R.string.print_times)
                initItemValue(
                    laserOptionsBean?.printCount,
                    1,
                    _deviceConfigBean?.maxPrintTimes ?: HawkEngraveKeys.maxPrintTimes
                )
                observeItemChange {
                    laserOptionsBean?.printCount = _itemTypeValue as Int
                    onSelfOptionsChanged()
                }
            }

            renderEmptyItem(height = 20 * dpi)
        }
    }
}

/**如果支持的话: 将深度转换成雕刻速度 mm/s */
fun depthToBigSpeedIfSupport(depth: Int): Int? {
    val supportMaxSpeed = _deviceConfigBean?.supportMaxSpeed ?: return null
    return (EngraveCmd.depthToSpeed(depth) / 100f * supportMaxSpeed).roundToInt()
}

@DSL
fun Context.laserOptionsConfigDialog(config: LaserOptionsConfigDialog.() -> Unit = {}) {
    return LaserOptionsConfigDialog().run {
        dialogContext = this@laserOptionsConfigDialog
        configBottomDialog(this@laserOptionsConfigDialog)
        canceledOnTouchOutside = true
        config()
        show()
    }
}
