package com.angcyo.canvas2.laser.pecker.engrave.newflow

import com.angcyo.library.annotation.CallPoint

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/18
 *
 * 雕刻数据状态提供
 * [EngraveState]
 */
class EngraveDataStateProvider {
    /**保存每个元素的雕刻状态, 这个状态包含传输的状态和雕刻的状态
     * 通过元素的uuid查询
     * [com.angcyo.laserpacker.bean.LPElementBean.uuid]
     *
     * [EngraveState.ENGRAVE_STATE_TRANSMITTING]
     * [EngraveState.ENGRAVE_STATE_ENGRAVING]
     * */
    val engraveStateMap = mutableMapOf<String, EngraveState>()

    /**更新状态*/
    fun updateState(uuid: String?, action: EngraveState.() -> Unit) {
        if (uuid.isNullOrEmpty()) {
            return
        }
        val state = engraveStateMap[uuid] ?: EngraveState()
        state.action()
        engraveStateMap[uuid] = state
    }

    /**将所有数据的状态更新更新到指定*/
    fun updateAllState(state: Int) {
        engraveStateMap.values.forEach {
            it.state = state
        }
    }

    /**释放资源*/
    @CallPoint
    fun release() {
        engraveStateMap.clear()
    }

    /**是否所有状态都是完成的*/
    fun isAllFinish(): Boolean {
        return engraveStateMap.values.all { it.state == EngraveState.ENGRAVE_STATE_FINISH }
    }

}