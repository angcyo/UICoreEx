package com.angcyo.canvas2.laser.pecker.engrave.newflow

import android.app.Dialog
import android.content.Context
import com.angcyo.bluetooth.fsc.laserpacker._deviceSettingBean
import com.angcyo.bluetooth.fsc.laserpacker.bean.LayerColorBean
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.dialog.BaseDialogConfig
import com.angcyo.dialog.configBottomDialog
import com.angcyo.dialog.hideSoftInput
import com.angcyo.library.annotation.DSL
import com.angcyo.library.ex._string
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.base.clickIt
import com.angcyo.widget.base.resetChild
import com.angcyo.widget.flow

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/16
 */
class LayerColorDialog(context: Context? = null) : BaseDialogConfig(context) {

    /**是否仅显示清除图层*/
    var onlyClearLayer: Boolean = false

    /**是否支持多选*/
    var multipleChoice: Boolean = false

    /**选中的颜色*/
    var selectedLayerColorList: List<LayerColorBean>? = null

    /**返回值,表示是否拦截默认处理*/
    var onLayerColorAction: ((LayerColorBean?) -> Boolean)? = null

    init {
        dialogTitle = _string(R.string.move_to)
        dialogLayoutId = R.layout.dialog_layer_color_layout
        positiveButton { dialog, dialogViewHolder ->
            if (onLayerColorAction?.invoke(selectedLayerColorList?.firstOrNull()) == true) {
                //拦截默认处理
            } else {
                dialog.hideSoftInput()
                dialog.dismiss()
            }
        }
    }

    override fun initDialogView(dialog: Dialog, dialogViewHolder: DslViewHolder) {
        super.initDialogView(dialog, dialogViewHolder)
        updateLayout(dialogViewHolder)
    }

    fun updateLayout(dialogViewHolder: DslViewHolder) {
        val list = mutableListOf<LayerColorBean>()
        list.add(LayerColorBean())/*empty*/
        if (!onlyClearLayer) {
            list.addAll(_deviceSettingBean?.layerColorList ?: emptyList())
        }
        dialogViewHolder.flow(R.id.lib_flow_layout)
            ?.resetChild(list, R.layout.layout_layer_color) { itemView, bean, index ->
                (itemView as? LayerColorView)?.apply {
                    val colorList = selectedLayerColorList ?: emptyList()
                    val newColorList = mutableListOf<LayerColorBean>()
                    newColorList.addAll(colorList)
                    isSelected = newColorList.contains(bean)
                    layerColor = bean
                    clickIt {
                        if (!multipleChoice) {
                            newColorList.clear()
                        }
                        newColorList.remove(bean)
                        newColorList.add(bean)
                        selectedLayerColorList = newColorList
                        updateLayout(dialogViewHolder)
                    }
                }
            }
    }
}

@DSL
fun Context.layerColorDialog(config: LayerColorDialog.() -> Unit = {}) {
    return LayerColorDialog().run {
        dialogContext = this@layerColorDialog
        configBottomDialog(this@layerColorDialog)
        canceledOnTouchOutside = true
        config()
        show()
    }
}
