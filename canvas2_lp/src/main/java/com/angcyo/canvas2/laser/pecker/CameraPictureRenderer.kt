package com.angcyo.canvas2.laser.pecker

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import androidx.core.graphics.withTranslation
import com.angcyo.bluetooth.fsc.bean.CameraTemplateBean
import com.angcyo.bluetooth.fsc.laserpacker._deviceSettingBean
import com.angcyo.canvas.render.core.CanvasRenderDelegate
import com.angcyo.canvas.render.data.RenderParams
import com.angcyo.canvas.render.renderer.BaseRenderer
import com.angcyo.library.unit.toPixel
import kotlin.math.roundToInt

/**
 * 摄像头拍照渲染器
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/07
 */
class CameraPictureRenderer : BaseRenderer() {

    val paint = Paint().apply {
        // 设置透明度
        setAlpha(
            _deviceSettingBean?.cameraTemplatePreviewAlpha ?: (0.75 * 255).roundToInt()
        ) // 75%透明度
    }

    /**拍照的照片*/
    var cameraPicture: Bitmap? = null

    /**模板*/
    var cameraTemplateBean: CameraTemplateBean? = null

    override fun isVisibleInRender(
        delegate: CanvasRenderDelegate?,
        fullIn: Boolean,
        def: Boolean
    ): Boolean {
        return true
    }

    override fun renderOnInside(canvas: Canvas, params: RenderParams) {
        //super.renderOnInside(canvas, params)
        if (cameraPicture?.isRecycled == true) {
            cameraPicture = null
        }
        cameraPicture?.let { bitmap ->
            val bean = cameraTemplateBean
            if (!bitmap.isRecycled && bean != null) {
                val left = bean.form!!.centerX!!.toPixel() - bitmap.width / 2
                val top = bean.form!!.centerY!!.toPixel() - bitmap.height / 2
                canvas.withTranslation(left, top) {
                    canvas.drawBitmap(bitmap, 0f, 0f, paint)
                }
            }
        }
    }
}