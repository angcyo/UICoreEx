package com.angcyo.canvas2.laser.pecker.dslitem

import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.item.DslSeekBarInfoItem
import com.angcyo.item.DslSeekBarItem
import com.angcyo.item.keyboard.numberKeyboardDialog
import com.angcyo.item.style.itemInfoText
import com.angcyo.library.component.RegionTouchDetector
import com.angcyo.library.ex._color
import com.angcyo.library.ex.dpi
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.progress.DslSeekBar

/**
 * 画布滑块Item, 带气泡提示
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2023/03/09
 */
class CanvasSeekBarInfoItem : DslSeekBarInfoItem() {

    /**键盘输入相关属性*/
    /**进度值的类型*/
    var itemSeekProgressType: Any? = null

    /**键盘输入的最小值*/
    var itemNumberMinValue: Any? = null

    /**键盘输入的最大值*/
    var itemNumberMaxValue: Any? = null

    init {
        itemExtendLayoutId = R.layout.dsl_extent_seek_little_item
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)
    }

    override fun initSeekBarView(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        itemHolder.v<DslSeekBar>(R.id.lib_seek_view)?.apply {
            if (itemNumberMinValue is Int) {
                progressMinValue = (itemNumberMinValue as Int).toFloat()
            }
            if (itemNumberMinValue is Float) {
                progressMinValue = itemNumberMinValue as Float
            }
            //--
            if (itemNumberMaxValue is Int) {
                progressMaxValue = (itemNumberMaxValue as Int).toFloat()
            }
            if (itemNumberMaxValue is Float) {
                progressMaxValue = itemNumberMaxValue as Float
            }
        }
        super.initSeekBarView(itemHolder, itemPosition, adapterItem, payloads)
        itemHolder.v<DslSeekBar>(R.id.lib_seek_view)?.apply {
            val color = _color(R.color.canvas_primary)
            setBgGradientColors("${_color(R.color.canvas_line)}")
            setTrackGradientColors("$color")
            updateThumbColor(color)

            if (itemSeekProgressType != null) {
                progressValueTouchAction = { touchType ->
                    if (touchType == RegionTouchDetector.TOUCH_TYPE_CLICK) {
                        //点击事件类型
                        itemHolder.context.numberKeyboardDialog {
                            dialogTitle = itemInfoText
                            numberValueType = itemSeekProgressType
                            numberMinValue = itemNumberMinValue
                            numberMaxValue = itemNumberMaxValue
                            numberValue = progressValue /*updateProgressValue(itemSeekProgress)*/
                            onNumberResultAction = {
                                it?.let {
                                    val value = getProgressValueFraction() ?: itemSeekProgress
                                    itemSeekProgress = value

                                    val fraction =
                                        if (itemNumberMinValue != null && itemNumberMaxValue != null) {
                                            val minFloat = itemNumberMinValue!!.toString().toFloat()
                                            val maxFloat = itemNumberMaxValue!!.toString().toFloat()
                                            value / (maxFloat - minFloat)
                                        } else {
                                            value / 100
                                        }
                                    onItemSeekChanged(value, fraction, true)
                                    itemSeekTouchEnd(value, fraction)
                                    updateAdapterItem()
                                }
                                false
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onItemChangeListener(item: DslAdapterItem) {
        //super.onItemChangeListener(item)
    }

}

open class CanvasSeekBarItem : DslSeekBarItem() {

    init {
        val color = _color(R.color.canvas_primary)
        itemSeekBarColor = color
        itemSeekBgColors = "${_color(R.color.canvas_line)}"

        itemSeekProgressHeight = 4 * dpi
    }

}