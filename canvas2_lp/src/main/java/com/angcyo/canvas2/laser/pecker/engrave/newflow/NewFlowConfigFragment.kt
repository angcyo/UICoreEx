package com.angcyo.canvas2.laser.pecker.engrave.newflow

import android.os.Bundle
import com.angcyo.DslFHelper
import com.angcyo.base.dslFHelper
import com.angcyo.behavior.BaseScrollBehavior
import com.angcyo.behavior.IScrollBehaviorListener
import com.angcyo.behavior.effect.TouchBackBehavior
import com.angcyo.canvas.render.core.CanvasRenderDelegate
import com.angcyo.canvas.render.renderer.BaseRenderer
import com.angcyo.canvas.render.renderer.CanvasElementRenderer
import com.angcyo.canvas.render.renderer.CanvasGroupRenderer
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.dslitem.CanvasLayerItem
import com.angcyo.canvas2.laser.pecker.engrave.newflow.dslitem.FlowLayerElementItem
import com.angcyo.canvas2.laser.pecker.engrave.newflow.dslitem.FlowLayerGroupItem
import com.angcyo.canvas2.laser.pecker.util.lpElementBean
import com.angcyo.core.fragment.BaseDslFragment
import com.angcyo.dialog.messageDialog
import com.angcyo.dsladapter.DragCallbackHelper
import com.angcyo.dsladapter.eachItem
import com.angcyo.dsladapter.item.initExtendLayout
import com.angcyo.library.annotation.CallPoint
import com.angcyo.library.canvas.core.Reason
import com.angcyo.library.component.Strategy
import com.angcyo.library.ex._color
import com.angcyo.library.ex._colorDrawable
import com.angcyo.library.ex._string
import com.angcyo.library.ex.gone
import com.angcyo.library.ex.mH
import com.angcyo.library.ex.size
import com.angcyo.library.toast
import com.angcyo.library.toastQQ
import com.angcyo.widget.base.behavior
import com.angcyo.widget.base.clickIt
import com.angcyo.widget.base.resetChild
import com.angcyo.widget.base.setTopIco
import com.angcyo.widget.flow
import com.angcyo.widget.span.span

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/15
 * 新的雕刻设置界面, 雕刻前的设置
 * [com.angcyo.canvas2.laser.pecker.engrave.BasePreviewLayoutHelper.renderPreviewItems]
 */
class NewFlowConfigFragment : BaseDslFragment() {

    var canvasRenderDelegate: CanvasRenderDelegate? = null

    val flowElementControl = FlowElementControl()

    init {
        fragmentTitle = _string(R.string.engrave_setting)
        fragmentLayoutId = R.layout.new_flow_config_layout
        enableSoftInput = false

        fragmentConfig.isLightStyle = true
        fragmentConfig.showTitleLineView = true
    }

    override fun initFHelperOnBack(helper: DslFHelper) {
        helper.anim(0, R.anim.lib_translate_y_remove_exit)
    }

    override fun onDestroy() {
        super.onDestroy()
        canvasRenderDelegate?.refresh()
    }

    override fun initBaseView(savedInstanceState: Bundle?) {
        fragmentConfig.showTitleLineView = true

        super.initBaseView(savedInstanceState)
        rootControl().setBackground(_colorDrawable(_color(R.color.transparent20)))
        initTouchBackLayout()

        _vh.click(R.id.lib_cancel_view) {
            closeThisFragment()
        }
        _vh.click(R.id.lib_next_view) {
            val elementList = flowElementControl.getAllEngraveElementList()
            if (elementList.isEmpty()) {
                toastQQ(_string(R.string.no_data_engrave))
            } else {
                dslFHelper {
                    anim(R.anim.lib_y_show_enter_holder, 0)
                    show(NewEngraveFragment::class) {
                        (this as NewEngraveFragment).flowElementControl =
                            this@NewFlowConfigFragment.flowElementControl
                    }
                }
            }
        }
        _vh.click(R.id.layer_edit_view) {
            //编辑模式
            flowElementControl.isEditModel = !flowElementControl.isEditModel
            updateLayout()
            updateDslAdapter()
        }

        //参数层
        _vh.selected(R.id.param_arrow_view, true, false)
        initExtendLayout(
            _vh,
            emptyList(),
            R.id.param_arrow_view,
            extendRotation = 0f,
            foldRotation = 90f,
            toExtendRotation = -90f,
            toFoldRotation = 90f,
            triggerViewIdList = listOf(R.id.param_arrow_view, R.id.param_header_wrap_layout)
        ) {
            _vh.gone(R.id.param_wrap_layout, !it)
        }

        //对象层
        _vh.selected(R.id.layer_arrow_view, true, false)
        initExtendLayout(
            _vh,
            emptyList(),
            R.id.layer_arrow_view,
            extendRotation = 0f,
            foldRotation = 90f,
            toExtendRotation = -90f,
            toFoldRotation = 90f,
            triggerViewIdList = listOf(R.id.layer_arrow_view, R.id.layer_header_wrap_layout)
        ) {
            _vh.invisible(R.id.lib_recycler_view, !it)
        }

        //控制操作
        _vh.click(R.id.layer_move_view) {
            fContext().layerColorDialog {
                onlyClearLayer = !flowElementControl.isSameTypeElementSelected()
                onLayerColorAction = { bean ->
                    if (bean != null) {
                        val rendererList = flowElementControl.getAllSelectElementList()
                        if (flowElementControl.updateElementColorLayer(rendererList, bean)) {
                            updateLayout()
                            //updateDslAdapter()
                            _adapter.updateAllItem()
                            false
                        } else {
                            fContext().messageDialog {
                                dialogTitle = _string(R.string.ui_reminder)
                                dialogMessage = _string(R.string.different_data_mode_tip)
                                positiveButtonText = _string(R.string.ui_known)
                            }
                            true
                        }
                    } else {
                        false
                    }
                }
            }
        }

        if (canvasRenderDelegate == null) {
            closeThisFragment()
            toast("data error!")
        } else {
            flowElementControl.initControl(canvasRenderDelegate)

            if (_layerDragHelper == null) {
                _layerDragHelper = DragCallbackHelper.install(
                    _vh.rv(R.id.lib_recycler_view)!!, DragCallbackHelper.FLAG_VERTICAL
                ).apply {
                    enableLongPressDrag = false//长按拖拽关闭
                    onClearViewAction = { recyclerView, viewHolder ->
                        if (_dragHappened) {
                            //发生过拖拽
                            val list = mutableListOf<BaseRenderer>()
                            _dslAdapter?.eachItem { index, dslAdapterItem ->
                                if (dslAdapterItem is CanvasLayerItem) {
                                    dslAdapterItem.itemRenderer?.let {
                                        list.add(0, it)
                                    }
                                }
                            }
                            flowElementControl.renderDelegate?.renderManager?.arrangeElementSortWith(
                                list, Reason.user, Strategy.normal
                            )
                        }
                    }
                }
            }

            updateLayout()
            updateDslAdapter()
        }
    }

    var _layerDragHelper: DragCallbackHelper? = null

    /**需要动态控制的布局*/
    fun updateLayout() {
        _vh.gone(R.id.lib_next_view, flowElementControl.isEditModel)
        flowElementControl.initColorLayerInfo()
        _updateLayerColorLayout()
        val selectedList = flowElementControl.getAllSelectElementList()
        val selectedCount = selectedList.size()
        _vh.tv(R.id.layer_header_des_view)?.text =
            if (flowElementControl.isEditModel && selectedCount > 0) {
                span { append(_string(R.string.selected_object_des, selectedCount)) }
            } else {
                null
            }
        _vh.gone(
            R.id.bottom_control_wrap_layout, !flowElementControl.isEditModel || selectedCount < 1
        )
        _vh.gone(R.id.layer_header_select_wrap_layout, !flowElementControl.isEditModel)
        _vh.img(R.id.layer_edit_view)?.apply {
            setImageResource(if (flowElementControl.isEditModel) R.drawable.layer_edit_cancel_svg else R.drawable.layer_edit_svg)
            clickIt {
                flowElementControl.isEditModel = !flowElementControl.isEditModel
                if (flowElementControl.isEditModel) {
                    touchBackBehavior()?.scrollToTop()
                } else {
                    flowElementControl.clearElementSelectState()
                }
                updateLayout()
                updateDslAdapter()
            }
        }
        _vh.img(R.id.layer_header_select_image_view)?.apply {
            val selectState = flowElementControl.getAllElementSelectState()
            setImageResource(
                when (selectState) {
                    1 -> R.drawable.canvas_checked
                    0 -> R.drawable.canvas_half_checked
                    else -> R.drawable.canvas_check
                }
            )
            clickIt {
                flowElementControl.updateElementSelectState(
                    _adapter, if (selectState == 1) -1 else 1
                )
                updateLayout()
                updateDslAdapter()
            }
        }

        //控制: 可见性
        val allSelectedVisible = flowElementControl.isAllSelectedElementVisible()
        _vh.tv(R.id.layer_hide_view)?.apply {
            text = if (allSelectedVisible) _string(R.string.hide) else _string(R.string.visible)
            setTopIco(if (allSelectedVisible) R.drawable.canvas_invisible_svg else R.drawable.canvas_visible_svg)
        }
        _vh.click(R.id.layer_hide_view) {
            flowElementControl.hideAllSelectedElement(allSelectedVisible)
            updateLayout()
            updateDslAdapter()
        }
        _vh.visible(R.id.layer_power_view, !flowElementControl.isEditModel)
        _vh.visible(R.id.layer_depth_view, !flowElementControl.isEditModel)

        //全部可见or全部隐藏
        _vh.view(R.id.layer_invisible_view)?.apply {
            gone(flowElementControl.isEditModel)
            isSelected = !flowElementControl.isAllElementInvisible()
            clickIt {
                if (flowElementControl.isAllElementVisible()) {
                    flowElementControl.hideAllElement(hide = true)
                } else {
                    flowElementControl.hideAllElement(hide = false)
                }
                updateLayout()
                updateDslAdapter()
            }
        }
    }

    /**是否隐藏图层参数相关布局*/
    fun _hideLayerParamLayout(hide: Boolean) {
        _vh.gone(R.id.param_header_wrap_layout, hide)
        _vh.gone(R.id.param_wrap_layout, hide)
        _vh.gone(R.id.param_line_view, hide)
    }

    /**更新图层*/
    fun _updateLayerColorLayout() {
        val list = flowElementControl.layerList.toList()
        _hideLayerParamLayout(list.isEmpty())
        _vh.flow(R.id.param_wrap_layout)
            ?.resetChild(list, R.layout.layout_layer_color) { itemView, bean, index ->
                (itemView as? LayerColorView)?.apply {
                    layerColor = bean
                    invalidate()
                    clickIt {
                        //需要更新图层内所有元素的雕刻参数
                        if (!flowElementControl.isEditModel) {
                            fContext().laserOptionsConfigDialog {
                                control = flowElementControl
                                layerColorBean = bean
                                onDismissListener = {
                                    if (_isOptionsChanged) {
                                        _adapter.updateAllItem()
                                    }
                                }
                            }
                        }
                    }
                }
            }
    }

    /**调用此方法重新渲染界面*/
    fun updateDslAdapter() {
        val rendererList = flowElementControl.getCanvasElementList()
        renderDslAdapter(true) {
            rendererList.forEach { renderer ->
                if (renderer is CanvasGroupRenderer) {
                    FlowLayerGroupItem()() {
                        itemRenderer = renderer
                        itemRenderDelegate = canvasRenderDelegate
                        itemIsEditModel = flowElementControl.isEditModel
                        itemSubList = mutableListOf()
                        renderer.rendererList.forEach { subRender ->
                            val subItem = FlowLayerElementItem().apply {
                                itemIndent = true
                                itemRenderer = subRender
                                itemRenderDelegate = canvasRenderDelegate
                                itemIsEditModel = flowElementControl.isEditModel
                                itemIsSelected = subRender.isSelected
                                observeItemSelectedChange {
                                    updateLayout()
                                    updateDslAdapter()
                                }
                                itemClick = {
                                    if (!flowElementControl.isEditModel) {
                                        fContext().laserOptionsConfigDialog {
                                            elementBean = subRender.lpElementBean()
                                            control = flowElementControl
                                            onDismissListener = {
                                                if (_isOptionsChanged) {
                                                    _adapter.updateAllItem()
                                                }
                                            }
                                        }
                                    }
                                }
                                itemVisibleChangeAction = {
                                    updateLayout()
                                }/*itemSortAction = {
                                  _layerDragHelper?.startDrag(it)
                              }*/
                                //组内元素不支持拖拽
                                itemSortAction = null
                            }
                            itemSubList.add(subItem)
                        }
                        itemGroupExtend = renderer.isExpand ?: true
                        itemUpdateAction {
                            val selectState = flowElementControl.getAllItemSelectState(itemSubList)
                            itemIsHalfChecked = selectState == 0
                            itemIsSelected = selectState == 1
                            itemGroupElementHide =
                                flowElementControl.isAllElementInvisible(renderer.rendererList)
                            if (itemGroupElementHide) {
                                renderer.updateVisible(false, Reason.user, canvasRenderDelegate)
                            } else {
                                renderer.updateVisible(
                                    true, Reason.user.apply {
                                        data = Reason.DATA_ONLY_SELF
                                    }, canvasRenderDelegate
                                )
                            }
                        }
                        observeItemGroupExtendChange {
                            renderer.isExpand = itemGroupExtend
                        }
                        observeItemSelectedChange {
                            updateLayout()
                            updateDslAdapter()
                        }
                        itemVisibleChangeAction = {
                            updateLayout()
                        }/*itemSortAction = {
                            _layerDragHelper?.startDrag(it)
                        }*/
                    }
                } else if (renderer is CanvasElementRenderer) {
                    FlowLayerElementItem()() {
                        itemRenderer = renderer
                        itemRenderDelegate = canvasRenderDelegate
                        itemIsEditModel = flowElementControl.isEditModel
                        itemIsSelected = renderer.isSelected

                        observeItemSelectedChange {
                            updateLayout()
                            updateDslAdapter()
                        }

                        itemClick = {
                            if (!flowElementControl.isEditModel) {
                                fContext().laserOptionsConfigDialog {
                                    control = flowElementControl
                                    elementBean = renderer.lpElementBean()
                                    onDismissListener = {
                                        if (_isOptionsChanged) {
                                            _adapter.updateAllItem()
                                        }
                                    }
                                }
                            }
                        }

                        itemVisibleChangeAction = {
                            updateLayout()
                        }

                        /*itemSortAction = {
                            _layerDragHelper?.startDrag(it)
                        }*/
                    }
                }
            }
        }
    }

    /**获取[TouchBackBehavior]*/
    fun touchBackBehavior(): TouchBackBehavior? {
        var touchBackBehavior: TouchBackBehavior? = null
        _vh.view(R.id.touch_back_layout).behavior()?.apply {
            if (this is TouchBackBehavior) {
                resetToMaxRadio = 0.85f
                touchBackBehavior = this
            }
        }
        return touchBackBehavior
    }

    /**初始化*/
    @CallPoint
    fun initTouchBackLayout() {
        touchBackBehavior()?.apply {
            addScrollListener(object : IScrollBehaviorListener {
                override fun onBehaviorScrollTo(
                    scrollBehavior: BaseScrollBehavior<*>, x: Int, y: Int, scrollType: Int
                ) {
                    //L.i("-> x:$x y:$y")
                    val maxY = childView.mH()
                    //关闭页面
                    if (y >= maxY) {
                        closeThisFragment()
                    }
                }
            })
        }
    }
}