package com.angcyo.canvas2.laser.pecker.dialog.dslitem

import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.dialog.SpeedInfo
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.item.keyboard.numberKeyboardDialog
import com.angcyo.library.ex._string
import com.angcyo.library.ex.decimal
import com.angcyo.library.ex.toColorInt
import com.angcyo.library.ex.toStr
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.span.span

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2023/12/27
 */
class SpeedConvertItem : DslAdapterItem() {

    var itemLayerId: String? = null

    var itemDpi: Float = 254f

    /**仅显示功率转换*/
    var itemOnlyPower = false

    /**功率*/
    var itemPower = HawkEngraveKeys.lastPower
        set(value) {
            field = value
            _resultPower = value
        }

    /**深度*/
    var itemDepth = HawkEngraveKeys.lastDepth
        set(value) {
            field = value
            _resultDepth = value
        }

    val color = "#0C0C06".toColorInt()

    private var _resultPower = itemPower
    private var _resultDepth = itemDepth

    var onItemDepthChanged: (depth: Int, power: Int) -> Unit = { _, _ -> }

    init {
        itemLayoutId = R.layout.item_speed_convert_layout
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)

        val oldSpeedList =
            SpeedInfo.getOldSpeedList(itemLayerId ?: LaserPeckerHelper.LAYER_LINE, itemDpi)
        val newSpeedList = SpeedInfo.getNewSpeedList(itemDpi)

        //旧值转成新的值
        val speedInfo = oldSpeedList.find { it.depth == itemDepth }
        val newSpeedInfo = SpeedInfo.findNearestSpeed(newSpeedList, speedInfo?.speed)
        _resultDepth = newSpeedInfo?.depth ?: _resultDepth

        val newPowerInfo = SpeedInfo.findNearestNewPower(itemPower)
        _resultPower = newPowerInfo?.power ?: _resultPower
        onItemDepthChanged(_resultDepth, _resultPower)

        itemHolder.tv(R.id.old_power_view)?.text = itemPower.toStr()
        itemHolder.tv(R.id.new_power_view)?.text = _resultPower.toStr()
        itemHolder.tv(R.id.old_speed_view)?.text = itemDepth.toStr()
        itemHolder.tv(R.id.new_speed_view)?.text = _resultDepth.toStr()

        itemHolder.tv(R.id.old_text_view)?.text = span {
            append(_string(R.string.engrave_speed))
            appendLine()
            append("${speedInfo?.speed?.decimal(fadedUp = true) ?: "--"} mm/s") {
                foregroundColor = color
            }
        }

        itemHolder.tv(R.id.new_text_view)?.text = span {
            append(_string(R.string.engrave_speed))
            appendLine()
            append("${newSpeedInfo?.speed?.decimal(fadedUp = true) ?: "--"} mm/s") {
                foregroundColor = color
            }
        }

        //click
        itemHolder.click(R.id.old_power_view) {
            it.context.numberKeyboardDialog {
                numberValue = itemPower
                numberMinValue = 1
                numberMaxValue = 100
                onNumberResultAction = {
                    it?.let {
                        itemPower = it as Int
                        HawkEngraveKeys.lastPower = itemPower
                        itemChanging = true
                        updateAdapterItem()
                    }
                    false
                }
            }
        }

        itemHolder.click(R.id.old_speed_view) {
            it.context.numberKeyboardDialog {
                numberValue = itemDepth
                numberMinValue = 1
                numberMaxValue = 100
                onNumberResultAction = {
                    it?.let {
                        itemDepth = it as Int
                        HawkEngraveKeys.lastDepth = itemDepth
                        itemChanging = true
                        updateAdapterItem()
                    }
                    false
                }
            }
        }

        //2024-4-19 itemOnlyPower
        itemHolder.gone(R.id.old_speed_label, itemOnlyPower)
        itemHolder.gone(R.id.old_speed_view, itemOnlyPower)
        itemHolder.gone(R.id.old_text_view, itemOnlyPower)
        itemHolder.gone(R.id.new_speed_label, itemOnlyPower)
        itemHolder.gone(R.id.new_speed_view, itemOnlyPower)
        itemHolder.gone(R.id.new_text_view, itemOnlyPower)
    }

}