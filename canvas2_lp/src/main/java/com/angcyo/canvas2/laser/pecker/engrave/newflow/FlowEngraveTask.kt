package com.angcyo.canvas2.laser.pecker.engrave.newflow

import android.graphics.RectF
import androidx.lifecycle.Observer
import com.angcyo.bluetooth.fsc.laserpacker._deviceSettingBean
import com.angcyo.canvas.render.renderer.BaseRenderer
import com.angcyo.canvas.render.renderer.CanvasGroupRenderer
import com.angcyo.canvas2.laser.pecker.engrave.LPEngraveHelper
import com.angcyo.canvas2.laser.pecker.engrave.LPTransferHelper
import com.angcyo.canvas2.laser.pecker.util.lpElementBean
import com.angcyo.core.vmApp
import com.angcyo.engrave2.EngraveFlowDataHelper
import com.angcyo.engrave2.data.TransferState
import com.angcyo.engrave2.model.EngraveModel
import com.angcyo.engrave2.model.TransferModel
import com.angcyo.http.rx.doBack
import com.angcyo.laserpacker.device.EngraveHelper
import com.angcyo.library.annotation.CallPoint
import com.angcyo.library.component.TaskManager
import com.angcyo.library.ex.nowTime
import com.angcyo.library.ex.resetAll
import com.angcyo.library.utils.uuid
import com.angcyo.objectbox.laser.pecker.entity.EngraveTaskEntity
import com.angcyo.objectbox.laser.pecker.entity.TransferDataEntity
import com.angcyo.objectbox.laser.pecker.lpSaveAllEntity
import com.angcyo.viewmodel.updateThis
import com.angcyo.viewmodel.vmData

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/18
 *
 * 流程雕刻任务, 一批一批的雕刻或者一个元素一个元素雕刻
 * 雕刻之前, 需要先传输数据, 然后再雕刻
 */
class FlowEngraveTask : TaskManager.ITask() {

    /**当前任务分配的id*/
    private var taskId = uuid()

    /**第一个元素的uuid*/
    val elementUuid: String?
        get() = rendererList.firstOrNull()?.lpElementBean()?.uuid

    /**当前任务的状态*/
    val engraveTaskStateData = vmData(EngraveState())

    val rendererList = mutableListOf<BaseRenderer>()

    /**传输模式*/
    val transferModel = vmApp<TransferModel>()

    /**雕刻模式*/
    val engraveModel = vmApp<EngraveModel>()

    /**传输状态监听*/
    val transferObserver: Observer<TransferState?> = Observer { transferState ->
        if (taskState == TaskManager.TaskState.Start && transferState != null) {
            val engraveState = engraveTaskStateData.value
            if (transferState.state == TransferState.TRANSFER_STATE_FINISH) {
                if (transferState.error == null) {
                    //全部数据传输完成
                    if (engraveState?.state == EngraveState.ENGRAVE_STATE_TRANSMITTING) {
                        engraveState.error = null
                        engraveState.progress = 100
                        engraveTaskStateData.updateThis()
                        _startEngrave()
                    }
                } else {
                    //传输失败
                    engraveState?.state = EngraveState.ENGRAVE_STATE_ERROR
                    engraveState?.error = transferState.error
                    engraveTaskStateData.updateThis()
                }
            } else if (transferState.state == TransferState.TRANSFER_STATE_NORMAL) {
                //传输中
                engraveState?.state = EngraveState.ENGRAVE_STATE_TRANSMITTING
                engraveState?.error = null
                engraveState?.progress = transferState.progress
                engraveTaskStateData.updateThis()
            } else if (transferState.state == TransferState.TRANSFER_STATE_CREATE) {
                //创建中
                engraveState?.state = EngraveState.ENGRAVE_STATE_CREATING
                engraveState?.error = null
                engraveState?.progress = -1
                engraveTaskStateData.updateThis()
            }
        }
    }

    /**雕刻状态监听*/
    val engraveObserver: Observer<EngraveTaskEntity?> = Observer { engraveEntity ->
        if (taskState == TaskManager.TaskState.Start && engraveEntity != null) {
            val engraveState = engraveTaskStateData.value
            if (engraveEntity.state == EngraveModel.ENGRAVE_STATE_ERROR) {
                engraveState?.state = EngraveState.ENGRAVE_STATE_ERROR
                engraveState?.error = engraveModel._lastEngraveCmdError
                engraveTaskStateData.updateThis()
                doBack {
                    taskManager?.nextTask(this, engraveModel._lastEngraveCmdError)
                }
            } else if (engraveEntity.state == EngraveModel.ENGRAVE_STATE_PAUSE) {
                engraveState?.state = EngraveState.ENGRAVE_STATE_PAUSE
                engraveTaskStateData.updateThis()
            } else if (engraveEntity.state == EngraveModel.ENGRAVE_STATE_FINISH) {
                if (engraveState?.state == EngraveState.ENGRAVE_STATE_ENGRAVING) {
                    engraveState.state = EngraveState.ENGRAVE_STATE_FINISH
                    engraveState.progress = 100
                    engraveTaskStateData.updateThis()
                    doBack {
                        val engraveDelay = _deviceSettingBean?.engraveDelay ?: 0
                        if (engraveDelay > 0) {
                            Thread.sleep(engraveDelay)
                        }
                        if (taskManager?.cancel == true) {
                        } else {
                            taskManager?.nextTask(this)
                        }
                    }
                }
            } else if (engraveEntity.state == EngraveModel.ENGRAVE_STATE_START) {
                engraveState?.state = EngraveState.ENGRAVE_STATE_ENGRAVING
                engraveState?.progress = engraveEntity.progress
                engraveTaskStateData.updateThis()
            }
        }
    }

    /**使用指定元素初始化一个任务*/
    @CallPoint
    fun initEngraveTask(rendererList: List<BaseRenderer>) {
        this.rendererList.resetAll(rendererList)
        transferModel.transferStateOnceData.observeForever(transferObserver)
        engraveModel.engraveStateOnceData.observeForever(engraveObserver)

        bindEngraveTaskConfig()
    }

    /**重新绑定雕刻任务的配置参数, 通常在改变任务id之后, 调用*/
    @CallPoint
    fun bindEngraveTaskConfig() {
        //雕刻参数绑定
        rendererList.forEach {
            it.lpElementBean()?.let { bean ->
                bean.index = bean.index ?: EngraveHelper.generateEngraveIndex()
                bean.name = bean.name ?: "${bean.index}"
                LPEngraveHelper.generateEngraveConfig(taskId, bean)
            }
        }
    }

    /**更新任务id*/
    fun updateTaskId() {
        taskId = uuid()
        bindEngraveTaskConfig()
    }

    /**是否资源*/
    @CallPoint
    fun release() {
        transferModel.transferStateOnceData.removeObserver(transferObserver)
        engraveModel.engraveStateOnceData.removeObserver(engraveObserver)
    }

    /**开始雕刻任务, 传输数据->雕刻*/
    fun startEngraveTask() {
        _startTransferData()
    }

    /**暂停雕刻*/
    fun pauseEngraveTask() {
        engraveModel.pauseEngrave()
    }

    /**恢复雕刻*/
    fun resumeEngraveTask() {
        engraveModel.continueEngrave()
    }

    /**取消雕刻*/
    fun cancelEngraveTask() {
        taskManager?.cancel()
        engraveModel.stopEngrave("手动停止")
    }

    /**开始传输数据*/
    private fun _startTransferData() {
        //开始创建数据
        engraveTaskStateData.value?.state = EngraveState.ENGRAVE_STATE_CREATING
        engraveTaskStateData.value?.error = null
        engraveTaskStateData.updateThis()

        //工程占用的宽高mm单位
        val originBounds =
            CanvasGroupRenderer.getRendererListRenderProperty(rendererList).getRenderBounds(RectF())
        val transferConfig = EngraveFlowDataHelper.getOrGenerateTransferConfig(taskId, originBounds)
        val resultDataList = mutableListOf<TransferDataEntity>()
        rendererList.forEach { renderer ->
            transferConfig.name = renderer.lpElementBean()?.name ?: "Untitled"
            val transferDataEntity = LPTransferHelper.transitionRenderer(renderer, transferConfig)
            if (transferDataEntity == null) {
                engraveTaskStateData.value?.state = EngraveState.ENGRAVE_STATE_ERROR
                engraveTaskStateData.value?.error = Throwable("TransferData is null.")
                engraveTaskStateData.updateThis()
                return
            }
            resultDataList.add(transferDataEntity)
        }
        resultDataList.lpSaveAllEntity()
        transferModel.startTransferData(taskId)
    }

    /**开始雕刻*/
    private fun _startEngrave() {
        engraveModel.startEngrave(taskId, ignoreShow = true, enableItemEngraveParams = true)
    }

    /**当前的任务完成*/
    private fun _onTaskFinish() {
    }

    override fun runTask() {
        startEngraveTask()
    }
}

/**雕刻状态*/
data class EngraveState(
    /**雕刻状态*/
    var state: Int = ENGRAVE_STATE_NORMAL,
    /**开始雕刻的时间*/
    var startTime: Long = nowTime(),
    /**雕刻结束时间*/
    var endTime: Long? = null,
    /**当前状态的进度[0~100]*/
    var progress: Int = 0,
    /**错误时的原因*/
    var error: Throwable? = null,
    //---
    /**界面上是否展开了*/
    var isExpand: Boolean = false,
) {
    companion object {
        /**就绪*/
        const val ENGRAVE_STATE_NORMAL = 0

        /**准备, 已经开始了雕刻*/
        const val ENGRAVE_STATE_READY = 1

        /**数据创建中*/
        const val ENGRAVE_STATE_CREATING = ENGRAVE_STATE_READY.shl(1)

        /**数据传输中*/
        const val ENGRAVE_STATE_TRANSMITTING = ENGRAVE_STATE_CREATING.shl(1)

        /**雕刻中*/
        const val ENGRAVE_STATE_ENGRAVING = ENGRAVE_STATE_TRANSMITTING.shl(1)

        /**雕刻暂停*/
        const val ENGRAVE_STATE_PAUSE = ENGRAVE_STATE_ENGRAVING.shl(1)

        /**雕刻完成*/
        const val ENGRAVE_STATE_FINISH = ENGRAVE_STATE_PAUSE.shl(1)

        /**雕刻取消*/
        const val ENGRAVE_STATE_CANCEL = ENGRAVE_STATE_FINISH.shl(1)

        /**雕刻错误*/
        const val ENGRAVE_STATE_ERROR = -1
    }
}