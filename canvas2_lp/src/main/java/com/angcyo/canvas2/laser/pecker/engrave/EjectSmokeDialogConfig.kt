package com.angcyo.canvas2.laser.pecker.engrave

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.angcyo.bluetooth.fsc.laserpacker.parse.isCoverConnect
import com.angcyo.bluetooth.fsc.laserpacker.parse.isCoverConnectAny
import com.angcyo.bluetooth.fsc.laserpacker.parse.isNoSafeKey
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.engrave.EjectSmokeDialogConfig.Companion.ejectSmokeNotPromptDay
import com.angcyo.canvas2.laser.pecker.engrave.EjectSmokeDialogConfig.Companion.ejectSmokeNotPromptDayKey
import com.angcyo.dialog.BaseDialogConfig
import com.angcyo.library._screenHeight
import com.angcyo.library._screenWidth
import com.angcyo.library.annotation.DSL
import com.angcyo.library.component.pad.isInPadMode
import com.angcyo.library.ex._string
import com.angcyo.library.ex.dayNotPrompt
import com.angcyo.library.ex.dpi
import com.angcyo.library.ex.isDayNotPrompt
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.span.span
import kotlin.math.min

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/05/09
 *
 * LP5排烟弹窗
 */
class EjectSmokeDialogConfig(context: Context? = null) : BaseDialogConfig(context) {

    companion object {
        /**默认多少天内不提示*/
        const val ejectSmokeNotPromptDay = 7

        /**不提示的key值*/
        const val ejectSmokeNotPromptDayKey = "EjectSmokeNotPromptDayKey"
    }

    init {
        dialogLayoutId = R.layout.dialog_eject_smoke_layout
        dialogBgDrawable = ColorDrawable(Color.TRANSPARENT)
    }

    override fun initDialogView(dialog: Dialog, dialogViewHolder: DslViewHolder) {
        super.initDialogView(dialog, dialogViewHolder)
        /*val sumTime = vmApp<LaserPeckerModel>().deviceSettingData.value?.fanTime ?: 15
        updateTimeView(dialogViewHolder, sumTime, false)

        dialogViewHolder.view(R.id.time_progress_view)?.apply {
            val circleProgressDrawable = CircleProgressDrawable()
            circleProgressDrawable.enableCover = false
            circleProgressDrawable.startOffsetAngle = 0f
            circleProgressDrawable.startDegrees = 0f
            circleProgressDrawable.coverAngle = 0f
            circleProgressDrawable.currentProgressValue = 0
            circleProgressDrawable.backgroundWidth = 4 * dp
            circleProgressDrawable.progressWidth = circleProgressDrawable.backgroundWidth
            background = circleProgressDrawable

            startCountDown(sumTime.toLong()) {
                if (dialog.isShowing) {
                    val time = (it / 1000f).ceilInt()
                    val progress = 1 - time * 1f / sumTime
                    circleProgressDrawable.updateProgressValue(
                        (progress * 100).ceilInt(), animDuration = 900L
                    )
                    val isFinish = it == 0L
                    updateTimeView(dialogViewHolder, time, isFinish)
                    if (isFinish) {
                        circleProgressDrawable.updateProgressGradientColors(intArrayOf("#2FCA54".toColorInt()))
                    }
                    circleProgressDrawable.invalidateSelf()
                }
            }
        }*/

        /*dialogViewHolder.click(R.id.lib_button) {
            dialog.dismiss()
        }*/

        //发送排烟指令
        /*ExitCmd().enqueue { _, error ->
            if (error != null) {
                EjectSmokeCmd(
                    true,
                    vmApp<LaserPeckerModel>().deviceSettingData.value?.fanLevel ?: 255,
                    sumTime
                ).enqueue { bean, error ->
                    if (error != null) {
                        toastQQOrMessage(error.message)
                    }
                }
            }
        }*/

        //2024-10-18 烫手提示
        dialogViewHolder.tv(R.id.lib_not_prompt_box)?.text =
            " " + _string(R.string.day_not_prompt_label, "$ejectSmokeNotPromptDay")
        dialogViewHolder.click(R.id.lib_button) {
            ejectSmokeNotPromptDayKey.dayNotPrompt(dialogViewHolder.isChecked(R.id.lib_not_prompt_box))
            dialog.dismiss()
        }
    }

    /**更新排烟倒计时*/
    fun updateTimeView(dialogViewHolder: DslViewHolder, time: Int, isFinish: Boolean) {
        if (isFinish) {
            dialogViewHolder.visible(R.id.success_image_view)
            dialogViewHolder.gone(R.id.time_text_view)
            dialogViewHolder.tv(R.id.lib_title_text_view)?.text =
                _string(R.string.eject_smoke_finish)
            dialogViewHolder.tv(R.id.lib_text_view)?.text = _string(R.string.eject_smoke_finish_des)
        } else {
            dialogViewHolder.tv(R.id.time_text_view)?.text = span {
                append("$time")
                append("s") {
                    fontSize = 12 * dpi
                }
            }
        }
    }
}

@DSL
fun Context.checkShowEjectSmokeDialog(): Boolean {
    if (ejectSmokeNotPromptDayKey.isDayNotPrompt(ejectSmokeNotPromptDay)) {
        return false
    }
    /*if (isNoSafeKey || !isCoverConnectAny) {
        return false
    }
    if (!isCoverConnect) {
        //2024-10-21 非合规保护罩, 不弹出提示
        return false
    }*/
    /*val isL5 = vmApp<LaserPeckerModel>().isL5();
    if (!isL5 || isNoSafeKey || !isCoverConnectAny) {
        return false
    }*/
    /*val time = vmApp<LaserPeckerModel>().deviceSettingData.value?.fanTime ?: 15
    if (time <= 0) {
        return false
    }*/
    EjectSmokeDialogConfig(this).run {
        dialogWidth = -1
        if (isInPadMode()) {
            dialogWidth = min(_screenWidth, _screenHeight)
        }
        canceledOnTouchOutside = false
        show()
    }
    return true
}