package com.angcyo.canvas2.laser.pecker.engrave.newflow.dslitem

import com.angcyo.bluetooth.fsc.laserpacker.DeviceStateModel
import com.angcyo.bluetooth.fsc.laserpacker.parse.toDeviceStateString
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave.appendDrawable
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.preview.DeviceInfoTipItem
import com.angcyo.canvas2.laser.pecker.engrave.newflow.EngraveState
import com.angcyo.canvas2.laser.pecker.engrave.newflow.FlowEngraveControl
import com.angcyo.core.vmApp
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.laserpacker.device.toEngraveTime
import com.angcyo.library.ex._color
import com.angcyo.library.ex._string
import com.angcyo.library.ex.dpi
import com.angcyo.library.ex.nowTime
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.span.span

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/19
 *
 * 新流程雕刻状态item
 */
class FlowEngraveStateItem : DslAdapterItem() {

    /**当前的雕刻状态*/
    var itemEngraveState: EngraveState? = null

    /**雕刻流程控制器*/
    var itemFlowEngraveControl: FlowEngraveControl? = null

    val deviceStateModel = vmApp<DeviceStateModel>()

    init {
        itemLayoutId = R.layout.layout_engrave_ing
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)

        itemHolder.visible(
            R.id.pause_button,
            itemEngraveState?.state == EngraveState.ENGRAVE_STATE_ENGRAVING
        )
        itemHolder.visible(
            R.id.resume_button,
            itemEngraveState?.state == EngraveState.ENGRAVE_STATE_PAUSE
        )

        itemHolder.visible(
            R.id.retry_button,
            itemEngraveState?.state == EngraveState.ENGRAVE_STATE_ERROR
        )

        val startTime = itemFlowEngraveControl?.engraveStateData?.value?.startTime ?: nowTime()
        val endTime = itemFlowEngraveControl?.engraveStateData?.value?.endTime ?: nowTime()
        itemHolder.tv(R.id.lib_text_view)?.text = span {

            //判断是否需要设备信息显示. 温度/角度
            val info = DeviceInfoTipItem.deviceInfoTip()
            if (info.isNotBlank()) {
                appendLineIfNotEmpty()
                append(info)
            }

            itemEngraveState?.error?.let {
                appendLineIfNotEmpty()
                append("${it.message}") {
                    foregroundColor = _color(R.color.error)
                }
            }

            //2024-5-29
            if (itemEngraveState?.state != null &&
                itemEngraveState?.state != EngraveState.ENGRAVE_STATE_CANCEL &&
                itemEngraveState?.state != EngraveState.ENGRAVE_STATE_FINISH
            ) {
                deviceStateModel.deviceStateData.value?.toDeviceStateString(false)?.let {
                    if (it.isNotBlank()) {
                        appendLineIfNotEmpty()
                        append(it) {
                            foregroundColor = _color(R.color.error)
                        }
                    }
                }
            }

            appendLineIfNotEmpty()
            append(_string(R.string.task_progress))
            append(" ${itemFlowEngraveControl?.engraveTaskManger?.currentTaskIndex}") {
                foregroundColor = _color(R.color.text_primary_color)
            }
            append("/")
            append(itemFlowEngraveControl?.engraveTaskManger?.taskCount ?: 0)
            appendLine()
            if (itemEngraveState?.state == EngraveState.ENGRAVE_STATE_CANCEL || itemEngraveState?.state == EngraveState.ENGRAVE_STATE_FINISH) {
                append(_string(R.string.total_time))
                append(" ${(endTime - startTime).toEngraveTime()}")
            } else {
                append(_string(R.string.upgrade_duration))
                append(" ${(nowTime() - startTime).toEngraveTime()}")
            }
        }

        if (itemEngraveState?.state == EngraveState.ENGRAVE_STATE_CANCEL || itemEngraveState?.state == EngraveState.ENGRAVE_STATE_FINISH ||
            itemEngraveState?.state == EngraveState.ENGRAVE_STATE_ERROR
        ) {
            itemHolder.tv(R.id.lib_tip_view)?.text =
                if (itemEngraveState?.state == EngraveState.ENGRAVE_STATE_CANCEL) {
                    _string(R.string.engrave_cancel)
                } else if (itemEngraveState?.state == EngraveState.ENGRAVE_STATE_ERROR) {
                    _string(R.string.engrave_fail)
                } else {
                    _string(R.string.engrave_finish)
                }
        }

        val drawableSize = 24 * dpi

        itemHolder.tv(R.id.pause_button)?.text = span {
            appendDrawable(R.drawable.pause_engrave_svg, drawableSize)
            append(_string(R.string.engrave_pause))
        }

        itemHolder.tv(R.id.resume_button)?.text = span {
            appendDrawable(R.drawable.resume_engrave_svg, drawableSize)
            append(_string(R.string.engrave_continue))
        }

        itemHolder.tv(R.id.cancel_button)?.text = span {
            appendDrawable(R.drawable.cancel_engrave_svg, drawableSize)
            append(_string(R.string.cancel_engrave))
        }
    }
}