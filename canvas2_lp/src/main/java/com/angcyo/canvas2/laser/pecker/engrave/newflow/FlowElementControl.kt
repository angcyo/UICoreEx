package com.angcyo.canvas2.laser.pecker.engrave.newflow

import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.bluetooth.fsc.laserpacker._deviceSettingBean
import com.angcyo.bluetooth.fsc.laserpacker.bean.LayerColorBean
import com.angcyo.canvas.render.core.CanvasRenderDelegate
import com.angcyo.canvas.render.renderer.BaseRenderer
import com.angcyo.canvas.render.renderer.CanvasElementRenderer
import com.angcyo.canvas.render.renderer.CanvasGroupRenderer
import com.angcyo.canvas2.laser.pecker.dslitem.ICanvasRendererItem
import com.angcyo.canvas2.laser.pecker.util.lpElementBean
import com.angcyo.dsladapter.DslAdapter
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.laserpacker.bean.LPLaserOptionsBean
import com.angcyo.laserpacker.bean.toLaserOptionsBean
import com.angcyo.laserpacker.bean.updateFromLaserOptionsBean
import com.angcyo.library.annotation.CallPoint
import com.angcyo.library.canvas.core.Reason

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/16
 *
 * 画布元素控制
 */
class FlowElementControl {

    var renderDelegate: CanvasRenderDelegate? = null

    /**是否处于编辑模式*/
    var isEditModel = false

    /**初始化*/
    @CallPoint
    fun initControl(renderDelegate: CanvasRenderDelegate?) {
        this.renderDelegate = renderDelegate
    }

    /**获取画布元素列表
     *
     * [com.angcyo.canvas.render.core.CanvasRenderManager.getAllElementRendererList]
     * */
    fun getCanvasElementList(
        dissolveGroup: Boolean = false,
        includeGroup: Boolean = false
    ): List<BaseRenderer> {
        return renderDelegate?.renderManager?.getAllElementRendererList(dissolveGroup, includeGroup)
            ?: emptyList()
    }

    /**获取所有简单的元素*/
    fun getAllSingleElementList(): List<BaseRenderer> {
        return getCanvasElementList(true)
    }

    /**获取所有选中的元素, 只返回单元素数据*/
    fun getAllSelectElementList(): List<BaseRenderer> {
        return getCanvasElementList(true).filter { it.isSelected }
    }

    /**获取所有需要雕刻的元素*/
    fun getAllEngraveElementList(): List<BaseRenderer> {
        return getCanvasElementList(true).filter { it.isVisible }
    }

    /**获取所有画布元素的选中状态*/
    fun getAllElementSelectState(): Int {
        return getAllElementSelectState(getCanvasElementList())
    }

    fun getAllElementSelectState(rendererList: List<BaseRenderer>): Int {
        return getAllSelectState(rendererList) {
            if (it is CanvasGroupRenderer)
                getAllElementSelectState(it.rendererList) == 1
            else
                it.isSelected
        }
    }

    /**获取所有[DslAdapterItem]选中的状态
     * -1: 全部未选中
     * 0: 部分选中
     * 1: 全部选中*/
    fun getAllItemSelectState(adapter: DslAdapter): Int {
        /*val rendererList = getCanvasElementList()
        if (rendererList.isEmpty()) {
            return -1
        }
        var selectedState = -1
        rendererList.forEach { renderer ->

        }*/
        return getAllItemSelectState(adapter.getDataList(false))
    }

    fun getAllItemSelectState(itemList: List<DslAdapterItem>): Int {
        return getAllSelectState(itemList) { it.itemIsSelected }
    }

    fun <T> getAllSelectState(itemList: List<T>, isSelectedAction: (T) -> Boolean): Int {
        var selectedState: Int? = null
        if (itemList.isEmpty()) {
            return selectedState ?: -1
        }
        itemList.forEach { item ->
            if (selectedState == null) {
                selectedState = if (isSelectedAction(item)) 1 else -1
            } else if (isSelectedAction(item)) {
                if (selectedState != 1) {
                    selectedState = 0
                }
            } else {
                if (selectedState != -1) {
                    selectedState = 0
                }
            }
        }
        return selectedState ?: -1
    }

    fun updateElementSelectState(adapter: DslAdapter, selectState: Int) {
        updateElementSelectState(adapter.getDataList(false), selectState)
    }

    /**更新元素选择状态*/
    fun updateElementSelectState(itemList: List<DslAdapterItem>, selectState: Int) {
        itemList.forEach { item ->
            if (item is ICanvasRendererItem) {
                //只做状态的赋值改变, 不更新界面
                item.itemRenderer?.isSelected = selectState == 1
            }
            updateElementSelectState(item.itemSubList, selectState)

            //更新状态, 并触发回调更新界面
            //item.itemIsSelected = selectState == 1
        }
    }

    /**清除所有元素的选中状态*/
    fun clearElementSelectState() {
        clearElementSelectState(getCanvasElementList())
    }

    fun clearElementSelectState(rendererList: List<BaseRenderer>) {
        rendererList.forEach {
            it.isSelected = false
            if (it is CanvasGroupRenderer) {
                clearElementSelectState(it.rendererList)
            }
        }
    }

    /**所有选中的元素是否是同一类型的数据*/
    fun isSameTypeElementSelected(): Boolean {
        val rendererList = getAllSelectElementList()
        var firstLayerMode: Int? = null
        for (renderer in rendererList) {
            val layerMode = renderer.lpElementBean()?._layerMode
            if (firstLayerMode == null) {
                firstLayerMode = layerMode
            } else {
                if (firstLayerMode != layerMode) {
                    return false
                }
            }
        }
        return true
    }

    /**2种颜色图层的数据类型是否一致*/
    fun isSameDataModeLayerColor(layerColor1: String?, layerColor2: String?): Boolean {
        if (layerColor1 == null || layerColor2 == null) {
            return true
        }
        val optionBean1 = layerOptionMap[layerColor1]
        val optionBean2 = layerOptionMap[layerColor2]
        if (optionBean1 == null || optionBean2 == null) {
            return true
        }
        return isSameDataMode(optionBean1.dataMode, optionBean2.dataMode)
    }

    fun isSameDataMode(dataMode1: Int?, dataMode2: Int?): Boolean {
        if (dataMode1 == null || dataMode2 == null) {
            return true
        }
        return dataMode1 == dataMode2
    }

    /**所有选中元素是否都可见*/
    fun isAllSelectedElementVisible(): Boolean {
        return isAllElementVisible(getAllSelectElementList())
    }

    /**所有元素是否都可见*/
    fun isAllElementVisible(rendererList: List<BaseRenderer> = getAllSingleElementList()): Boolean {
        return rendererList.all { it.isVisible }
    }

    /**所有选中元素是否都不可见*/
    fun isAllElementInvisible(rendererList: List<BaseRenderer> = getAllSingleElementList()): Boolean {
        return rendererList.all { !it.isVisible }
    }

    /**隐藏所有选中元素*/
    fun hideAllSelectedElement(hide: Boolean) {
        hideAllElement(getAllSelectElementList(), hide)
    }

    /**隐藏所有元素*/
    fun hideAllElement(
        rendererList: List<BaseRenderer> = getAllSingleElementList(),
        hide: Boolean
    ) {
        rendererList.forEach { renderer ->
            renderer.updateVisible(!hide, Reason.user, renderDelegate)
            if (renderer is CanvasGroupRenderer) {
                hideAllElement(renderer.rendererList, hide)
            }
        }
    }

    //---

    /**颜色图层*/
    val layerList = mutableSetOf<LayerColorBean>()

    /**颜色图层对应的雕刻参数
     * [LayerColorBean.color] -> [LPLaserOptionsBean]
     * */
    val layerOptionMap = mutableMapOf<String, LPLaserOptionsBean>()

    /**初始化已有的颜色图层信息, 和默认的雕刻参数*/
    @CallPoint
    fun initColorLayerInfo() {
        layerList.clear()
        layerOptionMap.clear()
        val rendererList = getAllSingleElementList()
        rendererList.forEach { renderer ->
            if (renderer is CanvasElementRenderer) {
                val elementBean = renderer.lpElementBean()
                if (elementBean != null) {
                    //默认的核心雕刻参数
                    elementBean.printPower = elementBean.printPower ?: HawkEngraveKeys.lastPower
                    elementBean.printDepth = elementBean.printDepth ?: HawkEngraveKeys.lastDepth
                    elementBean.printCount = elementBean.printCount ?: 1
                    //颜色图层
                    elementBean.layerColor?.let { layerColor ->
                        val layerColorBean =
                            _deviceSettingBean?.layerColorList?.find { it.color == layerColor }
                        if (layerColorBean != null) {
                            layerList.add(layerColorBean)
                            layerOptionMap[layerColor] = elementBean.toLaserOptionsBean()
                        }
                    }
                }
            }
        }
    }

    /**将[rendererList]元素更新到指定颜色图层
     * 如果数据类型不匹配, 则返回false*/
    fun updateElementColorLayer(rendererList: List<BaseRenderer>, bean: LayerColorBean): Boolean {
        var firstDataModel: Int? = null
        for (renderer in rendererList) {
            val elementBean = renderer.lpElementBean()
            if (elementBean != null) {
                firstDataModel = elementBean._layerMode
                break
            }
        }
        //移至到一个可能已经存在的数据类型颜色图层上
        val layerColor = bean.color
        if (!isSameDataMode(firstDataModel, layerOptionMap[layerColor]?.dataMode)) {
            return false
        }
        rendererList.forEach { renderer ->
            var optionBean = layerOptionMap[layerColor]
            renderer.lpElementBean()?.apply {
                this.layerColor = bean.color
                if (layerColor.isNullOrEmpty()) {
                    //清空颜色图层信息
                } else {
                    if (optionBean == null) {
                        //颜色图层还未初始化参数, 则默认使用第一个元素的参数
                        optionBean = toLaserOptionsBean()
                        layerOptionMap[layerColor] = optionBean!!
                    }
                    updateFromLaserOptionsBean(optionBean)
                }
            }
        }
        return true
    }

    /**更新指定颜色图层的参数, 此时应该同时更新相同颜色图层的所有元素参数*/
    fun updateLayerColorOptions(color: String?, bean: LPLaserOptionsBean) {
        if (color.isNullOrEmpty()) {
            return
        }
        layerOptionMap[color] = bean
        val rendererList = getAllSingleElementList().filter {
            it.lpElementBean()?.layerColor == color
        }
        rendererList.forEach { renderer ->
            renderer.lpElementBean()?.updateFromLaserOptionsBean(bean)
        }
    }
}