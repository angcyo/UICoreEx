package com.angcyo.canvas2.laser.pecker.engrave.newflow

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import com.angcyo.bluetooth.fsc.laserpacker.bean.LayerColorBean
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.library.ex.alphaRatio
import com.angcyo.library.ex.createPaint
import com.angcyo.library.ex.dp
import com.angcyo.library.ex.dpi
import com.angcyo.library.ex.randomColor
import com.angcyo.library.ex.textHeight
import com.angcyo.library.ex.toColorInt
import com.angcyo.library.ex.toHexColorString
import kotlin.math.min
import kotlin.random.Random

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/16
 *
 * 图层颜色绘制
 */
class LayerColorView(context: Context, attributeSet: AttributeSet? = null) :
    View(context, attributeSet) {

    /**是否要显示图层名称*/
    var showLayerName: Boolean = true

    /**是否激活选中状态样式*/
    var enableSelectState: Boolean = true

    var layerColor: LayerColorBean? = null
    val paint = createPaint(Color.MAGENTA, Paint.Style.FILL)

    val offsetSize = 4 * dpi

    private val drawRect = Rect()
    private var _noColorDrawable: Drawable? = null

    /**无颜色的状态*/
    val isNoColorState get() = layerColor != null && layerColor!!.color == null && layerColor!!.name == null

    init {
        if (isInEditMode) {
            layerColor =
                LayerColorBean(randomColor().toHexColorString(), "${Random.nextInt(0, 30)}")
        }
    }

    fun getNoColorDrawable(): Drawable {
        if (_noColorDrawable == null) {
            _noColorDrawable =
                AppCompatResources.getDrawable(context, R.drawable.canvas_layer_no_color)
        }
        return _noColorDrawable!!
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        val layerColor = this.layerColor ?: return

        drawRect.set(
            paddingLeft,
            paddingTop,
            measuredWidth - paddingRight,
            measuredHeight - paddingBottom
        )

        val cx = drawRect.centerX()
        val cy = drawRect.centerY()
        val radius = min(drawRect.width(), drawRect.height()) / 2f
        val offsetSize = if (enableSelectState) offsetSize else 0

        //选中背景提示
        if (enableSelectState && isSelected) {
            paint.color = (layerColor.color?.toColorInt() ?: Color.BLACK).alphaRatio(0.5f)
            canvas.drawCircle(cx.toFloat(), cy.toFloat(), radius, paint)
        }

        //颜色状态
        if (layerColor.color != null) {
            paint.color = layerColor.color!!.toColorInt()
            canvas.drawCircle(cx.toFloat(), cy.toFloat(), radius - offsetSize, paint)
        }
        if (isNoColorState) {
            getNoColorDrawable().apply {
                drawRect.inset(offsetSize, offsetSize)
                bounds = drawRect
                draw(canvas)
            }
        }

        //绘制name
        if (showLayerName && !layerColor.name.isNullOrEmpty()) {
            paint.color = Color.WHITE
            paint.textSize = 12 * dp
            paint.textAlign = Paint.Align.CENTER
            val text = layerColor.name!!
            canvas.drawText(
                text,
                cx.toFloat(),
                cy + paint.textHeight() / 2 - paint.descent(),
                paint
            )
        }
    }

}