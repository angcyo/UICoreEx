package com.angcyo.canvas2.laser.pecker.engrave.newflow

import com.angcyo.dialog.BaseRecyclerDialogConfig

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/17
 *
 * 新的雕刻对话框
 */
class NewEngraveDialog : BaseRecyclerDialogConfig() {
}