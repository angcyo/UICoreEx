package com.angcyo.canvas2.laser.pecker.engrave.newflow.dslitem

import android.view.View
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.dslitem.CanvasLayerItem
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.dsladapter.item.IExtendItem
import com.angcyo.laserpacker.toTypeNameString
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.span.span

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/16
 *
 * [FlowLayerElementItem]
 * [FlowLayerGroupItem]
 * [CanvasLayerItem]
 *
 * [FlowEngraveElementItem]
 * [FlowEngraveGroupElementItem]
 */
class FlowLayerGroupItem : FlowLayerElementItem(), IExtendItem {

    /**内部子元素是否全部隐藏了*/
    var itemGroupElementHide: Boolean = false

    override val itemLayerHide: Boolean
        get() = super.itemLayerHide && itemGroupElementHide

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)

        //item 名称
        itemHolder.tv(R.id.layer_item_name_view)?.text = span {
            append(itemItemName ?: operateElementBean?.mtype?.toTypeNameString())
            append("($childCount)")
        }

        initExtendItem(
            itemHolder,
            itemPosition,
            adapterItem,
            payloads,
            R.id.layer_group_arrow_view,
            foldRotation = -90f,
            toExtendRotation = 90f,
            toFoldRotation = -90f,
            triggerViewIdList = listOf(R.id.layer_group_arrow_view, View.NO_ID)
        )

        itemHolder.visible(R.id.layer_item_invisible_view, true)
    }

    override fun onSetItemSelected(select: Boolean) {
        if (isGroupRenderer) {
            itemSubList.forEach { subItem ->
                subItem.itemIsSelected = select
            }
        }
        super.onSetItemSelected(select)
    }

    override fun onSelfVisibleChange(visible: Boolean) {
        if (isGroupRenderer) {
            itemSubList.forEach { subItem ->
                if (subItem is CanvasLayerItem) {
                    subItem.onSelfVisibleChange(visible)
                }
            }
        }
        super.onSelfVisibleChange(visible)
    }
}