package com.angcyo.canvas2.laser.pecker.dialog

import android.app.Dialog
import android.content.Context
import com.angcyo.bluetooth.fsc.IReceiveBeanAction
import com.angcyo.bluetooth.fsc.enqueue
import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerModel
import com.angcyo.bluetooth.fsc.laserpacker._deviceConfigBean
import com.angcyo.bluetooth.fsc.laserpacker._deviceSettingBean
import com.angcyo.bluetooth.fsc.laserpacker.command.CoverSettingCmd
import com.angcyo.bluetooth.fsc.laserpacker.command.EngravePreviewCmd
import com.angcyo.bluetooth.fsc.laserpacker.parse.isCoverConnect
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.preview.PreviewBracketItem
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.preview.PreviewControlItem
import com.angcyo.canvas2.laser.pecker.util.LPConstant
import com.angcyo.canvas2.laser.pecker.util.mmToRenderUnitValue
import com.angcyo.core.CoreApplication
import com.angcyo.core.vmApp
import com.angcyo.dialog.BaseDialogConfig
import com.angcyo.dialog.configBottomDialog
import com.angcyo.item.keyboard.numberKeyboardDialog
import com.angcyo.laserpacker.device.ble.DeviceSettingFragment
import com.angcyo.library.annotation.DSL
import com.angcyo.library.annotation.MM
import com.angcyo.library.ex._string
import com.angcyo.library.ex.clamp
import com.angcyo.library.toast
import com.angcyo.library.unit.toMm
import com.angcyo.library.unit.toPixel
import com.angcyo.library.unit.unitDecimal
import com.angcyo.widget.DslViewHolder

/**
 * 保护罩材料高度弹窗/材料厚度
 *
 * [PreviewControlItem] 预览控制
 * [PreviewBracketItem] 支架高度控制
 *
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/14
 */
class CoverMaterialHeightDialogConfig(context: Context? = null) : BaseDialogConfig(context) {

    val digit = 2

    init {
        dialogLayoutId = R.layout.dialog_cover_material_height_layout
    }

    override fun initDialogView(dialog: Dialog, dialogViewHolder: DslViewHolder) {
        super.initDialogView(dialog, dialogViewHolder)

        // 帮助文档
        val helpUrl = DeviceSettingFragment.getHelpUrl(
            _deviceSettingBean?.materialHeightHelpUrl, _deviceSettingBean?.materialHeightHelpUrlZh
        )
        dialogViewHolder.visible(R.id.cover_material_height_help_view, helpUrl != null)
        if (helpUrl != null) {
            dialogViewHolder.click(R.id.cover_material_height_help_view) {
                CoreApplication.onOpenUrlAction?.invoke(helpUrl)
            }
        }

        // 单位
        val valueUnit = LPConstant.renderUnit
        dialogViewHolder.tv(R.id.cover_unit_label_view)?.text = "(${valueUnit.getUnit()})"
        updateMaterialHeight(dialogViewHolder)

        // 确定
        dialogViewHolder.click(R.id.confirm_button) {
            bracketAutoCmd(true, HawkEngraveKeys.lastCoverMaterialHeight) { bean, error ->
                if (error == null) {
                    if (isCoverConnect && bean?.receivePacket?.getOrNull(5)
                            ?.toInt() == 1
                    ) {
                        //支架运动中
                        toast(
                            _string(R.string.cover_bracket_move_tip),
                            icon = R.drawable.toast_warn
                        )
                    } else {
                        dialog.dismiss()
                    }
                }
            }
        }

        //材料高度
        dialogViewHolder.click(R.id.cover_height_view) {
            it.context.numberKeyboardDialog {
                val exMode = vmApp<LaserPeckerModel>().getExDeviceMode()

                @MM val coverMaterialMinHeight = _deviceConfigBean?.coverMaterialMinHeight ?: 0f
                val coverMaterialMaxHeight = _deviceConfigBean?.materialHeightMap?.get("$exMode")
                    ?: _deviceConfigBean?.coverMaterialMaxHeight ?: 150f
                val coverMaterialHeightMinValue =
                    valueUnit.convertPixelToValue(coverMaterialMinHeight.toPixel())
                val coverMaterialHeightMaxValue =
                    valueUnit.convertPixelToValue(coverMaterialMaxHeight.toPixel())

                @MM if (HawkEngraveKeys.lastCoverMaterialHeight < coverMaterialMinHeight) {
                    HawkEngraveKeys.lastCoverMaterialHeight = coverMaterialMinHeight
                } else if (HawkEngraveKeys.lastCoverMaterialHeight > coverMaterialMaxHeight) {
                    HawkEngraveKeys.lastCoverMaterialHeight = coverMaterialMaxHeight
                }

                numberMinValue = coverMaterialHeightMinValue.toInt()
                numberMaxValue = coverMaterialHeightMaxValue.toInt()
                decimalCount = digit
                numberValue = HawkEngraveKeys.lastCoverMaterialHeight.mmToRenderUnitValue()

                onNumberResultAction = { number ->
                    if (number != null) {
                        val numberPixel = valueUnit.convertValueToPixel(number as Float)
                        var size = numberPixel.toMm()
                        size = clamp(size, coverMaterialHeightMinValue, coverMaterialHeightMaxValue)
                        HawkEngraveKeys.lastCoverMaterialHeight = size

                        updateMaterialHeight(dialogViewHolder)
                    }
                    false
                }
            }
        }
    }

    fun updateMaterialHeight(dialogViewHolder: DslViewHolder) {
        val valueUnit = LPConstant.renderUnit
        val materialHeightPixel = HawkEngraveKeys.lastCoverMaterialHeight.toPixel()
        val materialHeightValue = valueUnit.convertPixelToValue(materialHeightPixel)
        dialogViewHolder.tv(R.id.cover_height_view)?.text = materialHeightValue.unitDecimal(digit)
    }

    /**自动移动支架*/
    fun bracketAutoCmd(
        isCoverConnect: Boolean = true,
        @MM height: Float,
        action: IReceiveBeanAction? = null,
    ) {
        //基础焦距
        val focal = (_deviceConfigBean?.focalDistance
            ?: 0).toFloat() + (_deviceConfigBean?.focalDistanceCompensate ?: 0f)

        //配件补偿焦距
        val laserPeckerModel = vmApp<LaserPeckerModel>()
        val exFocal = if (laserPeckerModel.isROpen()) {
            (_deviceConfigBean?.focalDistanceRFlagCompensate
                ?: 0f) + HawkEngraveKeys.lastDiameterPixel.toMm() / 2
        } else if (laserPeckerModel.isZOpen()) {
            _deviceConfigBean?.focalDistanceZFlagCompensate ?: 0f
        } else if (laserPeckerModel.isSOpen() || laserPeckerModel.isSRepMode()) {
            _deviceConfigBean?.focalDistanceSFlagCompensate ?: 0f
        } else {
            0f
        }
        val cmd = if (isCoverConnect) CoverSettingCmd.coverBracketAutoCmd(
            height,
            focal + exFocal,
        ) else EngravePreviewCmd.previewBracketStopCmd()
        cmd.enqueue { bean, error ->
            action?.invoke(bean, error)
        }
    }
}

@DSL
fun Context.coverMaterialHeightDialogConfig(config: CoverMaterialHeightDialogConfig.() -> Unit): Dialog {
    return CoverMaterialHeightDialogConfig().run {
        configBottomDialog(this@coverMaterialHeightDialogConfig)
        config()
        show()
    }
}
