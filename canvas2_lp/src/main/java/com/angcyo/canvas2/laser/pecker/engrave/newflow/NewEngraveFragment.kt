package com.angcyo.canvas2.laser.pecker.engrave.newflow

import android.os.Bundle
import com.angcyo.DslFHelper
import com.angcyo.base.removeThis
import com.angcyo.bluetooth.fsc.laserpacker.DeviceStateModel
import com.angcyo.canvas.render.renderer.CanvasElementRenderer
import com.angcyo.canvas.render.renderer.CanvasGroupRenderer
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.engrave.CoverFlameAlarmDialogConfig
import com.angcyo.canvas2.laser.pecker.engrave.LpStateCheckHelper
import com.angcyo.canvas2.laser.pecker.engrave.checkShowEjectSmokeDialog
import com.angcyo.canvas2.laser.pecker.engrave.newflow.dslitem.FlowEngraveElementItem
import com.angcyo.canvas2.laser.pecker.engrave.newflow.dslitem.FlowEngraveGroupElementItem
import com.angcyo.canvas2.laser.pecker.engrave.newflow.dslitem.FlowEngraveStateItem
import com.angcyo.canvas2.laser.pecker.util.lpElementBean
import com.angcyo.core.fragment.BaseDslFragment
import com.angcyo.core.vmApp
import com.angcyo.engrave2.model.EngraveModel
import com.angcyo.library.ex._color
import com.angcyo.library.ex._colorDrawable
import com.angcyo.library.ex._string
import com.angcyo.library.ex.replace
import com.angcyo.library.toast
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.base.resetDslItem

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/17
 * 新的雕刻流程界面, 控制具体的雕刻过程
 */
class NewEngraveFragment : BaseDslFragment() {

    /**流程控制*/
    var flowElementControl: FlowElementControl? = null

    /**雕刻控制*/
    val flowEngraveControl: FlowEngraveControl = FlowEngraveControl()

    val deviceStateModel = vmApp<DeviceStateModel>()

    init {
        fragmentTitle = _string(R.string.engrave)
        fragmentLayoutId = R.layout.new_engrave_layout
        enableSoftInput = false

        fragmentConfig.isLightStyle = true
        fragmentConfig.showTitleLineView = true
    }

    override fun onBackPressed(): Boolean {
        if (_lastEngraveState == EngraveState.ENGRAVE_STATE_TRANSMITTING) {
            return false
        }
        return super.onBackPressed()
    }

    override fun initFHelperOnBack(helper: DslFHelper) {
        helper.anim(0, R.anim.lib_translate_y_remove_exit)
    }

    override fun initBaseView(savedInstanceState: Bundle?) {
        fragmentConfig.fragmentBackgroundDrawable =
            _colorDrawable(_color(R.color.lib_theme_white_color))
        super.initBaseView(savedInstanceState)

        if (flowElementControl == null) {
            closeThisFragment()
            toast("data error!")
        } else {
            //异常暂停监听, 需要更新底部控制按钮
            deviceStateModel.queryErrorData.observeIt(allowBackward = false) { queryError ->
                if (queryError != null && queryError > 0) {
                    if (deviceStateModel.deviceStateData.value?.isEngravePause() == true) {
                        //出现错误之后, 检查是否要将当前的雕刻状态设为暂停
                        val engraveState = flowEngraveControl.engraveStateData.value?.state
                        if (engraveState == EngraveState.ENGRAVE_STATE_ENGRAVING) {
                            flowEngraveControl.engraveStateData.value?.state =
                                EngraveState.ENGRAVE_STATE_PAUSE
                        }
                    }
                }
                updateEngraveStateLayout()
            }

            //火焰检测监听
            deviceStateModel.flameAlarmData.observeIt(allowBackward = false) { flameState ->
                if (flameState != null) {
                    if (deviceStateModel.deviceStateData.value?.isEngravePause() == true) {
                        //出现火焰之后, 检查是否要将当前的雕刻状态设为暂停
                        val engraveState = flowEngraveControl.engraveStateData.value?.state
                        if (engraveState == EngraveState.ENGRAVE_STATE_ENGRAVING) {
                            flowEngraveControl.engraveStateData.value?.state =
                                EngraveState.ENGRAVE_STATE_PAUSE
                        }
                    }
                    updateEngraveStateLayout()
                }
            }

            //雕刻状态监听
            flowEngraveControl.engraveStateData.observeIt(allowBackward = false) {
                updateEngraveStateLayout()
                _adapter.updateAllItem()
            }
            flowEngraveControl.initControl(
                flowElementControl?.getCanvasElementList(false)?.filter { it.isVisible })

            updateEngraveStateLayout()
            updateDslAdapter()
        }
        vmApp<EngraveModel>().isInNewFlow = true

        //火焰报警
        CoverFlameAlarmDialogConfig.observer(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        vmApp<EngraveModel>().isInNewFlow = false
        flowEngraveControl.release()
    }

    /**调用此方法重新渲染界面*/
    fun updateDslAdapter() {
        val rendererList = flowEngraveControl.engraveRendererList
        renderDslAdapter(true) {
            rendererList?.forEach { renderer ->
                if (renderer is CanvasGroupRenderer) {
                    FlowEngraveGroupElementItem()() {
                        itemRenderer = renderer
                        itemSubList = mutableListOf()
                        renderer.rendererList.filter { it.isVisible }.forEach { subRender ->
                            val subItem = FlowEngraveElementItem().apply {
                                itemIndent = true
                                itemRenderer = subRender
                                itemGroupRenderer = renderer
                                itemUpdateAction {
                                    itemEngraveState =
                                        flowEngraveControl.getElementEngraveState(subRender.lpElementBean()?.uuid)
                                }
                            }
                            itemSubList.add(subItem)
                        }
                        itemGroupExtend = renderer.isExpand ?: true
                        observeItemGroupExtendChange {
                            renderer.isExpand = itemGroupExtend
                        }
                    }
                } else if (renderer is CanvasElementRenderer) {
                    FlowEngraveElementItem()() {
                        itemRenderer = renderer
                        itemUpdateAction {
                            itemEngraveState =
                                flowEngraveControl.getElementEngraveState(renderer.lpElementBean()?.uuid)
                        }
                    }
                }
            }
        }
    }

    /**最后一次记录的雕刻状态, 用于排烟弹窗检测*/
    private var _lastEngraveState: Int = -1

    /**更新雕刻状态布局
     *
     * 雕刻正常
     * 雕刻中
     * 雕刻暂停
     * 雕刻完成
     * 雕刻取消
     *
     * */
    fun updateEngraveStateLayout() {
        _vh.group(R.id.bottom_control_wrap_layout)?.apply {
            val viewHolder = DslViewHolder(this)
            val engraveState = flowEngraveControl.engraveStateData.value
            when (engraveState?.state) {
                EngraveState.ENGRAVE_STATE_NORMAL -> {
                    replace(R.layout.layout_engrave_ready)
                    viewHolder.click(R.id.start_engrave_button) {
                        LpStateCheckHelper.checkStartEngraveState(fContext()) {
                            flowEngraveControl.startEngraveTask()
                        }
                    }
                }

                EngraveState.ENGRAVE_STATE_READY -> {
                    removeAllViews()
                }

                EngraveState.ENGRAVE_STATE_CREATING, EngraveState.ENGRAVE_STATE_TRANSMITTING -> {
                    if (engraveState.error == null) {
                        replace(R.layout.layout_engrave_transmitting)
                        viewHolder.tv(R.id.lib_text_view)
                            ?.setText(if (engraveState.state == EngraveState.ENGRAVE_STATE_CREATING) R.string.creating else R.string.transmitting)
                        viewHolder.click(R.id.lib_button) {}
                    } else {
                        replace(R.layout.layout_engrave_transfer_error)
                        viewHolder.click(R.id.lib_button) {
                            flowEngraveControl.resumeEngraveTask()
                        }
                    }
                }

                EngraveState.ENGRAVE_STATE_ENGRAVING, EngraveState.ENGRAVE_STATE_PAUSE -> {
                    if (engraveState.state == EngraveState.ENGRAVE_STATE_ENGRAVING) {
                        fragmentTitle = _string(R.string.is_engraving)
                    } else {
                        fragmentTitle = _string(R.string.engrave_pause)
                    }

                    resetDslItem(FlowEngraveStateItem().apply {
                        itemLayoutId = R.layout.layout_engrave_ing
                        itemEngraveState = engraveState
                        itemFlowEngraveControl = flowEngraveControl
                    })

                    viewHolder.click(R.id.pause_button) {
                        flowEngraveControl.pauseEngraveTask()
                    }

                    viewHolder.click(R.id.resume_button) {
                        if ((deviceStateModel.queryErrorData.value ?: 0) > 0) {
                            //还有错误, 则不处理
                        } else {
                            flowEngraveControl.resumeEngraveTask()
                        }
                    }

                    viewHolder.click(R.id.cancel_button) {
                        flowEngraveControl.cancelEngraveTask()
                    }
                }

                EngraveState.ENGRAVE_STATE_CANCEL, EngraveState.ENGRAVE_STATE_ERROR, EngraveState.ENGRAVE_STATE_FINISH -> {
                    fragmentTitle = _string(R.string.engrave_finish)

                    resetDslItem(FlowEngraveStateItem().apply {
                        itemLayoutId =
                            if (engraveState.state == EngraveState.ENGRAVE_STATE_FINISH) R.layout.layout_engrave_finish //雕刻完成的布局
                            else R.layout.layout_engrave_cancel //取消雕刻的布局
                        itemEngraveState = engraveState
                        itemFlowEngraveControl = flowEngraveControl
                    })

                    viewHolder.click(R.id.lib_button) {
                        //返回
                        //back()
                        removeThis {
                            remove(NewFlowConfigFragment::class)
                        }
                    }

                    viewHolder.click(R.id.retry_button) {
                        LpStateCheckHelper.checkStartEngraveState(fContext()) {
                            flowEngraveControl.resumeEngraveTask()
                        }
                    }

                    //雕刻完成, 也支持再雕一次
                    if (engraveState.state == EngraveState.ENGRAVE_STATE_FINISH) {
                        //雕刻完成...触发变量文本刷新
                        vmApp<EngraveModel>().engraveFinishOnceData.postValue(true)

                        viewHolder.visible(R.id.retry_button, true)
                        viewHolder.click(R.id.retry_button) {
                            LpStateCheckHelper.checkStartEngraveState(fContext()) {
                                _lastEngraveState = -1
                                flowEngraveControl.startEngraveTask(true)
                            }
                        }
                    }

                    if (_lastEngraveState != engraveState.state) {
                        //排烟
                        fContext().checkShowEjectSmokeDialog()
                    }
                    _lastEngraveState = engraveState.state
                }

                else -> {
                    removeAllViews()
                }
            }
        }
    }
}