package com.angcyo.canvas2.laser.pecker.engrave

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import com.angcyo.bluetooth.fsc.IReceiveBeanAction
import com.angcyo.bluetooth.fsc.enqueue
import com.angcyo.bluetooth.fsc.laserpacker.DeviceStateModel
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerModel
import com.angcyo.bluetooth.fsc.laserpacker.command.ExitCmd
import com.angcyo.bluetooth.fsc.laserpacker.parse.isCoverConnectAny
import com.angcyo.bluetooth.fsc.laserpacker.parse.isCoverUnsafeState
import com.angcyo.bluetooth.fsc.laserpacker.parse.isEuropeanStandard
import com.angcyo.bluetooth.fsc.laserpacker.parse.isNoSafeKey
import com.angcyo.bluetooth.fsc.laserpacker.parse.toDeviceStr
import com.angcyo.canvas2.laser.pecker.BuildConfig
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.core.vmApp
import com.angcyo.dialog.messageDialog
import com.angcyo.dialog.toastQQOrMessage
import com.angcyo.laserpacker.device.EngraveNotifyHelper
import com.angcyo.laserpacker.device.ble.DeviceConnectTipActivity
import com.angcyo.laserpacker.device.engraveLoadingAsyncTimeout
import com.angcyo.library.component.isNotificationsEnabled
import com.angcyo.library.component.openNotificationSetting
import com.angcyo.library.ex._drawable
import com.angcyo.library.ex._string
import com.angcyo.library.ex.hawkGetBoolean
import com.angcyo.library.ex.hawkPut
import com.angcyo.library.ex.syncSingle
import com.angcyo.library.getAppVersionCode
import com.angcyo.widget.span.span

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/05/09
 *
 * 状态弹窗提示
 */
object LpStateCheckHelper {

    /**开始雕刻前的状态检查
     * [context] [LifecycleOwner]
     * [action] 当可以雕刻时, 执行的操作
     * @return true 表示可以开始雕刻
     * */
    fun checkStartEngraveState(context: Context?, action: () -> Unit) {
        checkEngraveNotify(context) { //通知
            checkExDevice(context) { //外设
                showFocalDistance(context) { //焦距
                    showSafetyTips(context) { //安全提示
                        checkQuerySafeKey(context) { //安全密钥
                            checkCoverState(context) { //保护罩
                                if (context is LifecycleOwner) {
                                    asyncTimeoutExitCmd(context) { bean, error ->
                                        if (error == null) {
                                            action()
                                        } else {
                                            toastQQOrMessage(error.message)
                                        }
                                    }
                                } else {
                                    action()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**发送查询指令, 查询当前是否插入了安全密钥*/
    fun checkQuerySafeKey(context: Context?, action: () -> Unit) {
        val deviceStateModel = vmApp<DeviceStateModel>()
        deviceStateModel.queryDeviceState { bean, error -> //重查设备状态
            if (error == null) {
                checkSafeKey(context) { //安全密钥
                    action()
                }
            } else {
                toastQQOrMessage(error.message)
            }
        }
    }

    /**检查雕刻通知*/
    private fun checkEngraveNotify(context: Context?, action: () -> Unit) {
        if (BaseFlowLayoutHelper._isCheckedEngraveNotify) {
            action()
        } else {
            EngraveNotifyHelper.createEngraveChannel()
            if (!isNotificationsEnabled()) {
                //未打开通知
                context?.messageDialog {
                    dialogTitle = _string(R.string.engrave_warn)
                    dialogMessage = _string(R.string.open_notify_tip)
                    negativeButton { dialog, dialogViewHolder ->
                        BaseFlowLayoutHelper._isCheckedEngraveNotify = true
                        dialog.dismiss()
                        action()
                    }
                    needPositiveButton { dialog, dialogViewHolder ->
                        dialog.dismiss()
                        openNotificationSetting()
                    }
                }
            } else if (!EngraveNotifyHelper.isChannelEnable()) {
                //未打开对应的通道
                context?.messageDialog {
                    dialogTitle = _string(R.string.engrave_warn)
                    dialogMessage = _string(R.string.open_notify_channel_tip)
                    negativeButton { dialog, dialogViewHolder ->
                        BaseFlowLayoutHelper._isCheckedEngraveNotify = true
                        dialog.dismiss()
                        action()
                    }
                    needPositiveButton { dialog, dialogViewHolder ->
                        dialog.dismiss()
                        EngraveNotifyHelper.openChannelSetting()
                    }
                }
            } else {
                //可以通知
                BaseFlowLayoutHelper._isCheckedEngraveNotify = true
                action()
            }
        }
    }

    /**检查扩展设备是否处于连接状态*/
    private fun checkExDevice(context: Context?, action: () -> Unit) {
        val deviceStateModel = vmApp<DeviceStateModel>()
        val noConnectType = exDeviceNoConnectType()
        if (noConnectType > 0) {
            context?.messageDialog {
                dialogMessageLeftIco = _drawable(R.mipmap.safe_tips)
                dialogMessage = _string(
                    R.string.engrave_ex_discontent_tips, noConnectType.toDeviceStr()
                )

                negativeButtonText = _string(R.string.dialog_negative)
                positiveButtonListener = { dialog, dialogViewHolder ->
                    dialog.dismiss()
                }

                positiveButtonText = _string(R.string.dialog_continue)
                positiveButtonListener = { dialog, dialogViewHolder ->
                    dialog.dismiss()
                    action()
                }

                onDismissListener = {
                    deviceStateModel.startLoopCheckState(reason = "外设未连接")
                }
            }
        } else {
            action()
        }
    }

    /**第三轴未连接连接*/
    fun exDeviceNoConnectType(): Int {
        val deviceStateModel = vmApp<DeviceStateModel>()
        val laserPeckerModel = vmApp<LaserPeckerModel>()
        var noConnectType = 0
        if (deviceStateModel.needShowExDeviceTipItem()) {
            val stateParser = deviceStateModel.deviceStateData.value
            if (laserPeckerModel.isZOpen()) {
                val connect = stateParser?.zConnect == 1
                if (!connect) {
                    //z轴没有连接, 但是开启了z轴flag
                    noConnectType = 1
                }
            } else if (laserPeckerModel.isSOpen() || laserPeckerModel.isSRepMode()) {
                val connect = stateParser?.sConnect == 1
                if (!connect) {
                    noConnectType = 2
                }
            } else if (laserPeckerModel.isROpen()) {
                val connect = stateParser?.rConnect == 1
                if (!connect) {
                    noConnectType = 3
                }
            }
        }
        return noConnectType
    }

    /**显示焦距提示*/
    fun showFocalDistance(context: Context?, action: () -> Unit) {
        val KEY = "${BaseFlowLayoutHelper.KEY_FOCAL_DISTANCE_TIPS}${getAppVersionCode()}"
        if (KEY.hawkGetBoolean() && !BuildConfig.DEBUG) {
            //不再提示
            action()
            return
        }
        context?.messageDialog {
            dialogMessageLargeDrawable = _drawable(DeviceConnectTipActivity.getDeviceImageRes())
            dialogTitle = _string(R.string.focal_distance_tip)
            negativeButtonText = _string(R.string.dialog_negative)
            dialogNotPromptKey = KEY

            positiveButton { dialog, dialogViewHolder ->
                KEY.hawkPut(_dialogIsNotPrompt)
                dialog.dismiss()
                action()
            }
        }
    }

    /**显示预览安全提示框*/
    fun showSafetyTips(context: Context?, action: () -> Unit) {
        val KEY = "${BaseFlowLayoutHelper.KEY_SAFETY_TIPS}${getAppVersionCode()}"
        if (KEY.hawkGetBoolean() && !BuildConfig.DEBUG) {
            //不再提示
            action()
            return
        }
        context?.messageDialog {
            dialogTitle = span {
                appendImage(_drawable(R.mipmap.safe_tips))
                appendln()
                append(_string(R.string.engrave_warn))
            }
            dialogMessage = _string(R.string.size_safety_content)
            negativeButtonText = _string(R.string.dialog_negative)
            dialogNotPromptKey = KEY

            positiveButton { dialog, dialogViewHolder ->
                KEY.hawkPut(_dialogIsNotPrompt)
                dialog.dismiss()
                action()
            }
        }
    }

    /**检查安全密钥提示*/
    fun checkSafeKey(context: Context?, action: () -> Unit) {
        if (isNoSafeKey) {
            context?.messageDialog {
                dialogTitle = span {
                    appendImage(_drawable(R.drawable.warn_tips))
                    appendln()
                    append(_string(R.string.no_safe_tip))
                }
                dialogMessage = _string(R.string.safe_key_des)
                negativeButtonText = null

                positiveButton { dialog, dialogViewHolder ->
                    dialog.dismiss()
                }
            }
        } else {
            action()
        }
    }

    /**检查未安装保护罩
     * 检查不安全提示
     * [BaseFlowLayoutHelper.checkUnsafe]
     * */
    fun checkCoverState(context: Context?, action: () -> Unit) {
        if (isEuropeanStandard) {
            //欧规, 严格模式
            if (!isCoverConnectAny) {
                //未安装保护罩
                context?.messageDialog {
                    dialogTitle = span {
                        appendImage(_drawable(R.drawable.warn_tips))
                        appendln()
                        append(_string(R.string.no_safe_tip))
                    }
                    dialogMessage = _string(R.string.no_cover_tip)
                    negativeButtonText = null

                    positiveButton { dialog, dialogViewHolder ->
                        dialog.dismiss()
                    }
                }
                return
            }

            if (isCoverUnsafeState) {
                //非安全状态
                context?.messageDialog {
                    dialogTitle = span {
                        appendImage(_drawable(R.drawable.warn_tips))
                        appendln()
                        append(_string(R.string.engrave_warn))
                    }
                    //dialogMessage = "请检查保护罩门已闭合后继续镭雕"
                    dialogMessage = _string(R.string.ex_tips_one2)
                    negativeButtonText = null

                    positiveButton { dialog, dialogViewHolder ->
                        dialog.dismiss()
                    }
                }
                return
            }

            action()
        } else {
            //常规

            if (isCoverUnsafeState) {
                //非安全状态
                context?.messageDialog {
                    dialogTitle = span {
                        appendImage(_drawable(R.drawable.warn_tips))
                        appendln()
                        append(_string(R.string.no_safe_tip))
                    }
                    dialogMessage = _string(R.string.cover_unsafe_standard_des)
                    //negativeButtonText = _string(R.string.dialog_negative)
                    negativeButtonText = null

                    positiveButton { dialog, dialogViewHolder ->
                        dialog.dismiss()
                    }
                }
                return
            }

            action()
        }
    }

    /**异步超时发送退出指令*/
    fun asyncTimeoutExitCmd(
        context: LifecycleOwner?,
        timeout: Long? = null,
        action: IReceiveBeanAction
    ) {
        context?.engraveLoadingAsyncTimeout({
            syncSingle { countDownLatch ->
                ExitCmd(timeout = timeout).enqueue { bean, error ->
                    countDownLatch.countDown()
                    action(bean, error)
                }
            }
        })
    }
}