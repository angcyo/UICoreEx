package com.angcyo.canvas2.laser.pecker.engrave

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.lifecycle.LifecycleOwner
import com.angcyo.bluetooth.fsc.enqueue
import com.angcyo.bluetooth.fsc.laserpacker.DeviceStateModel
import com.angcyo.bluetooth.fsc.laserpacker.command.CancelFlameAlarmCmd
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.core.vmApp
import com.angcyo.dialog.BaseDialogConfig
import com.angcyo.library._screenHeight
import com.angcyo.library._screenWidth
import com.angcyo.library.annotation.DSL
import com.angcyo.library.component.lastActivity
import com.angcyo.library.component.pad.isInPadMode
import com.angcyo.widget.DslViewHolder
import kotlin.math.min

/**
 * 保护罩火焰报警弹窗
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/10
 */
class CoverFlameAlarmDialogConfig(context: Context? = null) : BaseDialogConfig(context) {

    companion object {

        /**监听火焰警报*/
        fun observer(owner: LifecycleOwner) {
            val deviceStateModel = vmApp<DeviceStateModel>()
            deviceStateModel.flameAlarmData.observe(owner) {flameState->
                if (flameState == DeviceStateModel.FLAME_ALARM_L) {
                    lastActivity?.coverFlameAlarmDialogConfig {
                        dialogLayoutId = R.layout.dialog_cover_flame_alarm_l_layout
                    }
                } else if (flameState == DeviceStateModel.FLAME_ALARM_M) {
                    lastActivity?.coverFlameAlarmDialogConfig {
                        dialogLayoutId = R.layout.dialog_cover_flame_alarm_m_layout
                    }
                } else if (flameState == DeviceStateModel.FLAME_ALARM_H) {
                    lastActivity?.coverFlameAlarmDialogConfig {
                        dialogLayoutId = R.layout.dialog_cover_flame_alarm_h_layout
                    }
                }
            }
        }
    }

    init {
        dialogLayoutId = R.layout.dialog_cover_flame_alarm_l_layout
        dialogBgDrawable = ColorDrawable(Color.TRANSPARENT)
    }

    override fun initDialogView(dialog: Dialog, dialogViewHolder: DslViewHolder) {
        super.initDialogView(dialog, dialogViewHolder)
    }

    override fun initControlLayout(dialog: Dialog, dialogViewHolder: DslViewHolder) {
        super.initControlLayout(dialog, dialogViewHolder)
        dialogViewHolder.click(R.id.close_flame_alarm_button) {
            dialog.dismiss()
            CancelFlameAlarmCmd().enqueue()
        }
    }
}

@DSL
fun Context.coverFlameAlarmDialogConfig(action: CoverFlameAlarmDialogConfig.() -> Unit): Boolean {
    CoverFlameAlarmDialogConfig(this).run {
        dialogWidth = -1
        if (isInPadMode()) {
            dialogWidth = min(_screenWidth, _screenHeight)
        }
        canceledOnTouchOutside = false
        action()
        show()
    }
    return true
}