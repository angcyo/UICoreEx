package com.angcyo.canvas2.laser.pecker.engrave.newflow

import android.content.Context
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.dialog.BaseRecyclerDialogConfig
import com.angcyo.dialog.configBottomDialog
import com.angcyo.dialog.dismissWindow
import com.angcyo.dsladapter.DslAdapter
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.dsladapter.getAllItemSelectedList
import com.angcyo.dsladapter.updateItemSelected
import com.angcyo.item.DslBlackButtonItem
import com.angcyo.library.annotation.DSL
import com.angcyo.library.ex.BooleanAction
import com.angcyo.library.ex._string
import com.angcyo.widget.DslViewHolder

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/15
 */
class NewFlowSelectDialog : BaseRecyclerDialogConfig() {

    /**是否点击了新流程*/
    var onNewFlowAction: BooleanAction? = null

    init {
        dialogTitle = _string(R.string.select_engrave_flow_config)
        dialogTitleLayoutId = R.layout.lib_dialog_base_ico_title_layout
        positiveButtonText = null
    }

    override fun DslAdapter.onSelfRenderAdapter() {
        NewFlowSelectItem()() {
            itemText = _string(R.string.new_engrave_config_title)
            itemDes = _string(R.string.new_engrave_config_des)
            itemTip = _string(R.string.beta_version)
        }

        NewFlowSelectItem()() {
            itemText = _string(R.string.normal_engrave_config_title)
            itemDes = _string(R.string.normal_engrave_config_des)
        }

        DslBlackButtonItem()() {
            itemButtonText = _string(R.string.ui_next)
            itemClick = {
                val list = getAllItemSelectedList()
                val first = list.firstOrNull()
                if (first is NewFlowSelectItem) {
                    _dialog?.dismissWindow()
                    onNewFlowAction?.invoke(first.itemTip != null)
                }
            }
        }
    }
}

class NewFlowSelectItem : DslAdapterItem() {

    var itemText: CharSequence? = null
    var itemDes: CharSequence? = null
    var itemTip: CharSequence? = null

    init {
        itemLayoutId = R.layout.item_new_flow_select
        itemSingleSelectMutex = true

        itemClick = {
            updateItemSelected()
        }
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)

        itemHolder.updateText(R.id.lib_text_view, itemText)
        itemHolder.updateText(R.id.lib_des_view, itemDes)
        itemHolder.updateText(R.id.lib_tip_view, itemTip)
    }
}

@DSL
fun Context.newFlowSelectDialogConfig(config: NewFlowSelectDialog.() -> Unit = {}) {
    return NewFlowSelectDialog().run {
        dialogContext = this@newFlowSelectDialogConfig
        configBottomDialog(this@newFlowSelectDialogConfig)
        canceledOnTouchOutside = true
        config()
        show()
    }
}
