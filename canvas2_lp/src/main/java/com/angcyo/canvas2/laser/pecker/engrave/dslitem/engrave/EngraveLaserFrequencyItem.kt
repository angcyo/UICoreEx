package com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave

import android.text.style.DynamicDrawableSpan
import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.bluetooth.fsc.laserpacker._deviceSettingBean
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.dslitem.CanvasSeekBarItem
import com.angcyo.core.CoreApplication
import com.angcyo.dialog2.dslitem.getSelectedWheelBean
import com.angcyo.dialog2.dslitem.itemSelectedIndex
import com.angcyo.dialog2.dslitem.itemWheelList
import com.angcyo.dialog2.dslitem.itemWheelSelectorAction
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.item.style.itemInfoText
import com.angcyo.item.style.itemLabelText
import com.angcyo.laserpacker.bean.LPElementBean
import com.angcyo.laserpacker.bean.LPLaserOptionsBean
import com.angcyo.laserpacker.device.ble.DeviceSettingFragment
import com.angcyo.library.ex._drawable
import com.angcyo.library.ex._string
import com.angcyo.library.ex.dpi
import com.angcyo.objectbox.laser.pecker.entity.EngraveConfigEntity
import com.angcyo.objectbox.laser.pecker.lpSaveEntity
import com.angcyo.widget.span.span
import kotlin.math.max

/**
 * 雕刻参数, 白光出光频率
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2023-10-30
 */
class EngraveLaserFrequencyItem : EngraveOptionWheelItem() {

    companion object {
        const val PAYLOAD_UPDATE_LASER_FREQUENCY = 0x1000
    }

    init {
        itemLabelText = _string(R.string.engrave_laser_frequency_label)

        itemUpdateAction = {
            if (it == PAYLOAD_UPDATE_LASER_FREQUENCY) {
                val list = itemWheelList as? List<Int>
                if (list != null) {
                    val value = itemEngraveConfigEntity?.laserFrequency
                        ?: itemEngraveItemBean?.laserFrequency
                        ?: itemLaserOptionsBean?.laserFrequency
                        ?: HawkEngraveKeys.lastLaserFrequency
                        ?: HawkEngraveKeys.defaultLaserFrequency
                    itemSelectedIndex = max(
                        0,
                        list.indexOf(list.find { it == value })
                    )
                    onSelfLaserFrequencyChange()
                }
            }
        }

        itemWheelSelectorAction = { dialog, index, item ->
            false
        }
    }

    /**初始化默认的参数*/
    fun initLaserFrequencyIfNeed() {
        itemEngraveConfigEntity?.apply {
            if (laserFrequency == null) {
                laserFrequency =
                    HawkEngraveKeys.lastLaserFrequency ?: HawkEngraveKeys.defaultLaserFrequency
                lpSaveEntity()
                HawkEngraveKeys.lastLaserFrequency = laserFrequency
            }
        }
        itemEngraveItemBean?.apply {
            if (laserFrequency == null) {
                laserFrequency =
                    HawkEngraveKeys.lastLaserFrequency ?: HawkEngraveKeys.defaultLaserFrequency
                HawkEngraveKeys.lastLaserFrequency = laserFrequency
            }
        }
        itemLaserOptionsBean?.apply {
            if (laserFrequency == null) {
                laserFrequency =
                    HawkEngraveKeys.lastLaserFrequency ?: HawkEngraveKeys.defaultLaserFrequency
                HawkEngraveKeys.lastLaserFrequency = laserFrequency
            }
        }
    }

    override fun onItemChangeListener(item: DslAdapterItem) {
        super.onItemChangeListener(item)
        onSelfLaserFrequencyChange()
    }

    /**值改变时, 需要进行的操作*/
    fun onSelfLaserFrequencyChange() {
        val value = getSelectedWheelBean<Int>()
        itemEngraveConfigEntity?.apply {
            laserFrequency = value ?: HawkEngraveKeys.defaultLaserFrequency
            lpSaveEntity()
            HawkEngraveKeys.lastLaserFrequency = laserFrequency
        }
        itemEngraveItemBean?.apply {
            laserFrequency = value ?: HawkEngraveKeys.defaultLaserFrequency
            HawkEngraveKeys.lastLaserFrequency = laserFrequency
        }
        itemLaserOptionsBean?.apply {
            laserFrequency = value ?: HawkEngraveKeys.defaultLaserFrequency
            HawkEngraveKeys.lastLaserFrequency = laserFrequency
        }
    }
}

class EngraveLaserFrequencySeekItem : CanvasSeekBarItem() {

    /**参数配置实体*/
    var itemEngraveConfigEntity: EngraveConfigEntity? = null

    /**单元素参数配置*/
    var itemEngraveItemBean: LPElementBean? = null

    /**新流程雕刻参数配置*/
    var itemLaserOptionsBean: LPLaserOptionsBean? = null

    init {

        val helpUrl = DeviceSettingFragment.getHelpUrl(
            _deviceSettingBean?.laserFrequencyHelpUrl,
            _deviceSettingBean?.laserFrequencyHelpUrlZh
        )

        itemInfoText = span {
            append(_string(R.string.engrave_laser_frequency_label))
            append("(kHz)")

            if (!helpUrl.isNullOrBlank()) {
                appendSpace(8 * dpi)
                appendDrawable(
                    _drawable(R.drawable.canvas_invert_help_svg),
                    DynamicDrawableSpan.ALIGN_BOTTOM
                )
            }
        }
        if (!helpUrl.isNullOrBlank()) {
            itemTextClickAction = {
                CoreApplication.onOpenUrlAction?.invoke(helpUrl)
            }
        }
    }

    /**初始化默认的参数*/
    fun initLaserFrequencyIfNeed() {
        itemEngraveConfigEntity?.apply {
            if (laserFrequency == null) {
                laserFrequency =
                    HawkEngraveKeys.lastLaserFrequency ?: HawkEngraveKeys.defaultLaserFrequency
                lpSaveEntity()
                HawkEngraveKeys.lastLaserFrequency = laserFrequency
            }
        }
        itemEngraveItemBean?.apply {
            if (laserFrequency == null) {
                laserFrequency =
                    HawkEngraveKeys.lastLaserFrequency ?: HawkEngraveKeys.defaultLaserFrequency
                HawkEngraveKeys.lastLaserFrequency = laserFrequency
            }
        }
        itemLaserOptionsBean?.apply {
            if (laserFrequency == null) {
                laserFrequency =
                    HawkEngraveKeys.lastLaserFrequency ?: HawkEngraveKeys.defaultLaserFrequency
                HawkEngraveKeys.lastLaserFrequency = laserFrequency
            }
        }
    }

    override fun onItemChangeListener(item: DslAdapterItem) {
        super.onItemChangeListener(item)
        onSelfLaserFrequencyChange()
    }

    /**值改变时, 需要进行的操作*/
    fun onSelfLaserFrequencyChange() {
        val value = _itemTypeValue as Int
        HawkEngraveKeys.lastLaserFrequency = value
        itemEngraveConfigEntity?.apply {
            laserFrequency = value
            lpSaveEntity()
        }
        itemEngraveItemBean?.apply {
            laserFrequency = value
        }
        itemLaserOptionsBean?.apply {
            laserFrequency = value
        }
    }

}