package com.angcyo.canvas2.laser.pecker.engrave.newflow.dslitem

import android.view.View
import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper
import com.angcyo.bluetooth.fsc.laserpacker.bean.LayerColorBean
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.dslitem.CanvasLayerGroupItem
import com.angcyo.canvas2.laser.pecker.dslitem.CanvasLayerItem
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave.depthData
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave.materialData
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave.powerData
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave.resolutionData
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave.speedData
import com.angcyo.canvas2.laser.pecker.engrave.dslitem.engrave.timesData
import com.angcyo.canvas2.laser.pecker.engrave.newflow.EngraveState
import com.angcyo.canvas2.laser.pecker.engrave.newflow.LayerColorView
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.dsladapter.item.initExtendLayout
import com.angcyo.engrave2.EngraveFlowDataHelper
import com.angcyo.item.data.LabelDesData
import com.angcyo.laserpacker.device.filterLayerDpi
import com.angcyo.library.ex.dpi
import com.angcyo.library.ex.tooltipText
import com.angcyo.library.ex.visible
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.base.dslViewHolder
import com.angcyo.widget.base.resetChild
import com.angcyo.widget.flow
import com.angcyo.widget.progress.DslProgressBar

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/16
 *
 * [FlowLayerElementItem]
 * [FlowLayerGroupItem]
 * [CanvasLayerItem]
 *
 * [FlowEngraveElementItem]
 * [FlowEngraveGroupElementItem]
 */
open class FlowEngraveElementItem : CanvasLayerGroupItem() {

    /**是否需要缩进*/
    var itemIndent = false

    /**雕刻界面相关数据以及标识*/
    var itemEngraveState: EngraveState? = null

    val isItemInEngrave: Boolean = true

    init {
        itemLayoutId = R.layout.item_flow_engrave_element
        itemAutoSelectState = false
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        if (isItemInEngrave) {
            itemLongClick = null
        }
        val showOptions = isItemInEngrave && itemEngraveState?.isExpand == true
        itemIsSelected = showOptions
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)

        //缩进
        itemHolder.visible(R.id.indent_view, itemIndent)
        itemHolder.visible(R.id.indent_view2, itemIndent)

        itemHolder.gone(R.id.lib_progress_bar, isGroupRenderer)
        itemHolder.gone(R.id.engrave_state_view, isGroupRenderer)
        itemHolder.gone(R.id.engrave_progress_view, isGroupRenderer)
        itemHolder.gone(R.id.expand_arrow_view, isGroupRenderer)
        itemHolder.gone(R.id.layer_color_view, isGroupRenderer)
        itemHolder.gone(R.id.layer_item_drawable_view, isGroupRenderer)
        if (isGroupRenderer) {

        } else {
            //base
            itemHolder.v<LayerColorView>(R.id.layer_color_view)?.apply {
                showLayerName = false
                enableSelectState = false
                layerColor =
                    if (_elementBean?.layerColor == null) null else LayerColorBean(_elementBean?.layerColor)
                invalidate()
            }
        }

        //engrave
        if (!isGroupRenderer) {
            initExtendLayout(
                itemHolder,
                payloads,
                R.id.expand_arrow_view,
                extendRotation = 0f,
                foldRotation = 90f,
                toExtendRotation = -90f,
                toFoldRotation = 90f,
                triggerViewIdList = listOf(R.id.expand_arrow_view, View.NO_ID)
            ) {
                itemEngraveState?.isExpand = it
                updateAdapterItem()
            }
        }

        //展开详细信息
        itemHolder.visible(R.id.engrave_options_wrap_layout, showOptions)
        if (showOptions) {
            val drawableSize = 18 * dpi
            val bean = _elementBean
            val labelDesList = mutableListOf<LabelDesData>()

            //材质 分辨率
            labelDesList.add(
                materialData(
                    EngraveFlowDataHelper.getEngraveMaterNameByKey(
                        bean?.materialKey
                    )
                )
            )
            val layoutId = bean?._layerId
            if (layoutId != null &&
                layoutId != LaserPeckerHelper.LAYER_LINE &&
                layoutId != LaserPeckerHelper.LAYER_CUT
            ) {
                var dpi = bean.dpi ?: LaserPeckerHelper.DPI_254
                dpi = layoutId.filterLayerDpi(dpi)
                val findPxInfo = LaserPeckerHelper.findPxInfo(layoutId, dpi)
                labelDesList.add(resolutionData(findPxInfo.toText()))
            }

            //功率:
            labelDesList.add(powerData(bean?.printPower))

            //深度:
            labelDesList.add(
                if (bean?.printSpeed != null) {
                    speedData(bean.printSpeed)
                } else {
                    depthData(bean?.printDepth)
                }
            )

            //雕刻次数
            val times = bean?.printCount ?: 1
            labelDesList.add(timesData(null, times))

            itemHolder.flow(R.id.engrave_options_view)
                ?.resetChild(
                    labelDesList,
                    R.layout.dsl_label_des_tag_item
                ) { itemView, item, itemIndex ->
                    val viewHolder = itemView.dslViewHolder()
                    viewHolder.tv(com.angcyo.item.R.id.lib_label_view)?.text = item.label
                    viewHolder.tv(com.angcyo.item.R.id.lib_des_view)?.text = item.des
                    viewHolder.itemView.tooltipText(item.tooltipText)
                }
        }

        //进度
        val progress = itemEngraveState?.progress ?: 0
        itemHolder.v<DslProgressBar>(R.id.lib_progress_bar)?.apply {
            visible(
                itemEngraveState?.state == EngraveState.ENGRAVE_STATE_ENGRAVING ||
                        itemEngraveState?.state == EngraveState.ENGRAVE_STATE_PAUSE ||
                        itemEngraveState?.state == EngraveState.ENGRAVE_STATE_TRANSMITTING
            )
            enableProgressFlowMode =
                !HawkEngraveKeys.enableLowMode && itemEngraveState?.error != null
            if (progress == -1) {
                showProgressText = false
                setProgress(100f, animDuration = 0)
            } else {
                showProgressText = false
                setProgress(progress.toFloat())
            }
        }
        itemHolder.tv(R.id.engrave_progress_view)?.text = "${progress}%"

        if (itemEngraveState?.state == EngraveState.ENGRAVE_STATE_ENGRAVING) {
            itemHolder.gone(R.id.engrave_state_view)
            itemHolder.visible(R.id.engrave_progress_view)
        } else {
            itemHolder.gone(R.id.engrave_progress_view)
        }

        //状态
        when (itemEngraveState?.state) {
            EngraveState.ENGRAVE_STATE_FINISH -> {
                itemHolder.visible(R.id.engrave_state_view)
                itemHolder.img(R.id.engrave_state_view)
                    ?.setImageResource(R.drawable.engrave_state_finish)
            }

            EngraveState.ENGRAVE_STATE_ERROR -> {
                itemHolder.visible(R.id.engrave_state_view)
                itemHolder.img(R.id.engrave_state_view)
                    ?.setImageResource(R.drawable.engrave_state_error)
            }

            EngraveState.ENGRAVE_STATE_PAUSE -> {
                itemHolder.visible(R.id.engrave_state_view)
                itemHolder.img(R.id.engrave_state_view)
                    ?.setImageResource(R.drawable.engrave_state_pause)
            }

            EngraveState.ENGRAVE_STATE_READY -> {
                itemHolder.visible(R.id.engrave_state_view)
                itemHolder.img(R.id.engrave_state_view)
                    ?.setImageResource(R.drawable.engrave_state_wait)
            }

            else -> {
                itemHolder.gone(R.id.engrave_state_view)
            }
        }
    }
}