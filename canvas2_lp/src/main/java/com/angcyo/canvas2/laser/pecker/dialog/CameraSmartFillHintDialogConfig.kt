package com.angcyo.canvas2.laser.pecker.dialog

import android.app.Dialog
import android.content.Context
import com.angcyo.bluetooth.fsc.laserpacker._deviceSettingBean
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.core.CoreApplication
import com.angcyo.dialog.BaseDialogConfig
import com.angcyo.dialog.configBottomDialog
import com.angcyo.laserpacker.device.ble.DeviceSettingFragment
import com.angcyo.library.annotation.DSL
import com.angcyo.library.ex._string
import com.angcyo.widget.DslViewHolder

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/24
 *
 * 摄像头, 智能填充功能提示对话框
 */
class CameraSmartFillHintDialogConfig(context: Context? = null) : BaseDialogConfig(context) {

    init {
        dialogLayoutId = R.layout.dialog_camera_smart_fill_hint_layout
        dialogTitle = _string(R.string.camera_smart_fill)
        positiveButtonText = _string(R.string.camera_smart_fill_button)
    }

    override fun initDialogView(dialog: Dialog, dialogViewHolder: DslViewHolder) {
        super.initDialogView(dialog, dialogViewHolder)
        val helpUrl = DeviceSettingFragment.getHelpUrl(
            _deviceSettingBean?.cameraSmartFillHintHelpUrl,
            _deviceSettingBean?.cameraSmartFillHintHelpUrlZh
        )

        dialogViewHolder.visible(R.id.help_view, !helpUrl.isNullOrBlank())
        dialogViewHolder.click(R.id.help_view) {
            CoreApplication.onOpenUrlAction?.invoke(helpUrl ?: "")
        }
    }
}

/**摄像头智能填充提示对话框*/
@DSL
fun Context.cameraSmartFillHintDialogConfig(config: CameraSmartFillHintDialogConfig.() -> Unit = {}) {
    return CameraSmartFillHintDialogConfig(this).run {
        configBottomDialog(this@cameraSmartFillHintDialogConfig)
        // dialogThemeResId = R.style.LibDialogBaseFullTheme
        config()
        show()
    }
}