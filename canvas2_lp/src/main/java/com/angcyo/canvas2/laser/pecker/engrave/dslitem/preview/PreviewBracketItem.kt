package com.angcyo.canvas2.laser.pecker.engrave.dslitem.preview

import android.widget.TextView
import com.angcyo.bluetooth.fsc.IReceiveBeanAction
import com.angcyo.bluetooth.fsc.enqueue
import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerModel
import com.angcyo.bluetooth.fsc.laserpacker._deviceConfigBean
import com.angcyo.bluetooth.fsc.laserpacker._deviceSettingBean
import com.angcyo.bluetooth.fsc.laserpacker.bean.matchesProductVersion
import com.angcyo.bluetooth.fsc.laserpacker.command.EngravePreviewCmd
import com.angcyo.bluetooth.fsc.laserpacker.parse.EngravePreviewParser
import com.angcyo.bluetooth.fsc.laserpacker.parse.isCoverConnect
import com.angcyo.bluetooth.fsc.parse
import com.angcyo.canvas2.laser.pecker.R
import com.angcyo.canvas2.laser.pecker.util.LPConstant
import com.angcyo.core.vmApp
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.item.keyboard.keyboardNumberWindow
import com.angcyo.library.annotation.MM
import com.angcyo.library.ex._string
import com.angcyo.library.ex.clamp
import com.angcyo.library.ex.isTouchFinish
import com.angcyo.library.toast
import com.angcyo.library.unit.toMm
import com.angcyo.library.unit.toPixel
import com.angcyo.library.unit.unitDecimal
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.base.LongTouchListener

/**
 * 支架控制item
 * 合规保护罩升降控制
 * 支架上升/下降/停止
 *
 * [PreviewControlItem] 预览控制
 * [PreviewBracketItem] 支架高度控制
 *
 * Email:angcyo@126.com
 * @author angcyo
 * @date 2022/09/23
 * Copyright (c) 2020 angcyo. All rights reserved.
 */
class PreviewBracketItem : DslAdapterItem() {

    //产品模式
    val laserPeckerModel = vmApp<LaserPeckerModel>()

    init {
        itemLayoutId = R.layout.item_preview_bracket_layout
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)

        val isCoverConnect = isCoverConnect
        val digit = if (isCoverConnect) 2 else 1

        val heightPixel = HawkEngraveKeys.lastBracketHeight.toPixel()
        val valueUnit = LPConstant.renderUnit
        val value = valueUnit.convertPixelToValue(heightPixel)

        //支架升降高度
        itemHolder.tv(R.id.bracket_height_view)?.text = value.unitDecimal(digit)
        itemHolder.tv(R.id.unit_label_view)?.text = valueUnit.getUnit()

        //材料高度
        itemHolder.visible(
            R.id.cover_height_wrap_layout,
            isCoverConnect && !laserPeckerModel.isROpen() /*非旋转轴模式*/
                    && !_deviceSettingBean?.showCameraFuncRange.matchesProductVersion() /*没有在外面显示材料高度*/
        )
        val materialHeightPixel = HawkEngraveKeys.lastCoverMaterialHeight.toPixel()
        val materialHeightValue = valueUnit.convertPixelToValue(materialHeightPixel)
        itemHolder.tv(R.id.cover_height_view)?.text = materialHeightValue.unitDecimal(digit)
        itemHolder.tv(R.id.cover_unit_label_view)?.text = valueUnit.getUnit()

        @MM
        val coverMaterialMinHeight = _deviceConfigBean?.coverMaterialMinHeight ?: 0f
        val coverMaterialMaxHeight = _deviceConfigBean?.coverMaterialMaxHeight ?: 150f
        val coverMaterialHeightMinValue =
            valueUnit.convertPixelToValue(coverMaterialMinHeight.toPixel())
        val coverMaterialHeightMaxValue =
            valueUnit.convertPixelToValue(coverMaterialMaxHeight.toPixel())

        //材料高度
        itemHolder.click(R.id.cover_height_view) {
            it.context.keyboardNumberWindow(it) {
                onDismiss = {
                    updateAdapterItem()
                    bracketAutoCmd(isCoverConnect, HawkEngraveKeys.lastCoverMaterialHeight)
                    false
                }
                keyboardBindTextView = it as? TextView
                bindPendingDelay = -1 //关闭限流输入

                var minStep = 0.1f
                if (isCoverConnect) {
                    //合规保护罩连接
                    minStep = 0.01f
                    incrementStep = minStep
                    longIncrementStep = incrementStep * 100
                } else {
                    incrementStep = minStep
                    longIncrementStep = incrementStep * 10
                }

                onFormatValueAction = {
                    val fl = it.toFloatOrNull()
                    if (fl == null || fl == 0f || it.endsWith(".") || !it.contains(".")) {
                        it
                    } else {
                        val value = maxOf(fl, coverMaterialHeightMinValue).unitDecimal(digit)
                        if (value.contains(".")) {
                            value.trimEnd('0')
                        } else {
                            value
                        }
                    }
                }
                onNumberResultAction = { number ->
                    val numberPixel = valueUnit.convertValueToPixel(number)
                    var size = numberPixel.toMm()
                    size = clamp(size, coverMaterialHeightMinValue, coverMaterialHeightMaxValue)
                    HawkEngraveKeys.lastCoverMaterialHeight = size
                }
            }
        }

        //升降高度
        itemHolder.click(R.id.bracket_height_view) {
            it.context.keyboardNumberWindow(it) {
                onDismiss = {
                    updateAdapterItem()
                    false
                }
                keyboardBindTextView = it as? TextView
                bindPendingDelay = -1 //关闭限流输入

                var minStep = 0.1f
                if (isCoverConnect) {
                    //合规保护罩连接
                    minStep = 0.01f
                    incrementStep = minStep
                    longIncrementStep = incrementStep * 100
                } else {
                    incrementStep = minStep
                    longIncrementStep = incrementStep * 10
                }

                onFormatValueAction = {
                    val fl = it.toFloatOrNull()
                    if (fl == null || fl == 0f || it.endsWith(".") || !it.contains(".")) {
                        it
                    } else {
                        maxOf(fl, minStep).unitDecimal(digit)
                    }
                }
                onNumberResultAction = { number ->
                    val numberPixel = valueUnit.convertValueToPixel(number)
                    var size = numberPixel.toMm()
                    size = clamp(size, minStep, EngravePreviewCmd.BRACKET_MAX_STEP)
                    HawkEngraveKeys.lastBracketHeight = size
                }
            }
        }
        //支架上升
        itemHolder.longTouch(R.id.bracket_up_view) { view, event, eventType, longPressHappened ->
            when (eventType) {
                LongTouchListener.EVENT_TYPE_CLICK -> {
                    bracketUpCmd(isCoverConnect, HawkEngraveKeys.lastBracketHeight)
                }

                LongTouchListener.EVENT_TYPE_LONG_PRESS -> {
                    bracketUpCmd(isCoverConnect, EngravePreviewCmd.BRACKET_MAX_STEP)
                }
            }
            if (event.isTouchFinish() && longPressHappened) {
                bracketStopCmd(isCoverConnect)
            }
            true
        }
        //支架下降
        itemHolder.longTouch(R.id.bracket_down_view) { view, event, eventType, longPressHappened ->
            when (eventType) {
                LongTouchListener.EVENT_TYPE_CLICK -> {
                    bracketDownCmd(isCoverConnect, HawkEngraveKeys.lastBracketHeight)
                }

                LongTouchListener.EVENT_TYPE_LONG_PRESS -> {
                    bracketDownCmd(isCoverConnect, EngravePreviewCmd.BRACKET_MAX_STEP)
                }
            }
            if (event.isTouchFinish() && longPressHappened) {
                bracketStopCmd(isCoverConnect)
            }
            true
        }
        //停止
        itemHolder.click(R.id.bracket_stop_view) {
            bracketStopCmd(isCoverConnect)
        }
    }

    //

    /**支架上升*/
    fun bracketUpCmd(
        isCoverConnect: Boolean,
        @MM step: Float = EngravePreviewCmd.BRACKET_MAX_STEP,
        action: IReceiveBeanAction? = null
    ) {
        val cmd = if (isCoverConnect) EngravePreviewCmd.previewCoverUpCmd(step) else
            EngravePreviewCmd.previewBracketUpCmd(step)
        cmd.enqueue { bean, error ->
            if (!isCoverConnect) {
                if (bean?.parse<EngravePreviewParser>()?.isBracketConnect() != true) {
                    toast(_string(R.string.bracket_not_connect))
                }
            }
            action?.invoke(bean, error)
        }
    }

    /**支架下降*/
    fun bracketDownCmd(
        isCoverConnect: Boolean,
        @MM step: Float = EngravePreviewCmd.BRACKET_MAX_STEP,
        action: IReceiveBeanAction? = null
    ) {
        val cmd = if (isCoverConnect) EngravePreviewCmd.previewCoverDownCmd(step) else
            EngravePreviewCmd.previewBracketDownCmd(step)
        cmd.enqueue { bean, error ->
            if (!isCoverConnect) {
                if (bean?.parse<EngravePreviewParser>()?.isBracketConnect() != true) {
                    toast(_string(R.string.bracket_not_connect))
                }
            }
            action?.invoke(bean, error)
        }
    }

    /**停止支架*/
    fun bracketStopCmd(isCoverConnect: Boolean, action: IReceiveBeanAction? = null) {
        val cmd =
            if (isCoverConnect) EngravePreviewCmd.previewCoverStopCmd() else EngravePreviewCmd.previewBracketStopCmd()
        cmd.enqueue { bean, error ->
            if (!isCoverConnect) {
                if (bean?.parse<EngravePreviewParser>()?.isBracketConnect() != true) {
                    toast(_string(R.string.bracket_not_connect))
                }
            }
            action?.invoke(bean, error)
        }
    }

    /**自动移动支架*/
    fun bracketAutoCmd(
        isCoverConnect: Boolean,
        @MM height: Float,
        action: IReceiveBeanAction? = null
    ) {
        //基础焦距
        val focal = (_deviceConfigBean?.focalDistance
            ?: 0).toFloat() + (_deviceConfigBean?.focalDistanceCompensate
            ?: 0f)

        //配件补偿焦距
        val laserPeckerModel = vmApp<LaserPeckerModel>()
        val exFocal = if (laserPeckerModel.isROpen()) {
            (_deviceConfigBean?.focalDistanceRFlagCompensate
                ?: 0f) + HawkEngraveKeys.lastDiameterPixel.toMm() / 2
        } else if (laserPeckerModel.isZOpen()) {
            _deviceConfigBean?.focalDistanceZFlagCompensate ?: 0f
        } else if (laserPeckerModel.isSOpen() || laserPeckerModel.isSRepMode()) {
            _deviceConfigBean?.focalDistanceSFlagCompensate ?: 0f
        } else {
            0f
        }
        val cmd =
            if (isCoverConnect) EngravePreviewCmd.previewCoverAutoCmd(
                height,
                focal + exFocal,
            ) else EngravePreviewCmd.previewBracketStopCmd()
        cmd.enqueue { bean, error ->
            action?.invoke(bean, error)
        }
    }
}