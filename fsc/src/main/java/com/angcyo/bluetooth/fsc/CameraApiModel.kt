package com.angcyo.bluetooth.fsc

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.RectF
import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import android.net.wifi.WifiManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import com.angcyo.bluetooth.fsc.bean.CameraTemplateBean
import com.angcyo.bluetooth.fsc.bean.CameraVersionBean
import com.angcyo.bluetooth.fsc.core.WifiDeviceScan
import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.bluetooth.fsc.laserpacker._deviceSettingBean
import com.angcyo.bluetooth.fsc.laserpacker.parse.QueryCameraInfoParser
import com.angcyo.bluetooth.fsc.laserpacker.toIpHost
import com.angcyo.bluetooth.fsc.laserpacker.toLocal
import com.angcyo.http.asRequestBody
import com.angcyo.http.base.HttpBean
import com.angcyo.http.base.HttpCallback
import com.angcyo.http.base.beanType
import com.angcyo.http.base.fromJson
import com.angcyo.http.base.listBeanType
import com.angcyo.http.base.mapType
import com.angcyo.http.base.toFilePart
import com.angcyo.http.base.toJson
import com.angcyo.http.download.download
import com.angcyo.http.get
import com.angcyo.http.get2Body
import com.angcyo.http.interceptor.LogInterceptor
import com.angcyo.http.rx.doMain
import com.angcyo.http.rx.observe
import com.angcyo.http.toApi
import com.angcyo.http.toBean
import com.angcyo.http.uploadFile2Body
import com.angcyo.library.L
import com.angcyo.library.annotation.CallPoint
import com.angcyo.library.annotation.Pixel
import com.angcyo.library.app
import com.angcyo.library.component._delay
import com.angcyo.library.component.hawk.LibLpHawkKeys
import com.angcyo.library.component.lastActivity
import com.angcyo.library.component.lastContext
import com.angcyo.library.component.runOnMainThread
import com.angcyo.library.ex.file
import com.angcyo.library.ex.getFileAttachmentName
import com.angcyo.library.ex.toBitmap
import com.angcyo.library.toastQQ
import com.angcyo.library.unit.toPixel
import com.angcyo.opencv.OpenCV
import com.angcyo.viewmodel.IViewModel
import com.angcyo.viewmodel.MutableOnceLiveData
import com.angcyo.viewmodel.updateValue
import com.angcyo.viewmodel.vmDataNull
import com.angcyo.viewmodel.vmDataOnce
import java.util.concurrent.Semaphore
import kotlin.concurrent.thread
import kotlin.math.roundToInt

/**
 * 摄像头相关模型
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/05
 */
class CameraApiModel : ViewModel(), IViewModel {

    init {
        doMain(false) {
            val lastCameraNameData = HawkEngraveKeys.lastCameraNameData
            if (!lastCameraNameData.isNullOrBlank()) {
                val cameraDevice = lastCameraNameData.fromJson(CameraDevice::class.java)
                selectedCameraDeviceData.updateValue(cameraDevice)
            }
            val lastCameraTemplateData = HawkEngraveKeys.lastCameraTemplateData
            if (!lastCameraTemplateData.isNullOrBlank()) {
                val cameraTemplate = lastCameraTemplateData.fromJson(CameraTemplateBean::class.java)
                selectedCameraTemplateData.updateValue(cameraTemplate)
            }
        }
    }

    /**当前扫描状态
     * [WifiDeviceScan.STATE_SCAN_START]*/
    val scanStateData = vmDataNull<Int?>(null)

    val scanStateOnceData = vmDataOnce<Int>()

    /**当前的扫描状态*/
    val scanState: Int
        get() = scanStateData.value ?: 0

    /**扫描到的摄像头设备, 不重复的设备*/
    val cameraDeviceOnceData: MutableOnceLiveData<CameraDevice?> = vmDataOnce(null)

    /**扫描到的摄像头设备列表, 不重复的设备*/
    val cameraScanDeviceList = mutableListOf<CameraDevice>()

    /**选中的摄像头设备*/
    val selectedCameraDeviceData = vmDataNull<CameraDevice>()

    /**选中的摄像头模板*/
    val selectedCameraTemplateData = vmDataNull<CameraTemplateBean>()

    /**是否选择了摄像头*/
    val isSelectedCamera: Boolean
        get() = selectedCameraDeviceData.value != null

    /**是否选择了摄像头模板*/
    val isSelectedCameraTemplate: Boolean
        get() = selectedCameraTemplateData.value != null

    /**api 接口地址*/
    val cameraHost: String
        get() = if (LibLpHawkKeys.enableUseIpConnect) selectedCameraDeviceData.value?.resolveHost?.toIpHost
            ?: selectedCameraDeviceData.value?.deviceName?.toLocal
            ?: "" else selectedCameraDeviceData.value?.deviceName?.toLocal ?: ""

    //--
    var cameraInfoParser: QueryCameraInfoParser? = null
    val isHostConnectCamera: Boolean get() = cameraInfoParser?.isCameraConnected == true
    val isCameraConnectedNetwork: Boolean get() = cameraInfoParser?.isCameraConnectedNetwork == true

    /**更新摄像头的信息*/
    fun updateCameraInfo(parser: QueryCameraInfoParser?) {
        updateSelectedCameraDevice(null)//先重置摄像头
        cameraInfoParser = parser
        if (parser?.isCameraConnected == true) {
            if (parser.isCameraConnectedNetwork) {
                updateSelectedCameraDevice(
                    CameraDevice(
                        parser.name ?: "", parser.ip
                    )
                )
            } else {
                L.w("摄像头未连接到网络:${parser}")
            }
        } else {
            L.w("摄像头未连接:${parser}")
        }
    }

    /**更新选中的摄像头设备信息*/
    fun updateSelectedCameraDevice(cameraDevice: CameraDevice?) {
        HawkEngraveKeys.lastCameraNameData = cameraDevice?.toJson()
        L.d("选择摄像头:${HawkEngraveKeys.lastCameraNameData}")
        selectedCameraDeviceData.updateValue(cameraDevice)
        if (cameraDevice == null) {
            //移除摄像头之后, 清除图片
            cameraBitmapClearOnceData.updateValue(true)
            clearCameraRectify()
        } else {
            fetchCameraRectify()
        }
    }

    /**更新选中的摄像头模版信息*/
    fun updateSelectedCameraTemplate(cameraTemplate: CameraTemplateBean?) {
        clearCameraTemplateCache()
        HawkEngraveKeys.lastCameraTemplateData = cameraTemplate?.toJson()
        selectedCameraTemplateData.updateValue(cameraTemplate)
    }

    //--

    /**摄像头最后一次抓取的图片*/
    val cameraBitmapData = vmDataNull<Bitmap?>(null)

    /**监听此通知, 自动更新[cameraBitmapData]到画布上*/
    val cameraBitmapRefreshOnceData = vmDataOnce<Bitmap?>(null)

    /**监听此通知, 清除画布上的[_cameraTemplateBitmap]缓存*/
    val cameraBitmapClearOnceData = vmDataOnce<Boolean?>(null)

    /**[cameraTemplateBitmap]的缓存*/
    var _cameraTemplateBitmap: Bitmap? = null

    /**模板对应的图片*/
    val cameraTemplateBitmap: Bitmap?
        get() = _cameraTemplateBitmap ?: cameraBitmapData.value?.run {
            if (isRecycled) return null
            val templateBean = selectedCameraTemplateData.value ?: return null
            val form = templateBean.form ?: return null
            val data = templateBean.data ?: return null
            val matrix = Matrix()
            matrix.setScale(data.scaleX!!, data.scaleY!!)
            matrix.postRotate(data.rotate!!)
            //先放大旋转原图
            val bitmap1 =
                Bitmap.createBitmap(applyCameraRectify(this), 0, 0, width, height, matrix, true)/*val dataX = if (data.scaleX < 0) bitmap1.width - data.x!! else data.x!!
            val dataY = if (data.scaleY < 0) bitmap1.height - data.y!! else data.y!!*/
            val dataX = data.x!!
            val dataY = data.y!!
            if ((dataX + data.width!!) > bitmap1.width || (dataY + data.height!!) > bitmap1.height) {
                toastQQ("Invalid template")
                return null
            }
            //原图中截取区域
            val bitmap2 = Bitmap.createBitmap(
                bitmap1,
                dataX.roundToInt(),
                dataY.roundToInt(),
                data.width.roundToInt(),
                data.height.roundToInt(),
            )
            //bitmap1.recycle() //2024-10-10 这里回收会把 this 也回收
            //截图之后, 放大显示
            val scaleMatrix = Matrix()
            val sx = (form.width!!.toPixel() / bitmap2.width)
            val sy = (form.height!!.toPixel() / bitmap2.height)
            scaleMatrix.setScale(sx, sy)
            scaleMatrix.postRotate(form.angle!!)
            val bitmap3 =
                Bitmap.createBitmap(bitmap2, 0, 0, bitmap2.width, bitmap2.height, scaleMatrix, true)
            bitmap2.recycle()

            _cameraTemplateBitmap = bitmap3
            _cameraTemplateBitmap
        }

    /**模板图片所在的位置*/
    @Pixel
    val cameraTemplateBitmapRect: RectF?
        get() {
            val bitmap = cameraTemplateBitmap
            val bean = selectedCameraTemplateData.value
            if (bitmap == null || bean == null) {
                return null
            }
            return bean.form?.run {
                val width = bitmap.width
                val height = bitmap.height
                val left = centerX!!.toPixel() - width / 2
                val top = centerY!!.toPixel() - height / 2
                RectF(left, top, left + width, top + height)
            }
        }

    /**清空模板缓存
     * [_cameraTemplateBitmap]
     * */
    fun clearCameraTemplateCache() {
        _cameraTemplateBitmap?.recycle()
        _cameraTemplateBitmap = null
    }

    /**是否有拍照数据*/
    val haveCameraBitmap: Boolean
        get() = cameraBitmapData.value != null

    //--

    /**保护罩版本*/
    val coverVersionData = vmDataNull<CameraVersionBean>()

    /**摄像头版本*/
    val cameraVersionData = vmDataNull<CameraVersionBean>()

    //--

    var _lifecycleOwner: LifecycleOwner? = null
    val _lifecycleObserver = LifecycleEventObserver { source, event ->
        if (event == Lifecycle.Event.ON_DESTROY) {
            //销毁之后, 自动移除
            stopDiscovery()
        }
    }

    /**[NsdManager]*/
    private var nsdManager: NsdManager? = null
    private var multicastLock: WifiManager.MulticastLock? = null
    private var wifiManager: WifiManager? = null

    /**使用nsd服务,开始发现设备
     * [timeout] 超时时长, 毫秒*/
    fun startDiscovery(lifecycleOwner: LifecycleOwner, timeout: Long = 3 * 1000): Boolean {
        if (scanState == WifiDeviceScan.STATE_SCAN_START) {
            return false
        }
        initServerManager()
        _lifecycleOwner = lifecycleOwner
        lifecycleOwner.lifecycle.addObserver(_lifecycleObserver)
        cameraScanDeviceList.clear()
        try {
            multicastLock?.acquire()

            nsdManager?.discoverServices(
                HawkEngraveKeys.nsdServiceType, NsdManager.PROTOCOL_DNS_SD, discoveryListener
            )
            updateDiscoveryState(WifiDeviceScan.STATE_SCAN_START)

            //3秒后停止
            _delay(timeout) {
                L.d("超时停止发现设备!")
                stopDiscovery()
            }
        } catch (e: Exception) {
            multicastLock?.release()
            e.printStackTrace()
        }
        return true
    }

    /**停止发现设备*/
    fun stopDiscovery() {
        doMain {
            try {
                multicastLock?.release()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            try {
                nsdManager?.stopServiceDiscovery(discoveryListener)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (scanState == WifiDeviceScan.STATE_SCAN_START) {
                updateDiscoveryState(WifiDeviceScan.STATE_SCAN_FINISH)
            }
            _lifecycleOwner?.lifecycle?.removeObserver(_lifecycleObserver)
            _lifecycleOwner = null
            try {
                nsdManager = null
                multicastLock = null
                wifiManager = null
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    /**更新探测状态*/
    private fun updateDiscoveryState(state: Int) {
        scanStateData.updateValue(state)
        scanStateOnceData.updateValue(state)
    }

    private fun initServerManager() {
        val context = lastActivity ?: app()
        nsdManager = ContextCompat.getSystemService(context, NsdManager::class.java) ?: nsdManager
        wifiManager = ContextCompat.getSystemService(context, WifiManager::class.java)
        wifiManager?.apply {
            if (multicastPermissionGranted(context)) {
                multicastLock = createMulticastLock("wifiMulticastLock").also {
                    it.setReferenceCounted(true)
                }
            }
        }
    }

    private fun multicastPermissionGranted(context: Context) = ContextCompat.checkSelfPermission(
        context, Manifest.permission.CHANGE_WIFI_MULTICAST_STATE
    ) == PackageManager.PERMISSION_GRANTED

    // 公平锁, 信号量, 用于解析服务
    private val resolveSemaphore = Semaphore(1)

    private val discoveryListener = object : NsdManager.DiscoveryListener {

        // Called as soon as service discovery begins.
        override fun onDiscoveryStarted(regType: String) {
            L.d("开始发现服务:${regType}")
        }

        override fun onServiceFound(service: NsdServiceInfo) {
            L.d("发现服务:$service")
            runOnMainThread {
                val prefix = _deviceSettingBean?.cameraPrefix
                if (!prefix.isNullOrBlank()) {
                    val serviceName = service.serviceName
                    if (serviceName.startsWith(prefix)) {
                        //
                        val find = cameraScanDeviceList.find { it.deviceName == serviceName }
                        if (find == null) {
                            val device = CameraDevice(serviceName)
                            cameraScanDeviceList.add(device)
                            cameraDeviceOnceData.updateValue(device)
                        }
                        thread {
                            resolveSemaphore.acquire()
                            try {
                                nsdManager?.resolveService(service, resolveListener)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                }
            }
        }

        override fun onServiceLost(service: NsdServiceInfo) {
            // When the network service is no longer available.
            // Internal bookkeeping code goes here.
            L.e("服务丢失:$service")
        }

        override fun onDiscoveryStopped(serviceType: String) {
            L.i("停止发现服务:$serviceType")
            stopDiscovery()
        }

        override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
            L.e("发现服务失败:$serviceType :$errorCode")
            stopDiscovery()
        }

        override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
            L.e("停止发现服务失败:${serviceType} :$errorCode")
            stopDiscovery()
        }
    }

    private val resolveListener = object : NsdManager.ResolveListener {
        override fun onResolveFailed(serviceInfo: NsdServiceInfo?, errorCode: Int) {
            L.e("解析服务失败:${serviceInfo?.serviceName} :$errorCode")
            resolveSemaphore.release()
        }

        override fun onServiceResolved(serviceInfo: NsdServiceInfo?) {
            val host = serviceInfo?.host?.hostAddress ?: serviceInfo?.host?.canonicalHostName
            L.d("解析服务成功[${host}]:${serviceInfo}")
            // 解析服务成功才有ip地址返回
            try {
                cameraScanDeviceList.find { it.deviceName == serviceInfo?.serviceName }?.apply {
                    resolveHost = host
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            resolveSemaphore.release()
        }
    }

    //region --api--

    /**获取摄像头模板*/
    @CallPoint
    fun fetchCameraConfigList(result: HttpCallback<List<CameraTemplateBean>>? = null) {
        get {
            url = "camera_config?id=all".toApi(cameraHost)
        }.observe { data, error ->
            val body =
                data?.toBean<HttpBean<List<CameraTemplateBean>>>(listBeanType(CameraTemplateBean::class.java))
            result?.invoke(body?.data, error)
        }
    }

    /**获取摄像头拍照图片*/
    @CallPoint
    fun fetchCameraPicture(autoRefresh: Boolean = true, result: HttpCallback<Bitmap>? = null) {
        get2Body {
            url = "snapshot".toApi(cameraHost)
            isSuccessful = { it.isSuccessful }
        }.observe { data, error ->
            val bitmap = data?.body()?.bytes()?.toBitmap()
            if (bitmap != null) {
                clearCameraTemplateCache()
                cameraBitmapData.updateValue(bitmap)
                if (autoRefresh) {
                    cameraBitmapRefreshOnceData.updateValue(bitmap)
                }
            } else {
                toastQQ("fetch error")
            }
            result?.invoke(bitmap, error)
        }
    }

    /**获取保护罩固件版本*/
    @CallPoint
    fun fetchCoverVersion(result: HttpCallback<CameraVersionBean>? = null) {
        //camera_firmware_version
        get {
            url = "cover_firmware_version".toApi(cameraHost)
        }.observe { data, error ->
            val body =
                data?.toBean<HttpBean<CameraVersionBean>>(beanType(CameraVersionBean::class.java))
            if (body?.data != null) {
                coverVersionData.updateValue(body.data)
            }
            result?.invoke(body?.data, error)
        }
    }

    /**获取摄像头固件版本*/
    @CallPoint
    fun fetchCameraVersion(result: HttpCallback<CameraVersionBean>? = null) {
        get {
            url = "camera_firmware_version".toApi(cameraHost)
        }.observe { data, error ->
            val body =
                data?.toBean<HttpBean<CameraVersionBean>>(beanType(CameraVersionBean::class.java))
            if (body?.data != null) {
                cameraVersionData.updateValue(body.data)
            }
            result?.invoke(body?.data, error)
        }
    }

    /**状态通知*/
    val firmwareUpdateOnceData = vmDataOnce<FirmwareUpdateState>(null)

    /**开始升级保护罩固件
     * [url] 固件的在线地址
     * */
    @CallPoint
    fun startUpdateCover(
        url: String,
        action: (firmwareFilePath: String?, error: Throwable?) -> Unit = { firmwareFilePath, error ->
            firmwareFilePath?.let {
                firmwareUpdateOnceData.postValue(
                    FirmwareUpdateState(FirmwareUpdateState.STATE_FINISH, 100)
                )
            }
            error?.let {
                firmwareUpdateOnceData.postValue(
                    FirmwareUpdateState(FirmwareUpdateState.STATE_ERROR, -1, it)
                )
            }
        }
    ) {
        firmwareUpdateOnceData.postValue(FirmwareUpdateState(FirmwareUpdateState.STATE_DOWNLOAD))
        url.download { task, error ->
            if (task.isFinish) {
                task.savePath.file().inputStream().asRequestBody().let { body ->
                    //保护罩固件升级
                    val coverUpdateApi = "cover_update".toApi(cameraHost)
                    uploadFile2Body {
                        this.url = coverUpdateApi
                        header = hashMapOf(LogInterceptor.closeLog(true))
                        filePart = body.toFilePart(
                            task.savePath.getFileAttachmentName() ?: "firmware.bin", name = "update"
                        )
                    }.observe { data, error ->
                        L.i("保护罩固件升级结束:${coverUpdateApi}\n${task.savePath}\n${data}")
                        action(task.savePath, error)
                    }
                }
            }
            error?.let {
                action(null, it)
            }
        }
    }

    /**开始升级摄像头固件
     * [url] 固件的在线地址
     * */
    @CallPoint
    fun startUpdateCamera(
        url: String,
        action: (firmwareFilePath: String?, error: Throwable?) -> Unit = { firmwareFilePath, error ->
            firmwareFilePath?.let {
                firmwareUpdateOnceData.postValue(
                    FirmwareUpdateState(FirmwareUpdateState.STATE_FINISH, 100)
                )
            }
            error?.let {
                firmwareUpdateOnceData.postValue(
                    FirmwareUpdateState(FirmwareUpdateState.STATE_ERROR, -1, it)
                )
            }
        }
    ) {
        firmwareUpdateOnceData.postValue(FirmwareUpdateState(FirmwareUpdateState.STATE_DOWNLOAD))
        url.download { task, error ->
            if (task.isFinish) {
                task.savePath.file().inputStream().asRequestBody().let { body ->
                    //保护罩固件升级
                    val cameraUpdateApi = "camera_update".toApi(cameraHost)
                    uploadFile2Body {
                        this.url = cameraUpdateApi
                        header = hashMapOf(LogInterceptor.closeLog(true))
                        filePart = body.toFilePart(
                            task.savePath.getFileAttachmentName() ?: "firmware.bin",
                            name = "filename"
                        )
                    }.observe { data, error ->
                        L.i("摄像头固件升级结束:${cameraUpdateApi}\n${task.savePath}\n${data}")
                        if (error == null) {
                            //升级成功摄像头需要重启
                            get {
                                this.url = "camera_reboot".toApi(cameraHost)
                            }.observe { _, _ ->
                                //重启end
                            }
                        }
                        action(task.savePath, error)
                    }
                }
            }
            error?.let {
                action(null, it)
            }
        }
    }

    /** 3*3 通常是9个值 */
    var intrinsicMatrixList: DoubleArray? = null

    /** 通常是12个值 */
    var distortionCoeffList: DoubleArray? = null

    /**获取摄像头矫正数据
     * ```
     *  {"code":200,"message":"OK","data":{"intrinsic_matrix":[[2546.857812,0,1259.304318],[0,2552.716882,1016.583818],[0,0,1]],
     *  "distortion_coeff":[-0.820001564,0.822331385,-0.001876274,0.000985952,13.84821043,-0.473128337,
     *  -1.812155125,21.13507531,-0.001615713,0.002697366,0.000061776,0.001916805]}}
     * ```
     * */
    @CallPoint
    fun fetchCameraRectify(result: HttpCallback<Map<String, Any>>? = null) {
        get {
            url = "camera_rectify".toApi(cameraHost)
        }.observe { data, error ->
            val body = data?.toBean<Map<String, Any>>(mapType(String::class.java, Any::class.java))
            val dataMap = body?.get("data") as? Map<*, *>;
            val intrinsicMatrix = dataMap?.get("intrinsic_matrix")
            val distortionCoeff = dataMap?.get("distortion_coeff")
            if (intrinsicMatrix is ArrayList<*>) {
                intrinsicMatrixList =
                    intrinsicMatrix.flatMap { it as ArrayList<Double> }.toDoubleArray()
            }
            if (distortionCoeff is ArrayList<*>) {
                distortionCoeffList = distortionCoeff.map { it as Double }.toDoubleArray()
            }
            result?.invoke(body, error)
        }
    }

    /**矫正图片*/
    fun applyCameraRectify(inBitmap: Bitmap): Bitmap {
        return if (intrinsicMatrixList != null && distortionCoeffList != null) {
            OpenCV.bitmapToUndistort(
                lastContext, inBitmap, intrinsicMatrixList!!, distortionCoeffList!!
            ) ?: inBitmap
        } else {
            inBitmap
        }
    }

    /**清除摄像头矫正数据*/
    fun clearCameraRectify() {
        intrinsicMatrixList = null
        distortionCoeffList = null
    }

    //endregion --api--
}

data class FirmwareUpdateState(
    /**当前更新的状态*/
    val state: Int,
    /**当前状态下的记录*/
    val progress: Int = -1,
    /**失败时的错误信息*/
    val error: Throwable? = null
) {
    companion object {
        const val STATE_NORMAL = 0
        const val STATE_DOWNLOAD = 1
        const val STATE_UPDATE = 2
        const val STATE_FINISH = 3
        const val STATE_ERROR = 4
    }
}

/**
 * [TcpDevice]
 * */
data class CameraDevice(
    /**设备名称
     * `lds-camera-e1a794`
     * http://lds-camera-e1a794.local/wifisave?p=Hi-Redmi-VPN&s=Hi-Redmi-VPN OK(200)
     * */
    var deviceName: String = "",
    /**解析出来的host主机地址, 有可能是ip*/
    var resolveHost: String? = null,
)