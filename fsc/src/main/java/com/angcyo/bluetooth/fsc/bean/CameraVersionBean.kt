package com.angcyo.bluetooth.fsc.bean

import androidx.annotation.Keep

/**
 * 摄像头固件版本版本
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/19
 *
 * ```
 * "hostVersion": 80300,
 * "hostVersionStr": "V80300"
 * ```
 */
@Keep
data class CameraVersionBean(
    val hostVersion: Int = 0,
    val hostVersionStr: String? = null,
)
