package com.angcyo.bluetooth.fsc.laserpacker.command

import android.graphics.Point
import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper
import com.angcyo.library.annotation.MM
import com.angcyo.library.annotation.Pixel
import com.angcyo.library.ex.toDC

/**
 * 出厂设置指令
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2023/06/25
 */
data class FactoryCmd(

    /**状态码*/
    var state: Byte = 0x0,
    var custom: Byte = 0x0,

    /**数据索引, 完成矫正指令*/
    var index: Int = 0,

    /**跳到指定ad点*/
    @Pixel var adX: Int = 0,
    var adY: Int = 0,
    var laser: Byte = 0x0, //激光功率 （范围0 - 255）
    var laserAdjust: Int = 0,//2个字节, 激光功率微调 （范围0 - 4095）
    /**[com.angcyo.bluetooth.fsc.laserpacker.command.EngraveCmd.type]*/
    var type: Byte = LaserPeckerHelper.LASER_TYPE_BLUE, //激光类型

    /**跳到执行坐标*/
    var x: Int = 0,
    var y: Int = 0,

    /**data1=0时为计算数据，data1=1时使用校正数据*/
    var data1: Byte = 0x1,
    var data2: Byte = 0x0,
    var data3: Byte = 0x0,

    /**data4*/
    var data4: Byte = 0x0,

    /**[pointList] mm单位, 精度需要手动乘以10后发送来*/
    var pointList: List<Point> = emptyList(),

    /**激光功率*/
    var pwr: Int = 255,

    /**无矫正预览类型
     * 0x00为范围预览；
     * 0x01为XY轴十字预览。*/
    var previewType: Int = 0,
) : BaseCommand() {

    companion object {
        /**无矫正范围预览
         * [previewType] 0x00为范围预览；
         * 0x01为XY轴十字预览。
         * [type] 为激光类型，0为450nm,1为1064nm, 2为450nm与1064同时开。
         * */
        fun noAdjustRangePreviewCmd(
            previewType: Int = 0,
            type: Byte = LaserPeckerHelper.LASER_TYPE_BLUE,
            pwr: Int = HawkEngraveKeys.lastPwrProgress.toLaserPeckerPWR().toInt()
        ): FactoryCmd {
            return FactoryCmd(state = 0x05, previewType = previewType, type = type, pwr = pwr)
        }

        /**较正数据传输完成*/
        fun finishAdjustDataCmd(index: Int): FactoryCmd {
            return FactoryCmd(state = 0x08, index = index).apply {
                receiveTimeout = 5 * 60 * 1000 //5分钟
            }
        }

        /**校正数据使能
         * [boolean] 是否使用矫正数据*/
        fun useAdjustData(boolean: Boolean = true): FactoryCmd {
            return FactoryCmd(state = 0x10, data1 = if (boolean) 0x1 else 0x0)
        }

        /**激光点跳至指定AD值
         * [laser] 激光功率 （范围0 - 255）
         * [type] 激光类型
         * */
        fun jumpToAdCmd(adX: Int, adY: Int, laser: Byte, type: Byte): FactoryCmd {
            return FactoryCmd(state = 0x09, adX = adX, adY = adY, laser = laser, type = type)
        }

        /**激光点跳到指定坐标*/
        fun jumpToCoordCmd(x: Int, y: Int): FactoryCmd {
            return FactoryCmd(state = 0x0A, x = x, y = y)
        }

        /**[pointList] mm单位, 精度需要手动乘以10后发送来*/
        fun jumpToCoordCmd(
            pwr: Byte, type: Byte, @MM pointList: List<Point>
        ): FactoryCmd {
            return FactoryCmd(state = 0x0A, laser = pwr, type = type, pointList = pointList)
        }

        /**激光点预览功率设置*/
        fun previewPowerSettingCmd(laser: Byte, type: Byte): FactoryCmd {
            return FactoryCmd(state = 0x0B, laser = laser, type = type)
        }

        /**激光功率微调功能*/
        fun laserPowerAdjustCmd(laserAdjust: Int, type: Byte): FactoryCmd {
            return FactoryCmd(state = 0x14, laserAdjust = laserAdjust, type = type)
        }

        /**LX1 出厂雕刻设置指令*/
        fun factoryPCTCmd(enable: Boolean): FactoryCmd {
            return FactoryCmd(state = 0x21, data4 = if (enable) 0x1 else 0x0)
        }

        /**LX1 黑白/抖动图片补偿设置*/
        fun factoryCompensateCmd(bwc: Byte, dc: Byte, enable: Boolean): FactoryCmd {
            return FactoryCmd(
                state = 0x22, data1 = bwc, data2 = dc, data3 = if (enable) 0x1 else 0x0
            )
        }

        /**保存对焦光标坐标值（L5有用）*/
        fun saveFocusCmd(): FactoryCmd {
            return FactoryCmd(state = 0x11)
        }

        /**0x12 保存雕刻最低功率值。（L5有用）*/
        fun saveMinPowerCmd(): FactoryCmd {
            return FactoryCmd(state = 0x12)
        }

        /**0x13 保存预览最高功率值。（L5有用）*/
        fun saveMaxPowerCmd(): FactoryCmd {
            return FactoryCmd(state = 0x13)
        }

        /**0x15 跳至指定坐标，返回校正ADC给上位机（L5有用）//2024/05/27
         * LP5 精度为0.1mm, 所以需要乘以10
         * [x].[y]自行乘以10传过来
         * 此指令会返回下位机的ad坐标
         * */
        fun getCoordValueCmd(
            @MM x: Int,
            @MM y: Int,
            type: Byte,
            pwr: Int = HawkEngraveKeys.lastPwrProgress.toLaserPeckerPower().toInt()
        ): FactoryCmd {
            return FactoryCmd(state = 0x15, x = x, y = y, type = type, pwr = pwr)
        }

        /**0x0F 出厂抖动图效果测试。。（L4/L5有用）*/
        fun factoryImageCmd(): FactoryCmd {
            return FactoryCmd(state = 0x0F)
        }
    }

    override fun toByteArray(): ByteArray {
        return commandByteWriter {
            writeUByte(commandFunc())
            writeUByte(state)

            when (state) {
                0x05.toByte() -> {
                    write(pwr, 1)
                    writeUByte(type)
                    write(previewType, 1)
                    writeUByte(0)//占位
                    writeUByte(custom)
                }

                0x08.toByte() -> {
                    write(index, 4) //数据索引
                    writeUByte(custom)
                }

                0x09.toByte() -> {
                    write(adX, 2)
                    write(adY, 2)
                    writeUByte(custom)
                    writeUByte(laser)
                    writeUByte(type)
                }

                0x0A.toByte() -> {
                    if (pointList.isEmpty()) {
                        write(x, 2)
                        write(y, 2)
                        writeUByte(custom)
                    } else {
                        writeUByte(laser)
                        writeUByte(type)
                        write(pointList.size)
                        pointList.forEach {
                            write(it.x, 2)
                            write(it.y, 2)
                        }
                        writeUByte(custom)
                    }
                }

                0x0B.toByte() -> {
                    writeUByte(laser)
                    writeUByte(type)
                    write(0, 2)//补齐2个字节
                    writeUByte(custom)
                }

                0x10.toByte() -> {
                    writeUByte(data1)
                    writeUByte(0)
                    writeUByte(custom)
                    writeUByte(0)
                }

                0x14.toByte() -> {
                    write(laserAdjust, 2)
                    writeUByte(type)
                    writeUByte(custom)
                }

                0x15.toByte() -> {
                    write(pwr, 1)
                    writeUByte(type)
                    writeUByte(custom)
                    write(x, 2)
                    write(y, 2)
                }

                0x21.toByte() -> {
                    writeUByte(0)
                    writeUByte(0)
                    writeUByte(custom)
                    writeUByte(data4)
                }

                0x22.toByte() -> {
                    writeUByte(data1)
                    writeUByte(data2)
                    writeUByte(data3)
                    writeUByte(custom)
                }

                else -> {
                    writeUByte(custom)
                    writeUByte(data2)
                    writeUByte(data3)
                    writeUByte(data4)
                }
            }
        }
    }

    /**超时时长, 毫秒*/
    var receiveTimeout: Long? = null

    override fun getReceiveTimeout(): Long {
        return receiveTimeout ?: super.getReceiveTimeout()
    }

    /**出厂设置：功能码为0x0f*/
    override fun commandFunc(): Byte = 0x0f

    override fun toHexCommandString(): String {
        return super.toHexCommandString()
    }

    override fun toCommandLogString(): String {
        return buildString {
            append("出厂设置指令:")
            val laser = laser.toUByte().toInt()
            when (state) {
                0x05.toByte() -> append("无较正范围预览")
                0x08.toByte() -> append("较正数据传输完成[$index]")
                0x09.toByte() -> append("激光点跳至指定AD值:x:${adX} y:${adY} laser:$laser type:${type}")
                0x0A.toByte() -> {
                    if (pointList.isEmpty()) {
                        append("激光点跳到指定坐标:x:${x} y:${y}")
                    } else {
                        append("激光点跳到指定坐标[${pointList.size}]: laser:$laser type:${type}")
                        pointList.forEach {
                            append(" (x:${it.x} y:${it.y})")
                        }
                    }
                }

                0x0B.toByte() -> append("激光点预览功率设置:laser:$laser type:${type}")
                0x10.toByte() -> append("校正数据使能:${if (data1 == 0x1.toByte()) "校正数据" else "计算数据"}")
                0x11.toByte() -> append("保存对焦光标坐标值")
                0x14.toByte() -> append("激光功率微调功能:${laserAdjust} type:${type}")
                0x15.toByte() -> append("获取AD值($x,$y)")
                0x21.toByte() -> append("LX1出厂雕刻设置:${(data4 == 0x1.toByte()).toDC()}")
                0x22.toByte() -> append("LX1出厂雕刻补偿设置: 黑白:${data1} 抖动:${data2} ${(data3 == 0x1.toByte()).toDC()}")
            }
            append(" custom:${custom}")
        }
    }
}
