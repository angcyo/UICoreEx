package com.angcyo.bluetooth.fsc.bean

import androidx.annotation.Keep

/**
 * 摄像头模板数据结构
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/06
 */
@Keep
data class CameraTemplateBean(
    /**模板id
     * dft_lp5: dft开头的模版, 就是系统模板
     * */
    var id: String? = null,
    /**模板名称*/
    var name: String? = null,
    /**显示到界面的模板数据*/
    var form: TemplateDataBean? = null,
    /**从图片中截取的模板数据*/
    var data: TemplateDataBean? = null,
) {

    /**是否是系统模板*/
    val isSystemTemplate: Boolean get() = id?.startsWith("dft_") == true

    override fun equals(other: Any?): Boolean {
        return when (other) {
            is CameraTemplateBean -> id == other.id
            else -> false
        }
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }
}

data class TemplateDataBean(
    /**旋转的角度*/
    val angle: Float? = 0f,
    /**旋转的角度*/
    val rotate: Float? = 0f,
    val centerX: Float? = 0f,
    val centerY: Float? = 0f,
    val height: Float? = 0f,
    val x: Float? = 0f,
    val y: Float? = 0f,
    val left: Float? = 0f,
    val top: Float? = 0f,
    val scaleX: Float? = 0f,
    val scaleY: Float? = 0f,
    val skewX: Float? = 0f,
    val skewY: Float? = 0f,
    val width: Float? = 0f,
)