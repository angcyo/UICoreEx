package com.angcyo.bluetooth.fsc.laserpacker.parse

import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper
import com.angcyo.bluetooth.fsc.laserpacker.command.IPacketParser
import com.angcyo.bluetooth.fsc.laserpacker.command.QueryCmd
import com.angcyo.library.component.reader

/**
 * 摄像头域名信息
 * date内容：为域名内容，如："LP-CAM-DA5FB4","192.168.1.85"以0x00结束
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024-9-30
 */
data class QueryCameraInfoParser(
    var name: String? = null,
    var ip: String? = null,
) : IPacketParser<QueryCameraInfoParser> {
    //解析数据
    override fun parse(packet: ByteArray): QueryCameraInfoParser? {
        return try {
            packet.reader {
                offset(LaserPeckerHelper.packetHeadSize)//偏移头部
                offset(1)//偏移长度
                val func = readInt(1)//功能码
                if (func.toByte() != QueryCmd.cameraInfo.commandFunc()) {
                    throw IllegalStateException("非查询指令!")
                }
                val state = readInt(1)
                val custom = readInt(1)
                if (state != QueryCmd.QUERY_CAMERA_INFO.toInt()) {
                    throw IllegalStateException("非查询摄像头信息指令!")
                }
                val data = readStringEnd()
                if (data?.isNotEmpty() == true) {
                    val split = data.split(",")
                    name = split.getOrNull(0)?.trim('"')
                    ip = split.getOrNull(1)?.trim('"')
                }
            }
            this
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    /**主机是否连接摄像头*/
    val isCameraConnected: Boolean
        get() {
            return !name.isNullOrEmpty()
        }


    /**摄像头是否连接上网络*/
    val isCameraConnectedNetwork: Boolean
        get() {
            return ip?.isNotEmpty() == true && ip != "0.0.0.0"
        }

}