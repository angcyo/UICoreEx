package com.angcyo.bluetooth.fsc.laserpacker.bean

import com.angcyo.bluetooth.fsc.laserpacker.command.QueryCmd

/**
 * AT指令返回值
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since  2023-12-20
 */

/**U盘/sd卡文件信息*/
data class FileIndexBean(
    /**
     * 雕刻的索引
     * */
    var index: Int = -1,

    /**雕刻的文件名*/
    var name: String? = null,
    /**
     * [QueryCmd.TYPE_SD]
     * [QueryCmd.TYPE_USB]
     * */
    var mount: Int = 1
) {

    /**
     * 是否是SD卡上的文件, sd卡文件支持雕刻参数带入
     * U盘上的文件, 不支持雕刻参数带入
     * */
    val isSdMount: Boolean
        get() = mount == QueryCmd.TYPE_SD

}