package com.angcyo.bluetooth.fsc.laserpacker.parse

import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper
import com.angcyo.bluetooth.fsc.laserpacker.command.IPacketParser
import com.angcyo.library.component.reader
import com.angcyo.library.ex.toHexInt

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/05/31
 *
 * AD坐标值解析
 */
class FactoryCoordValueParser(
    var adx: Int = 0,
    var ady: Int = 0,
) : IPacketParser<FactoryCoordValueParser> {
    override fun parse(packet: ByteArray): FactoryCoordValueParser? {
        return try {
            packet.reader {
                offset(LaserPeckerHelper.packetHeadSize)//偏移头部

                val length = readByte().toHexInt()
                val func = readByte()
                val state = readByte()
                val rev = readByte()
                val nul = readByte()
                val custom = readByte()

                adx = readInt(2)
                ady = readInt(2)
            }
            this
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }
}