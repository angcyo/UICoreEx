package com.angcyo.bluetooth.fsc.laserpacker.parse

import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper
import com.angcyo.bluetooth.fsc.laserpacker.command.IPacketParser
import com.angcyo.bluetooth.fsc.laserpacker.command.QueryCmd
import com.angcyo.library.component.reader

/**
 * WIFI固件版本
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/05/24
 */
data class QueryWifiVersionParser(
    var wifiVersion: String? = null,
    var custom: Int = 0,
) : IPacketParser<QueryWifiVersionParser> {
    //解析数据
    override fun parse(packet: ByteArray): QueryWifiVersionParser? {
        return try {
            packet.reader {
                offset(LaserPeckerHelper.packetHeadSize)//偏移头部
                offset(1)//偏移长度
                val func = readInt(1)//功能码
                if (func.toByte() != QueryCmd.wifiVersion.commandFunc()) {
                    throw IllegalStateException("非查询指令!")
                }
                val state = readInt(1)
                custom = readInt(1)
                if (state != QueryCmd.QUERY_WIFI_VERSION.toInt()) {
                    throw IllegalStateException("非查询Wifi版本!")
                }
                wifiVersion = readStringEnd()
            }
            this
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }
}