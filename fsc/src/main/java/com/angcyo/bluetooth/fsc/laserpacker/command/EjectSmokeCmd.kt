package com.angcyo.bluetooth.fsc.laserpacker.command

import com.angcyo.library.ex.toHexString

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/05/09
 * 排烟指令
 */
data class EjectSmokeCmd(
    /**是否打开风扇, 否则就是关闭风扇*/
    val open: Boolean,
    /**LEVEL为风速等级分0 - 255级*/
    val level: Int = 0,
    /**time为风速保持时长。单位为秒，占2个字节。*/
    val time: Int = 0,
) : BaseCommand() {

    override fun commandFunc(): Byte = 0x09

    override fun toCommandLogString(): String = buildString {
        append("排烟指令[${toByteArray().toHexString()}]:")
        append(if (open) "打开" else "关闭")
        append(" 风扇, 等级:$level, 时长:${time}s")
    }

    override fun toByteArray(): ByteArray = commandByteWriter {
        write(commandFunc())
        write(if (open) 1 else 0)
        write(level)
        write(time, 2)
    }
}