package com.angcyo.bluetooth.fsc.laserpacker.command

import com.angcyo.library.annotation.MM
import com.angcyo.library.ex.removeAll
import kotlin.math.roundToInt

/**
 * 保护罩设置指令
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/09
 */
data class CoverSettingCmd(

    /**
     * 当state = 1时，进入保护罩标定
     * 当state = 2时，标定确定。
     * 当state = 3时，退出保护罩调试状态。
     * 当state = 4时，表示设置合规保护罩led灯亮度等级，0% ~ 100%。
     * 当state = 5时，表示设置排烟风速等级，0% ~ 100%。
     * 当state = 6时，表示设置排烟时长，单位秒。0~65535秒
     * 当state = 7时，表示电动支架调节。
     * 当state = 8时，表示取消报警。
     * */
    var state: Byte = 0x0,

    //dir为方向，0为向下，1为向上，3为自动。
    var dir: Byte = 3,
    //dist移动距离，单位0.01mm。
    var dist: Int = 0,
    //focal焦距，单位0.01mm。
    var focal: Int = 0,

    //LED灯等级 合规保护罩led灯亮度等级，0% ~ 100%。
    var ledLevel: Int = 0,
    //风扇等级 排烟风速等级，0% ~ 100%。
    var fanLevel: Int = 0,
    //排烟时长 排烟时长，单位秒。0~65535秒
    var fanTime: Int = 0,
) : BaseCommand() {

    companion object {
        /**保护罩支架自动移动
         * [height] 高度,0.01mm
         * [focal] 焦距
         * ```
         * AABB 05 09 00 00 0009 //0为静止
         * AABB 05 09 00 01 000A //1为运动中
         * ```
         * */
        fun coverBracketAutoCmd(@MM height: Float, @MM focal: Float): CoverSettingCmd {
            return CoverSettingCmd(0x07).apply {
                dir = 3
                dist = (height * 100).roundToInt()
                this.focal = (focal * 100).roundToInt()
            }
        }
    }

    override fun commandFunc(): Byte = 0x09

    override fun toByteArray(): ByteArray {
        return commandByteWriter {
            writeUByte(commandFunc())
            writeUByte(state)

            //设置参数
            if (state.toInt() == 4) {
                write(ledLevel, 2)
            } else if (state.toInt() == 5) {
                write(fanLevel, 2)
            } else if (state.toInt() == 6) {
                write(fanTime, 2)
            } else if (state.toInt() == 7) {
                write(dir)
                write(dist, 2)
                write(focal, 2)
            }
            //
        }
    }

    override fun toCommandLogString(): String = buildString {
        append(toHexCommandString().removeAll())
        if (state.toInt() == 7) {
            append(" 保护罩支架调节:dir:${dir} dist:${dist} focal:${focal}")
        } else {
            append(" 保护罩设置:${state} ${this@CoverSettingCmd}")
        }
    }
}

/**取消报警指令*/
data class CancelFlameAlarmCmd(
    val custom: Int = 0,
) : BaseCommand() {

    override fun commandFunc(): Byte = 0xfd.toByte()

    override fun toByteArray(): ByteArray {
        return commandByteWriter {
            writeUByte(commandFunc())
        }
    }

    override fun toCommandLogString(): String = buildString {
        append(toHexCommandString().removeAll())
        append(" 取消报警")
    }
}