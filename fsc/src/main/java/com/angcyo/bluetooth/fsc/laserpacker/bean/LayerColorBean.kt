package com.angcyo.bluetooth.fsc.laserpacker.bean

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/04/16
 */
data class LayerColorBean(
    /**图层颜色, 如果颜色为空, 则表示清空图层的意思*/
    var color: String? = null,
    /**颜色对应的名称/序号*/
    var name: String? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (javaClass != other?.javaClass) return false
        return other is LayerColorBean && color == other.color
    }

    override fun hashCode(): Int {
        return color?.hashCode() ?: 0
    }
}
