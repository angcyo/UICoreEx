package com.angcyo.bluetooth.fsc.laserpacker.parse

import com.angcyo.bluetooth.fsc.R
import com.angcyo.bluetooth.fsc.laserpacker.DeviceStateModel
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerModel
import com.angcyo.bluetooth.fsc.laserpacker._deviceSettingBean
import com.angcyo.bluetooth.fsc.laserpacker.bean.matchesProductVersion
import com.angcyo.bluetooth.fsc.laserpacker.command.IPacketParser
import com.angcyo.bluetooth.fsc.laserpacker.command.QueryCmd
import com.angcyo.core.vmApp
import com.angcyo.library.component.reader
import com.angcyo.library.ex._string
import com.angcyo.library.ex.appendLineIfNotEmpty
import com.angcyo.library.ex.bit
import com.angcyo.library.ex.low4Bit
import com.angcyo.library.ex.nowTime

/**
 * 蓝牙设备状态数据结构
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2022/03/25
 */
//DeviceStateParser(mode=2, workState=2, rate=0, laser=0, speed=0, error=0, state=0, name=0, temp=0, custom=0, zConnect=0, printTimes=1, angle=0)
data class QueryStateParser(
    //设备当前工作模式
    //L1当前工作模式，
    // 0x01为打印模式，
    // 0x02为打印预览模式，
    // 0x04为调焦模式，
    // 0x05为文件下载模式，
    // 0x06为空闲模式，
    // 0x07为关机状态，
    // 0x08为设置模式，
    // 0x09为下载模式。
    var mode: Int = WORK_MODE_IDLE,
    //当前模式下的工作状态, 暂停, 255:结束
    //空闲模式下: 0xff
    //打印模式下: 工作中0x01，暂停0x04，结束0x03 继续打印0x02 0x05停止打印
    //打印预览模式下: 0x01:预览图片 0x02:预览范围 0x04:第三轴暂停预览 0x05 第三轴继续预览 0x06:支架调整 0x07:显示中心 0x08:4点预览
    //开始打印: 0x01 从头开始打印文件，0x02继续打印文件，0x03结束打印，0x04暂停打印，0x05多文件打印，0x06文件名打印（L5新增）
    var workState: Int = 0,
    //打印进度百分比[0-100]
    var rate: Int = 0,
    //激光强度[1-100]
    var laser: Int = 0,
    //激光打印速度[1-100]
    var speed: Int = 0,
    //错误状态
    //0表示无错误状态
    //1表示不处于安全状态，且自由模式没开。
    //2打印超过边界报警
    //3激光工作温度报警
    //4打印过程中移动设备报警
    //5打印过程中遮挡激光报警
    //6打印数据错误
    //7文件编号查询错误
    //8陀螺仪自检错误
    //9flash自检错误
    //10 图片范围错误
    //11 火灭报警
    var error: Int = 0,
    //发送查询指令时传递的state
    //0时表示查询工作状态
    //1时表示查询文件列表
    //2时表示查询设置状态
    //3时表示查询版本
    //4时表示查询安全码与用户帐号
    var state: Int = 0,
    //当前正在打印的文件编号(文本编号对应本地存储的文件信息)
    var index: Int = 0,
    //工作温度, 精度1度
    var temp: Byte = 0,
    //发送指令时, 自定义的数据
    var custom: Int = -1,
    //Z轴连接状态, 0未连接, 1连接
    var zConnect: Int = 0,
    //当前图片打印的次数,从1开始
    var printTimes: Int = 0,
    //设备与水平面的平角
    var angle: Int = 0,
    //雕刻模块识别位（C1专用位）
    //0 5W激光
    //1 10W激光
    //2 20W激光
    //3 1064激光
    //4 单色笔模式
    //5 彩色笔模式
    //6 刀切割模式
    //7 CNC模式
    //255 激光设备未插好
    var moduleState: Int = -1,
    //旋转轴连接状态, 0未连接, 1连接
    var rConnect: Int = 0,
    //滑台连接状态, 0未连接, 1连接
    var sConnect: Int = 0,
    //C1 移动平台连接状态, 0未连接, 1连接
    var carConnect: Int = 0,

    /**0时无连接，1时蓝牙连接，2时为USB连接。
     * [com.angcyo.bluetooth.fsc.laserpacker.parse.QueryStateParser.CONNECT_TYPE_BLE]
     * [com.angcyo.bluetooth.fsc.laserpacker.parse.QueryStateParser.CONNECT_TYPE_USB]
     * */
    var usbConnect: Int = 0,

    /**2024-4-13
     * 1为急停状态, 1字节
     * */
    var stop: Int = 0,

    /** 2024-4-13
     * 1未检测到密钥, 1字节
     * 0未检测到密钥
     * */
    var safeKey: Int = 1,

    /**2024-4-13
     * 2个字节
     * 合规保护罩错误状态，此错误状态占用2个字节，每一位表示一种错误情况：
     * bit0: 保护罩连接状态，1连接，0未连接。
     * bit1: 急停开关错误位，1急停，0未急停；
     * bit2: 火焰检测报警，1报警，0未报警;
     * bit3: 顶盖检测，1关，0未关；
     * bit4: 前门检测，1关，0未关；
     * bit5: 后门检测，1关，0未关；
     * bit6: 主机摆放检测，1为摆放正常，0摆放异常；//2024年5月21日L5
     * bit8:小火报警，1报警，0未报警;
     * bit9:中火报警，1报警，0未报警;
     * bit10:大火报警，1报警，0未报警;
     * */
    var cover: Int = 0,

    /**
     * cover_conn: 便携保护罩连接状态， 1为连接，0为未连接。//2024年4月28日L5
     * */
    var coverConnect: Int = 1,

    /**
     *  1时为欧规，0时为常规。//2024年4月28日L5
     * */
    var standard: Int = 0,

    //---

    var stateTime: Long = nowTime(), //app数据时间
    var deviceAddress: String? = null, //app数据, 当前数据的设备地址
) : IPacketParser<QueryStateParser> {

    companion object {

        /**0x01为打印模式*/
        const val WORK_MODE_ENGRAVE = 0x01

        /**0x02为打印预览模式*/
        const val WORK_MODE_ENGRAVE_PREVIEW = 0x02

        /**0x03为结束预览模式*/
        const val WORK_MODE_STOP_ENGRAVE_PREVIEW = 0x03

        /**0x04为调焦模式*/
        const val WORK_MODE_FOCUSING = 0x04

        /**0x05为文件下载模式*/
        const val WORK_MODE_FILE_DOWNLOAD = 0x05

        /**0x06为空闲模式*/
        const val WORK_MODE_IDLE = 0x06

        /**0x07为关机状态*/
        const val WORK_MODE_SHUTDOWN = 0x07

        /**0x08为设置模式*/
        const val WORK_MODE_SETUP = 0x08

        /**0x09为下载模式(工厂)*/
        const val WORK_MODE_DOWNLOAD = 0x09

        //---

        /**当前是蓝牙连接*/
        const val CONNECT_TYPE_BLE = 1

        /**当前是USB连接*/
        const val CONNECT_TYPE_USB = 2
    }

    /**工作状态*/
    val _workState: Int
        get() = workState.toByte().low4Bit().toInt()

    //解析数据
    override fun parse(packet: ByteArray): QueryStateParser? {
        return try {
            //AABB 17 00 06 000000000000000000000000000100010000000008
            packet.reader {
                keepLastSize = LaserPeckerHelper.CHECK_SIZE
                offset(LaserPeckerHelper.packetHeadSize)//偏移头部
                val length = readInt(1)//长度
                val func = readInt(1)//功能码
                if (func.toByte() != QueryCmd.workState.commandFunc()) {
                    throw IllegalStateException("非查询指令!")
                }
                if (length < 2) {
                    throw IllegalStateException("无效的指令返回值!")
                }
                mode = readInt(1, mode)
                workState = readInt(1, workState)
                rate = readInt(1, rate)
                laser = readInt(1, laser)
                speed = readInt(1, speed)
                error = readInt(1, error)
                state = readInt(1, state)
                index = readInt(4, index)
                temp = readByte(temp)
                custom = readInt(1, custom)
                zConnect = readInt(1, zConnect)
                printTimes = readInt(1, printTimes)
                angle = readInt(1, angle)
                moduleState = readInt(1, moduleState)
                rConnect = readInt(1, rConnect)
                sConnect = readInt(1, sConnect)
                carConnect = readInt(1, carConnect)
                usbConnect = readInt(1, usbConnect)
                //2024-4-13
                stop = readInt(1, stop)
                safeKey = readInt(1, safeKey)
                cover = readInt(2, cover)
                //2024-5-8
                coverConnect = readInt(1, coverConnect)
                standard = readInt(1, standard)

                //最后的设备地址
                deviceAddress = LaserPeckerHelper.lastDeviceAddress()
            }
            this
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    //region ------Engrave------

    /**是否是文件名雕刻*/
    fun isFileNameEngrave(): Boolean = mode == WORK_MODE_ENGRAVE && _workState == 0x06

    /**打印模式下, 打印是否暂停了*/
    fun isEngravePause(): Boolean = mode == WORK_MODE_ENGRAVE && _workState == 0x04

    /**打印模式下, 打印是否正在打印*/
    fun isEngraving(): Boolean =
        mode == WORK_MODE_ENGRAVE && (_workState == 0x01 || _workState == 0x02 || _workState == 0x05 || _workState == 0x06)

    /**打印模式下, 打印是否正在停止,或者停止了*/
    fun isEngraveStop(): Boolean = mode == WORK_MODE_ENGRAVE && _workState == 0x03

    //endregion

    //region ------Mode------

    /**设备是否处于空闲模式*/
    fun isModeIdle() = mode == WORK_MODE_IDLE

    /**空闲状态, 并且没有错误*/
    fun isModeIdleAndNoError() = mode == WORK_MODE_IDLE && error == 0

    /**设备是否处于雕刻模式*/
    fun isModeEngrave() = mode == WORK_MODE_ENGRAVE

    /**雕刻预览模式*/
    fun isModeEngravePreview() = mode == WORK_MODE_ENGRAVE_PREVIEW

    /**雕刻预览模式
     * [com.angcyo.bluetooth.fsc.laserpacker.DeviceStateModel.isShutdownMode]*/
    fun isModeShutdown() = mode == WORK_MODE_SHUTDOWN

    //endregion
}

/**转换成模式状态文本
 * [includeModeState] 是否要包含当前模式的状态提示?*/
fun QueryStateParser.toDeviceStateString(includeModeState: Boolean = true): String? {
    val builder = StringBuilder()

    if (includeModeState) {
        when (mode) {
            QueryStateParser.WORK_MODE_ENGRAVE -> {
                when (workState) {
                    0x03 -> builder.append(_string(R.string.engrave_state_stop))
                    0x04 -> builder.append(_string(R.string.engrave_state_pause))
                    else -> builder.append(_string(R.string.is_engraving))
                }
            }

            QueryStateParser.WORK_MODE_ENGRAVE_PREVIEW -> {
                when (workState) {
                    0x01 -> builder.append(_string(R.string.preview_state_gcode))
                    0x02 -> builder.append(_string(R.string.preview_state_range))

                    0x04, 0x05 -> {
                        val peckerModel = vmApp<LaserPeckerModel>()
                        val type =
                            if (peckerModel.isROpen()) 3 else if (peckerModel.isSOpen()) 2 else if (peckerModel.isZOpen()) 1 else 0
                        if (type > 0) {
                            when (workState) {
                                0x04 -> builder.append(
                                    _string(R.string.preview_state_z_pause2, type.toDeviceStr())
                                )

                                0x05 -> builder.append(
                                    _string(R.string.preview_state_z_continue2, type.toDeviceStr())
                                )
                            }
                        } else {
                            when (workState) {
                                0x04 -> builder.append(_string(R.string.preview_state_z_pause))
                                0x05 -> builder.append(_string(R.string.preview_state_z_continue))
                            }
                        }
                    }

                    0x06 -> builder.append(_string(R.string.preview_state_bracket))
                    0x07 -> builder.append(_string(R.string.preview_state_center))
                    0x08 -> builder.append(_string(R.string.preview_state_points))
                    0x0A -> builder.append(_string(R.string.preview_state_z_scroll))
                }
            }

            QueryStateParser.WORK_MODE_FOCUSING -> builder.append(_string(R.string.work_mode_focusing))
            QueryStateParser.WORK_MODE_FILE_DOWNLOAD -> builder.append(_string(R.string.work_mode_file_download))
            QueryStateParser.WORK_MODE_SHUTDOWN -> builder.append(_string(R.string.work_mode_shutdown))
            QueryStateParser.WORK_MODE_SETUP -> builder.append(_string(R.string.work_mode_setup))
            QueryStateParser.WORK_MODE_DOWNLOAD -> builder.append(_string(R.string.work_mode_download))
        }
    }

    //错误信息
    if (error != 0) {
        builder.appendLineIfNotEmpty()
        builder.append(error.toErrorStateString(standard))
    }

    //C1 激光头未插好提示
    if (moduleState == 255) {
        builder.appendLineIfNotEmpty()
        builder.append(_string(R.string.laser_not_plugged_tip))
    }

    //是否是自由模式, 自由模式下忽略常规保护罩的未连接提示
    //所以这里需要先查设置状态, 再查状态
    val deviceSetting = vmApp<LaserPeckerModel>().deviceSettingData.value
    val isFreeMode = deviceSetting?.free == 1 /*&& deviceSetting.safe == 1*/

    //常规, 自由模式下忽略保护罩未连接提示
    val ignoreNoCover = isFreeMode && standard == 0

    //保护罩错误状态
    if (_deviceSettingBean?.supportComplianceCoverRange.matchesProductVersion()) {/*合规保护罩错误状态提示*/
        if (ignoreNoCover) {
            //自由模式, 忽略提示
        } else if (isCoverConnect) {
            //合规保护罩已连接, 检查是否有错误提示
            cover.toCoverStateString()?.let {
                builder.appendLineIfNotEmpty()
                builder.append(it)
            }
        } else if (coverConnect == 1) {
            //便携保护罩已连接
            if (isCoverUnsafeState) {

            }
        } else {
            //未连接保护罩
        }
    }

    //急停状态
    if (stop == 1) {
        builder.appendLineIfNotEmpty()
        builder.append(_string(R.string.stop_state_tip))
    }

    //未检测到密钥
    if (_deviceSettingBean?.supportSafeKeyRange.matchesProductVersion() && safeKey == 0) {
        builder.appendLineIfNotEmpty()
        builder.append(_string(R.string.safe_key_tip))
    }

    return builder.toString()
}

/**机器错误码对应的提示语
 * [standard].[QueryStateParser.standard]
 * */
fun Int.toErrorStateString(standard: Int = 0) = when (this) {
    1 -> if (standard == 1) _string(R.string.ex_tips_one2_safe) else _string(R.string.ex_tips_one2)
    2 -> _string(R.string.ex_tips_two)
    3 -> _string(R.string.ex_tips_three)
    4 -> _string(R.string.ex_tips_four)
    5 -> _string(R.string.ex_tips_five)
    6 -> _string(R.string.ex_tips_six)
    7 -> _string(R.string.ex_tips_seven)
    8 -> _string(R.string.ex_tips_eight)
    9 -> _string(R.string.ex_tips_nine)
    10 -> _string(R.string.ex_tips_ten)
    11 -> _string(R.string.ex_tips_eleven)
    0 -> null
    else -> "error $this"
}

/**保护罩错误状态对应的提示语*/
fun Int.toCoverStateString() = when {
    bit(1) == 0 -> _string(R.string.no_cover_tip)
    //bit(2) == 1 -> "急停"
    bit(3) == 1 -> _string(R.string.flame_warning_tip)
    bit(4) == 0 -> _string(R.string.head_cover_tip)
    bit(5) == 0 -> _string(R.string.front_gate_tip)
    bit(6) == 0 -> _string(R.string.back_gate_tip)
    bit(7) == 0 -> _string(R.string.put_warning_tip)
    else -> null
}

/**转成对应设备字符串*/
fun Int.toDeviceStr(): String = when (this) {
    1 -> _string(R.string.device_ex_z_label) //z轴
    2 -> _string(R.string.device_ex_s_label) //s轴
    3 -> _string(R.string.device_ex_r_label) //r轴
    else -> ""
}

/**[LaserPeckerModel.getExDeviceMode]*//*fun Int.toExDeviceStr(): String = when (this) {
    1 -> _string(R.string.device_ex_z_label) //z轴
    2 -> _string(R.string.device_ex_s_label) //s轴
    3 -> _string(R.string.device_ex_r_label) //r轴
    else -> ""
}*/

fun String.toDeviceStr(): String = when (this) {
    QuerySettingParser.EX_Z -> _string(R.string.device_ex_z_label) //z轴
    QuerySettingParser.EX_S -> _string(R.string.device_ex_s_label) //s轴
    QuerySettingParser.EX_R -> _string(R.string.device_ex_r_label) //r轴
    QuerySettingParser.EX_CAR -> _string(R.string.engrave_module_car) //car
    else -> ""
}

/**当前设备是否是欧规模式*/
val isEuropeanStandard
    get() = _deviceSettingBean?.supportEuropeanStandardRange.matchesProductVersion() && vmApp<DeviceStateModel>().deviceStateData.value?.standard == 1

/**是否是不安全的状态, 通用
 * 连接了保护罩, 但是没有关门*/
val isCoverUnsafeState get() = vmApp<DeviceStateModel>().deviceStateData.value?.error == 1

/**LP5 安全密钥是否未插入*/
val isNoSafeKey get() = _deviceSettingBean?.supportSafeKeyRange.matchesProductVersion() && vmApp<DeviceStateModel>().deviceStateData.value?.safeKey == 0

/**是否连接了合规保护罩, 非便携保护罩*/
val isCoverConnect
    get() = _deviceSettingBean?.supportComplianceCoverRange.matchesProductVersion() && vmApp<DeviceStateModel>().deviceStateData.value?.cover?.bit(
        1
    ) == 1

/**
 * 当前设备是否连接了合规保护罩和便携保护罩
 * LP5是否安装了保护罩/连接了保护罩, 合规/便携保护罩*/
val isCoverConnectAny
    get() = isCoverConnect || vmApp<DeviceStateModel>().deviceStateData.value?.coverConnect == 1