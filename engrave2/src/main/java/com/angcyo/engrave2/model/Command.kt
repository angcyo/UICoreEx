package com.angcyo.engrave2.model

import com.angcyo.bluetooth.fsc.IReceiveBeanAction
import com.angcyo.bluetooth.fsc.ReceivePacket
import com.angcyo.bluetooth.fsc.enqueue
import com.angcyo.bluetooth.fsc.laserpacker.command.ExitCmd
import com.angcyo.bluetooth.fsc.laserpacker.command.ICommand
import com.angcyo.bluetooth.fsc.laserpacker.command.QueryCmd
import com.angcyo.bluetooth.fsc.laserpacker.parse.QueryStateParser
import com.angcyo.bluetooth.fsc.parse
import com.angcyo.library.component._delay
import com.angcyo.library.toastQQ

/**
 * 指令
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2023/06/29
 */
object Command {

    /**退出指令*/
    fun wrapExit(
        action: IReceiveBeanAction = { bean: ReceivePacket?, error: Exception? ->
            error?.let {
                toastQQ(it.message)
            }
        }
    ) {
        ExitCmd().appEnqueue { bean, error ->
            action(bean, error)
        }
    }

    /**监听设备状态
     * [action] 是否退出
     * */
    fun listenerExit(action: (Boolean) -> Unit) {
        listenerState { state, error ->
            if (error != null) {
                action(false)
                true
            } else if (state?.isModeIdle() == true) {
                action(true)
                true
            } else {
                false
            }
        }
    }

    /**监听设备的状态
     * [action] 返回true表示处理, 否则继续监听
     * */
    fun listenerState(action: (state: QueryStateParser?, error: Exception?) -> Boolean) {
        QueryCmd.workState.enqueue { bean, error ->
            if (action(bean?.parse<QueryStateParser>(), error)) {
                //处理
            } else {
                //1s后继续监听
                _delay(1000L) {
                    listenerState(action)
                }
            }
        }
    }
}

/**指令入队列*/
fun ICommand.appEnqueue(
    action: IReceiveBeanAction = { bean: ReceivePacket?, error: Exception? ->
        error?.let {
            toastQQ(it.message)
        }
    }
) {
    enqueue(action = action)
}
