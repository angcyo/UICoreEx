package com.angcyo.engrave2.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.angcyo.bluetooth.fsc.enqueue
import com.angcyo.bluetooth.fsc.laserpacker.DeviceStateModel
import com.angcyo.bluetooth.fsc.laserpacker.command.EngraveCmd
import com.angcyo.bluetooth.fsc.laserpacker.command.ExitCmd
import com.angcyo.bluetooth.fsc.laserpacker.syncQueryDeviceState
import com.angcyo.core.vmApp
import com.angcyo.dialog.MessageDialogConfig
import com.angcyo.engrave2.R
import com.angcyo.library._screenHeight
import com.angcyo.library._screenWidth
import com.angcyo.library.component.pad.isInPadMode
import com.angcyo.library.ex._string
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.progress.DslProgressBar
import kotlin.math.min

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/06/05
 *
 * 雕刻状态恢复对话框, 提供恢复雕刻/停止雕刻/进度显示
 */
class EngraveStateRestoreDialog(context: Context? = null) : MessageDialogConfig(context) {

    val deviceStateModel = vmApp<DeviceStateModel>()

    init {
        dialogLayoutId = R.layout.dialog_engrave_state_restore_layout
        dialogBgDrawable = ColorDrawable(Color.TRANSPARENT)

        dialogTitle = _string(R.string.device_engrave_ing_title)
        dialogMessage = _string(R.string.device_engrave_ing_des)
        negativeButtonText = _string(R.string.dialog_negative)
    }

    override fun initControlLayout(dialog: Dialog, dialogViewHolder: DslViewHolder) {
        super.initControlLayout(dialog, dialogViewHolder)

        deviceStateModel.startLoopCheckState(reason = "恢复雕刻进度")
        deviceStateModel.deviceStateData.observe(this) { queryState ->
            if (queryState != null) {
                if (queryState.isEngravePause()) {
                    //暂停了雕刻
                    dialogViewHolder.visible(R.id.engrave_continue_line)
                    dialogViewHolder.visible(R.id.engrave_continue_button)
                    dialogViewHolder.gone(R.id.engrave_pause_line)
                    dialogViewHolder.gone(R.id.engrave_pause_button)
                } else {
                    dialogViewHolder.gone(R.id.engrave_continue_line)
                    dialogViewHolder.gone(R.id.engrave_continue_button)
                    dialogViewHolder.visible(R.id.engrave_pause_line)
                    dialogViewHolder.visible(R.id.engrave_pause_button)
                }

                dialogViewHolder.v<DslProgressBar>(R.id.lib_progress_bar)
                    ?.setProgress(queryState.rate)
            }
        }

        dialogViewHolder.click(R.id.engrave_continue_button) {
            EngraveCmd.continueEngrave().enqueue()
        }

        dialogViewHolder.click(R.id.engrave_pause_button) {
            EngraveCmd.pauseEngrave().enqueue()
        }

        dialogViewHolder.click(R.id.engrave_stop_button) {
            dialog.dismiss()
            ExitCmd().enqueue()
            syncQueryDeviceState()
        }
    }
}

fun Context.engraveStateRestoreDialog(config: EngraveStateRestoreDialog.() -> Unit = {}): Dialog {
    return EngraveStateRestoreDialog(this).run {
        dialogWidth = -1
        if (isInPadMode()) {
            dialogWidth = min(_screenWidth, _screenHeight)
        }
        canceledOnTouchOutside = false
        config()
        show()
    }
}