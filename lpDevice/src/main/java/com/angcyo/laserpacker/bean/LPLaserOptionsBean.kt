package com.angcyo.laserpacker.bean

import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper
import com.angcyo.laserpacker.LPDataConstant
import com.angcyo.library.annotation.MM
import com.angcyo.library.ex.toStr
import com.angcyo.library.unit.toMm
import com.angcyo.objectbox.findLast
import com.angcyo.objectbox.laser.pecker.LPBox
import com.angcyo.objectbox.laser.pecker.bean.getLayerConfig
import com.angcyo.objectbox.laser.pecker.entity.EngraveConfigEntity
import com.angcyo.objectbox.laser.pecker.entity.MaterialEntity
import com.angcyo.objectbox.laser.pecker.entity.MaterialEntity_
import com.angcyo.objectbox.laser.pecker.entity.TransferConfigEntity
import com.angcyo.objectbox.laser.pecker.entity.TransferConfigEntity_

/**
 * 图层参数
 *
 * [com.angcyo.objectbox.laser.pecker.entity.MaterialEntity]
 *
 * https://www.showdoc.com.cn/2057569273029235/10372848926666581
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2023/07/18
 */
data class LPLaserOptionsBean(

    //region ---数据相关---

    /**版本号*/
    var version: Int = 1,

    /**旋转轴直径*/
    @MM var diameter: Float = HawkEngraveKeys.lastDiameterPixel.toMm(),

    /**[com.angcyo.objectbox.laser.pecker.entity.MaterialEntity.layerId]*/
    var layerId: String? = null,

    /**[layerId] 的动态计算的值, 不进入属性, 只用来内存使用*/
    var _layerId: String? = null,

    /**需要发给机器的数据模式
     * [layerId] 相同图层的数据模式可能不同
     * [LPDataConstant.DATA_MODE_GREY] 灰度数据
     * [LPDataConstant.DATA_MODE_BLACK_WHITE] 黑白数据
     * [LPDataConstant.DATA_MODE_DITHERING] 抖动数据
     * [LPDataConstant.DATA_MODE_GCODE] gcode数据
     * */
    var dataMode: Int? = null,

    /**2024-4-22 切割类型:
     * [com.angcyo.laserpacker.bean.LPElementBean.cutType]
     * */
    var cutType: String? = null,

    /**2024-4-24 浮雕类型:
     * [com.angcyo.laserpacker.bean.LPElementBean.reliefType]
     * */
    var reliefType: String? = null,

    //endregion ---数据相关---

    //region ---材质相关---

    /**PC端*/
    var materialId: String? = null,

    /**[com.angcyo.objectbox.laser.pecker.entity.MaterialEntity.code]
     * 材质位置code吗*/
    var materialCode: String? = null,

    /**[com.angcyo.objectbox.laser.pecker.entity.MaterialEntity.resIdStr]
     * 系统默认的材质国际化资源key*/
    var materialKey: String? = null,

    /**[com.angcyo.objectbox.laser.pecker.entity.MaterialEntity.name]
     * 自定义的材质名称, 不支持国际化*/
    var materialName: String? = null,

    //endregion ---材质相关---

    //region ---机器雕刻相关---

    /**[com.angcyo.objectbox.laser.pecker.entity.MaterialEntity.type]*/
    var lightSource: Int = 0,

    /**[com.angcyo.objectbox.laser.pecker.entity.MaterialEntity.power]*/
    var printPower: Int = 0,

    /**[com.angcyo.objectbox.laser.pecker.entity.MaterialEntity.depth]*/
    var printDepth: Int = 0,

    /**直接指定速度 mm/s */
    var printSpeed: Int? = null,

    /**雕刻次数
     * [com.angcyo.objectbox.laser.pecker.entity.MaterialEntity.count]*/
    var printCount: Int = 1,

    /**[com.angcyo.objectbox.laser.pecker.entity.MaterialEntity.precision]*/
    var precision: Int = 0,

    /**[com.angcyo.objectbox.laser.pecker.entity.MaterialEntity.dpi]*/
    var dpi: Float = 0f,

    /**[com.angcyo.laserpacker.bean.LPElementBean.pump]
     *气泵等级/风速等级, Fan_speed_fill * 168 + 25000
     * [0~255]
     * */
    var pump: Int = 0,

    /**是否要使用激光功率
     * [laserFrequency]*/
    var useLaserFrequency: Boolean = false,

    /** [com.angcyo.objectbox.laser.pecker.entity.MaterialEntity.laserFrequency]
     * */
    var laserFrequency: Int? = null,

    //endregion ---机器雕刻相关---

)

/**[getProjectMaterialList]*/
fun EngraveConfigEntity.toLaserOptionsBean(): LPLaserOptionsBean {
    val laser = LPLaserOptionsBean()

    laser.layerId = layerId
    //bean.dataMode = layerId

    laser.materialCode = materialCode
    laser.materialKey = materialKey
    laser.materialName = materialName
    MaterialEntity::class.findLast(LPBox.PACKAGE_NAME) {
        apply(MaterialEntity_.code.equal("$materialCode"))
    }?.let {
        laser.materialName = it.toText()?.toStr()
    }

    laser.lightSource = type.toInt()
    laser.printCount = time
    laser.diameter = diameterPixel.toMm()
    laser.precision = precision
    laser.printPower = power
    laser.printDepth = depth
    //laser.printSpeed = printSpeed
    laser.useLaserFrequency = useLaserFrequency
    laser.laserFrequency = laserFrequency
    laser.pump = pump

    TransferConfigEntity::class.findLast(LPBox.PACKAGE_NAME) {
        apply(TransferConfigEntity_.taskId.equal("$taskId"))
    }?.apply {
        laser.dpi = layerJson?.getLayerConfig(layerId)?.dpi ?: LaserPeckerHelper.DPI_254
    }

    return laser
}

/**从[LPElementBean]结构中获取参数相关结构*/
fun LPElementBean.toLaserOptionsBean(): LPLaserOptionsBean {
    val laser = LPLaserOptionsBean()

    laser.layerId = layerId
    laser._layerId = _layerId
    laser.dataMode = _layerMode

    //laser.layerColor = layerColor
    laser.cutType = cutType
    laser.reliefType = reliefType

    laser.materialCode = materialCode
    laser.materialKey = materialKey
    laser.materialName = materialName
    MaterialEntity::class.findLast(LPBox.PACKAGE_NAME) {
        apply(MaterialEntity_.code.equal("$materialCode"))
    }?.let {
        laser.materialName = it.toText()?.toStr()
    }

    //LaserPeckerHelper.findProductSupportLaserTypeList().firstOrNull()?.type
    laser.lightSource = printType ?: LaserPeckerHelper.LASER_TYPE_BLUE.toInt()
    laser.printCount = printCount ?: 1
    laser.diameter = HawkEngraveKeys.lastDiameterPixel.toMm()
    laser.precision = printPrecision ?: HawkEngraveKeys.lastPrecision
    laser.printPower = printPower ?: HawkEngraveKeys.lastPower
    laser.printDepth = printDepth ?: HawkEngraveKeys.lastDepth
    laser.printSpeed = printSpeed
    laser.useLaserFrequency = useLaserFrequency
    laser.laserFrequency = laserFrequency
    laser.pump = pump ?: 1
    laser.dpi = dpi ?: LaserPeckerHelper.DPI_254

    //反向塞到[LPElementBean]
    updateFromLaserOptionsBean(laser)

    return laser
}

/**将[LPLaserOptionsBean]参数结构,更新到[LPElementBean]*/
fun LPElementBean.updateFromLaserOptionsBean(laser: LPLaserOptionsBean?) {
    if (laser == null) return
    layerId = laser.layerId

    materialCode = laser.materialCode
    materialKey = laser.materialKey
    materialName = laser.materialName

    printPower = laser.printPower
    printDepth = laser.printDepth
    printSpeed = laser.printSpeed
    printType = laser.lightSource
    printCount = laser.printCount
    printPrecision = laser.precision
    useLaserFrequency = laser.useLaserFrequency
    laserFrequency = laser.laserFrequency
    pump = laser.pump
    dpi = laser.dpi

    //layerColor = laser.layerColor
    cutType = laser.cutType
    reliefType = laser.reliefType
}

fun LPElementBean.toMaterialEntity(): MaterialEntity {
    val material = MaterialEntity()
    material.code = materialCode ?: material.code
    material.resIdStr = materialKey
    material.name = materialName

    material.dpi = dpi ?: material.dpi
    //entity.dpiScale = dpiScale ?: entity.dpiScale
    material.type = printType ?: material.type
    material.power = printPower ?: material.power
    material.depth = printDepth ?: material.depth
    //material.speed = printSpeed ?: material.speed
    material.count = printCount ?: material.count
    material.precision = printPrecision ?: material.precision
    material.laserFrequency = laserFrequency
    material.useLaserFrequency = useLaserFrequency

    //entity.pump = pump

    return material
}

fun LPLaserOptionsBean.toMaterialEntity(): MaterialEntity {
    val material = MaterialEntity()
    material.code = materialCode ?: material.code
    material.resIdStr = materialKey
    material.name = materialName

    material.dpi = dpi
    //entity.dpiScale = dpiScale ?: entity.dpiScale
    material.type = lightSource
    material.power = printPower
    material.depth = printDepth
    material.count = printCount
    material.precision = precision
    material.useLaserFrequency = useLaserFrequency
    material.laserFrequency = laserFrequency
    //entity.pump = pump

    return material
}