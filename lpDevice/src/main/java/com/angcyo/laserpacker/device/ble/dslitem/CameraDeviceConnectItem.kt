package com.angcyo.laserpacker.device.ble.dslitem

import com.angcyo.bluetooth.fsc.CameraApiModel
import com.angcyo.bluetooth.fsc.CameraDevice
import com.angcyo.core.vmApp
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.item.DslSingleSelectedTextItem
import com.angcyo.viewmodel.updateValue
import com.angcyo.widget.DslViewHolder

/**
 * 摄像头连接列表的item
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/05
 */
class CameraDeviceConnectItem : DslSingleSelectedTextItem() {

    val cameraApi = vmApp<CameraApiModel>()

    /**设备信息*/
    var itemCameraDevice: CameraDevice? = null

    /**是否是选中的摄像头*/
    val isSelected: Boolean
        get() = cameraApi.selectedCameraDeviceData.value == itemCameraDevice

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        itemIsSelected = isSelected
        itemText = itemCameraDevice?.deviceName
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)
    }

    override fun onSetItemSelected(select: Boolean) {
        super.onSetItemSelected(select)
        if (select) {
            cameraApi.updateSelectedCameraDevice(itemCameraDevice)
        }
    }
}