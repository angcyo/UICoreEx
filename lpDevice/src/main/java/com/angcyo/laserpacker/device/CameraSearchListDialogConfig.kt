package com.angcyo.laserpacker.device

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LifecycleOwner
import com.angcyo.base.dslFHelper
import com.angcyo.bluetooth.fsc.CameraApiModel
import com.angcyo.bluetooth.fsc.CameraDevice
import com.angcyo.bluetooth.fsc.core.WifiDeviceScan
import com.angcyo.core.vmApp
import com.angcyo.dialog.BaseDialogConfig
import com.angcyo.dialog.configBottomDialog
import com.angcyo.dialog.dismissWindow
import com.angcyo.dsladapter.DslAdapter
import com.angcyo.dsladapter.DslAdapterStatusItem
import com.angcyo.dsladapter.annotation.UpdateFlag
import com.angcyo.dsladapter.findItem
import com.angcyo.dsladapter.renderAdapterEmptyStatus
import com.angcyo.dsladapter.toLoading
import com.angcyo.dsladapter.updateAdapterState
import com.angcyo.laserpacker.device.ble.dslitem.CameraDeviceConnectItem
import com.angcyo.library.annotation.DSL
import com.angcyo.library.component.lastActivityCaller
import com.angcyo.library.ex._string
import com.angcyo.library.ex.cancelAnimator
import com.angcyo.library.ex.dpi
import com.angcyo.library.ex.getWifiIP
import com.angcyo.library.ex.getWifiSSID
import com.angcyo.library.ex.infinite
import com.angcyo.library.ex.isDebug
import com.angcyo.library.ex.rotateAnimation
import com.angcyo.viewmodel.observe
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.loading.RadarScanLoadingView
import com.angcyo.widget.span.span

/**
 * 摄像头扫描列表界面
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/05
 */
class CameraSearchListDialogConfig(context: Context? = null) : BaseDialogConfig(context) {

    val cameraApiModel = vmApp<CameraApiModel>()

    init {
        dialogLayoutId = R.layout.dialog_camera_search_list_layout
        dialogTitle = if (isDebug()) {
            span {
                append(_string(R.string.camera))
                val ssid = getWifiSSID()
                getWifiIP()?.let {
                    appendLine()
                    if (ssid.isNullOrEmpty()) {
                        append(it)
                    } else {
                        append("$ssid/$it") {
                            fontSize = 12 * dpi
                        }
                    }
                }
            }
        } else {
            _string(R.string.camera)
        }
    }

    private val cameraAdapter: DslAdapter = DslAdapter().apply {
        renderAdapterEmptyStatus(R.layout.camera_empty_layout) { itemHolder, state ->
            //
        }
    }

    override fun initDialogView(dialog: Dialog, dialogViewHolder: DslViewHolder) {
        super.initDialogView(dialog, dialogViewHolder)

        dialogViewHolder.click(R.id.add_view) {
            dialog.dismissWindow()
            lastActivityCaller?.dslFHelper {
                show(AddCameraFragment::class)
            }
        }

        dialogViewHolder.click(R.id.lib_loading_view) {
            toggleScan(this, dialogViewHolder)
        }

        //发现设备监听
        cameraApiModel.cameraDeviceOnceData.observe(this) { device ->
            device?.let {
                cameraAdapter.render(false) {
                    //移除旧的item
                    val find =
                        findItem(false) { it is CameraDeviceConnectItem && it.itemCameraDevice?.deviceName == device.deviceName }

                    //过滤
                    if (find == null) {
                        //添加新的item
                        renderCameraDeviceItem(device)
                        autoAdapterStatus()
                    } else {
                        (find as CameraDeviceConnectItem).apply {
                            itemCameraDevice = device
                            itemUpdateFlag = true
                        }
                    }
                }
            }
        }

        //扫描状态监听
        cameraApiModel.scanStateOnceData.observe(this, allowBackward = false) { state ->
            if (state != null) {
                updateScanStateLayout(dialogViewHolder)

                //state
                if (state > WifiDeviceScan.STATE_SCAN_START) {
                    cameraAdapter.updateAdapterState()
                }
            }
        }

        dialogViewHolder.rv(R.id.lib_recycler_view)?.adapter = cameraAdapter

        //开始扫描
        val list = cameraApiModel.cameraScanDeviceList
        cameraAdapter.render(false) {
            clearAllItems()
            if (list.isEmpty()) {
                setAdapterStatus(DslAdapterStatusItem.ADAPTER_STATUS_LOADING)
            } else {
                list.forEach {
                    renderCameraDeviceItem(it)
                }
            }
        }
        cameraApiModel.startDiscovery(this)
    }

    override fun onDialogDestroy(dialog: Dialog, dialogViewHolder: DslViewHolder) {
        super.onDialogDestroy(dialog, dialogViewHolder)
        stopScan()
    }

    @UpdateFlag
    fun DslAdapter.renderCameraDeviceItem(device: CameraDevice) {
        CameraDeviceConnectItem()() {
            itemAnimateRes = R.anim.item_translate_to_left_animation
            itemCameraDevice = device
        }
    }

    /**切换扫描状态*/
    fun toggleScan(lifecycleOwner: LifecycleOwner, viewHolder: DslViewHolder) {
        if (cameraApiModel.scanState == WifiDeviceScan.STATE_SCAN_START) {
            cameraApiModel.stopDiscovery()
        } else {
            cameraAdapter.toLoading()
            cameraApiModel.startDiscovery(lifecycleOwner)
        }
    }

    /**停止扫描*/
    fun stopScan() {
        cameraApiModel.stopDiscovery()
    }

    /**更新扫描状态布局*/
    private fun updateScanStateLayout(viewHolder: DslViewHolder) {
        val scan = cameraApiModel.scanState == WifiDeviceScan.STATE_SCAN_START

        //loading
        viewHolder.view(R.id.lib_loading_view)?.apply {
            if (scan) {
                //animate().rotationBy(360f).setDuration(240).start()
                rotateAnimation(duration = 1000, config = {
                    infinite()
                })
            } else {
                cancelAnimator()
            }
        }

        //radar
        viewHolder.v<RadarScanLoadingView>(R.id.radar_scan_loading_view)?.loading(scan)
        viewHolder.visible(R.id.radar_scan_loading_view, scan)
    }

}

/**摄像头搜索列表对话框*/
@DSL
fun Context.cameraSearchListDialog(config: CameraSearchListDialogConfig.() -> Unit) {
    return CameraSearchListDialogConfig(this).run {
        configBottomDialog(this@cameraSearchListDialog)
        // dialogThemeResId = R.style.LibDialogBaseFullTheme
        config()
        show()
    }
}