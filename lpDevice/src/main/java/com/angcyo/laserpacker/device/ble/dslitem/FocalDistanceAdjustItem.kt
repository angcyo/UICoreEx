package com.angcyo.laserpacker.device.ble.dslitem

import androidx.fragment.app.Fragment
import com.angcyo.base.dslFHelper
import com.angcyo.bluetooth.fsc.laserpacker.DeviceStateModel
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerModel
import com.angcyo.core.vmApp
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.dsladapter.item.IFragmentItem
import com.angcyo.laserpacker.device.FocalDistanceAdjustFragment
import com.angcyo.laserpacker.device.R
import com.angcyo.library.toastQQ
import com.angcyo.widget.DslViewHolder

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/08/28
 *
 * 焦距校准item
 */
class FocalDistanceAdjustItem : DslAdapterItem(), IFragmentItem {

    override var itemFragment: Fragment? = null

    init {
        itemLayoutId = R.layout.item_focal_distance_adjust
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)
        itemHolder.click(R.id.start_button) {

            if (vmApp<LaserPeckerModel>().deviceSettingData.value?.free == 1 ||
                vmApp<DeviceStateModel>().deviceStateData.value?.error == 0
            ) {
                //自由模式 或者 并且安全状态
                itemFragment?.dslFHelper {
                    show(FocalDistanceAdjustFragment::class)
                }
            } else {
                toastQQ("It is currently in the non-secure state")
            }
        }
    }
}