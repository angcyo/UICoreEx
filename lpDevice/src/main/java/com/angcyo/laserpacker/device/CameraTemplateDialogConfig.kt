package com.angcyo.laserpacker.device

import android.app.Dialog
import android.content.Context
import com.angcyo.bluetooth.fsc.CameraApiModel
import com.angcyo.bluetooth.fsc.bean.CameraTemplateBean
import com.angcyo.core.vmApp
import com.angcyo.dialog.BaseDialogConfig
import com.angcyo.dialog.configBottomDialog
import com.angcyo.dialog.dismissWindow
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.dsladapter._dslAdapter
import com.angcyo.dsladapter.data.loadDataEnd
import com.angcyo.dsladapter.getAllItemSelectedList
import com.angcyo.dsladapter.loadingStatus
import com.angcyo.dsladapter.onRefreshOrLoadMore
import com.angcyo.item.DslSingleSelectedTextItem
import com.angcyo.library.annotation.DSL
import com.angcyo.library.ex._string
import com.angcyo.library.model.singlePage
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.recycler.renderDslAdapter

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/05
 */
class CameraTemplateDialogConfig(context: Context? = null) : BaseDialogConfig(context) {

    val cameraApiModel = vmApp<CameraApiModel>()

    /**模板确认回调*/
    var onTemplateConfirmAction: ((CameraTemplateBean) -> Unit)? = null

    init {
        dialogLayoutId = R.layout.dialog_camera_template_list_layout
        dialogTitle = _string(R.string.camera_selected_template_title)
    }

    override fun initDialogView(dialog: Dialog, dialogViewHolder: DslViewHolder) {
        super.initDialogView(dialog, dialogViewHolder)

        //确认按钮
        dialogViewHolder.enable(R.id.lib_confirm_view, false)

        dialogViewHolder.click(R.id.lib_confirm_view) {
            //选择模板
            val itemList =
                dialogViewHolder.rv(R.id.lib_recycler_view)?._dslAdapter?.getAllItemSelectedList()
            val first = itemList?.firstOrNull()
            if (first is CameraTemplateItem) {
                dialog.dismissWindow()
                if (first.itemCameraTemplateBean != null) {
                    cameraApiModel.updateSelectedCameraTemplate(first.itemCameraTemplateBean)
                    onTemplateConfirmAction?.invoke(first.itemCameraTemplateBean!!)
                }
            }
        }

        dialogViewHolder.rv(R.id.lib_recycler_view)?.renderDslAdapter {
            onRefreshOrLoadMore { itemHolder, loadMore ->
                //加载模板列表
                cameraApiModel.fetchCameraConfigList { list, error ->
                    //只要结果不为空, 确认按钮就可以点击
                    dialogViewHolder.enable(R.id.lib_confirm_view, !list.isNullOrEmpty())
                    loadDataEnd(CameraTemplateItem::class.java, list, error, singlePage()) {
                        itemCameraTemplateBean = it
                        itemIsSelected = cameraApiModel.selectedCameraTemplateData.value == it
                        observeItemSelectedChange {
                            //更新确认按钮状态
                            val itemList = getAllItemSelectedList()
                            dialogViewHolder.enable(R.id.lib_confirm_view, itemList.isNotEmpty())
                        }
                    }
                }
            }
            loadingStatus()
        }
    }
}

/**item*/
class CameraTemplateItem : DslSingleSelectedTextItem() {

    /**设备信息*/
    var itemCameraTemplateBean: CameraTemplateBean? = null

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        val bean = itemCameraTemplateBean
        itemRightIcon =
            if (bean?.isSystemTemplate == true) R.drawable.canvas_system_template else null
        itemText = if (bean != null) {
            when (bean.name) {
                "Editor.Camera.Template.LP5" -> _string(R.string.camera_template_lp5)
                "Editor.Camera.Template.LP4" -> _string(R.string.camera_template_lp4)
                "Editor.Camera.Template.LP3" -> _string(R.string.camera_template_lp3)
                "Editor.Camera.Template.LP2" -> _string(R.string.camera_template_lp2)
                "Editor.Camera.Template.LP1" -> _string(R.string.camera_template_lp1)
                else -> bean.name ?: itemCameraTemplateBean?.id
            }
        } else {
            "--"
        }
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)
    }

}

/**摄像头模板列表对话框*/
@DSL
fun Context.cameraTemplateDialogConfig(config: CameraTemplateDialogConfig.() -> Unit) {
    return CameraTemplateDialogConfig(this).run {
        configBottomDialog(this@cameraTemplateDialogConfig)
        // dialogThemeResId = R.style.LibDialogBaseFullTheme
        config()
        show()
    }
}