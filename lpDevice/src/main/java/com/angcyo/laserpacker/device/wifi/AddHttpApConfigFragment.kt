package com.angcyo.laserpacker.device.wifi

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.provider.Settings
import com.angcyo.base.dslFHelper
import com.angcyo.bluetooth.fsc.laserpacker.DeviceStateModel
import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper
import com.angcyo.bluetooth.fsc.laserpacker.isCameraPrefix
import com.angcyo.core.fragment.BaseDslFragment
import com.angcyo.core.fragment.bigTitleLayout
import com.angcyo.getData
import com.angcyo.laserpacker.device.R
import com.angcyo.laserpacker.device.wifi.AddWifiConfigFragment.Companion.KEY_IS_CONFIG_AP_DEVICE
import com.angcyo.laserpacker.device.wifi.AddWifiConfigFragment.Companion.KEY_IS_CONFIG_CAMERA_DEVICE
import com.angcyo.library.ex._color
import com.angcyo.library.ex._string
import com.angcyo.library.ex.getWifiSSID
import com.angcyo.library.ex.startIntent
import com.angcyo.library.toastQQ
import com.angcyo.putData

/**
 * A配网, 提示连接Ap界面
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/03/01
 */
class AddHttpApConfigFragment : BaseDslFragment() {

    companion object {
        /**ap名称, 同时也是host.local请求地址*/
        var wifiSsid: String? = null
    }

    /**是否是配置camera网络*/
    var isConfigCameraDevice: Boolean = false

    init {
        fragmentTitle = _string(R.string.add_wifi_device_title)
        fragmentConfig.isLightStyle = true
        fragmentConfig.fragmentBackgroundDrawable =
            ColorDrawable(_color(R.color.lib_theme_white_color))
        bigTitleLayout()

        contentLayoutId = R.layout.layout_add_http_ap_config
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isConfigCameraDevice = getData<Boolean>(KEY_IS_CONFIG_CAMERA_DEVICE) ?: isConfigCameraDevice
    }

    var _isOpenHappend = false

    override fun initBaseView(savedInstanceState: Bundle?) {
        super.initBaseView(savedInstanceState)

        if (isConfigCameraDevice) {
            fragmentTitle = _string(R.string.add_camera_title)
        }

        renderDslAdapter {
            AddHttpDeviceStep1Item()() {
                if (isConfigCameraDevice) {
                    itemTipText = _string(R.string.camera_ap_connect_tip)
                    itemImageRes = R.drawable.camera_ap_connect_step
                } else {
                    itemTipText = _string(R.string.ap_connect_tip2)
                    //itemImageRes = R.mipmap.device_lx2
                    itemImageRes = R.drawable.lx2_ap_connect_step
                }
            }
        }

        _vh.click(R.id.open_ap_button) {
            //打开系统wifi列表
            _isOpenHappend = true
            it.context.startIntent {
                action = Settings.ACTION_WIFI_SETTINGS
            }
        }

        _vh.click(R.id.next_button) {
            onConfirmNext()
        }
    }

    /**确认, 下一步配网*/
    private fun onConfirmNext() {
        //2024-10-14 断开所有连接
        DeviceStateModel.disconnectAllDevice()
        dslFHelper {
            show(AddWifiStateFragment::class) {
                putData(true, KEY_IS_CONFIG_AP_DEVICE)
                putData(isConfigCameraDevice, KEY_IS_CONFIG_CAMERA_DEVICE)
            }
        }
    }

    override fun onFragmentShow(bundle: Bundle?) {
        wifiSsid = getWifiSSID() ?: wifiSsid
        if (_isOpenHappend && isConfigCameraDevice) {
            _isOpenHappend = false
            if (HawkEngraveKeys.lastWifiSSID != wifiSsid) {
                if (wifiSsid?.isCameraPrefix == true) {
                    //是摄像头的wifi ap名称
                } else {
                    toastQQ(_string(R.string.ap_mismatching_tip))
                }
            }
        }
        super.onFragmentShow(bundle)
    }

    override fun onFragmentNotFirstShow(bundle: Bundle?) {
        super.onFragmentNotFirstShow(bundle)
        if (isConfigCameraDevice && wifiSsid?.isCameraPrefix == true) {
            onConfirmNext()
        } else if (LaserPeckerHelper.isHttpDevice(wifiSsid)) {
            onConfirmNext()
        }
    }
}