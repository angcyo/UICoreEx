package com.angcyo.laserpacker.device

import android.content.res.ColorStateList
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.widget.ImageView
import androidx.core.widget.ImageViewCompat
import com.angcyo.base.dslFHelper
import com.angcyo.bluetooth.fsc.laserpacker._deviceSettingBean
import com.angcyo.core.CoreApplication
import com.angcyo.core.appendIconItem
import com.angcyo.core.fragment.BaseDslFragment
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.laserpacker.device.ble.DeviceSettingFragment
import com.angcyo.laserpacker.device.model.FscDeviceModel
import com.angcyo.laserpacker.device.wifi.AddWifiConfigFragment
import com.angcyo.laserpacker.device.wifi.AddWifiConfigFragment.Companion.KEY_IS_CONFIG_CAMERA_DEVICE
import com.angcyo.library.ex._color
import com.angcyo.library.ex._string
import com.angcyo.putData
import com.angcyo.widget.base.clickIt

/**
 * 添加摄像头界面 第一个界面
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/09/05
 */
class AddCameraFragment : BaseDslFragment() {

    init {
        fragmentTitle = _string(R.string.add_camera_title)
        contentOverlayLayoutId = R.layout.layout_overlay_next
        fragmentConfig.isLightStyle = true
        fragmentConfig.showTitleLineView = true

        //2023-8-21 禁用自动连接
        FscDeviceModel.disableAutoConnect()
    }

    override fun onInitFragment(savedInstanceState: Bundle?) {
        fragmentConfig.fragmentBackgroundDrawable =
            ColorDrawable(_color(R.color.lib_theme_white_color))
        super.onInitFragment(savedInstanceState)
    }

    override fun initBaseView(savedInstanceState: Bundle?) {
        super.initBaseView(savedInstanceState)
        _vh.click(R.id.next_button) {
            dslFHelper {
                remove(this@AddCameraFragment)
                show(AddWifiConfigFragment::class) {
                    putData(true, KEY_IS_CONFIG_CAMERA_DEVICE)
                }
            }
        }

        val helpUrl = DeviceSettingFragment.getHelpUrl(
            _deviceSettingBean?.addCameraHelpUrl, _deviceSettingBean?.addCameraHelpUrlZh
        )

        if (!helpUrl.isNullOrBlank()) {
            rightControl()?.appendIconItem {
                ImageViewCompat.setImageTintList(
                    this, ColorStateList.valueOf(fragmentConfig.titleItemIconColor)
                )
                setImageResource(R.drawable.help_svg)
                scaleType = ImageView.ScaleType.CENTER_INSIDE
                clickIt {
                    CoreApplication.onOpenUrlAction?.invoke(helpUrl ?: "")
                }
            }
        }

        renderDslAdapter(true) {
            AddCameraStepItem()()
        }
    }

}

class AddCameraStepItem : DslAdapterItem() {
    init {
        itemLayoutId = R.layout.item_add_camera_step
    }
}