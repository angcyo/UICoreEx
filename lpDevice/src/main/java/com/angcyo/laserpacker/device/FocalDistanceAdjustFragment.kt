package com.angcyo.laserpacker.device

import android.content.res.ColorStateList
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.core.widget.ImageViewCompat
import com.angcyo.base.removeThis
import com.angcyo.bluetooth.fsc.IReceiveBeanAction
import com.angcyo.bluetooth.fsc.ReceivePacket
import com.angcyo.bluetooth.fsc.enqueue
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerModel
import com.angcyo.bluetooth.fsc.laserpacker.command.ExitCmd
import com.angcyo.bluetooth.fsc.laserpacker.parse.QuerySettingParser
import com.angcyo.bluetooth.fsc.laserpacker.parse.isCoverConnect
import com.angcyo.core.appendIconItem
import com.angcyo.core.fragment.BaseDslFragment
import com.angcyo.core.vmApp
import com.angcyo.dialog.messageDialog
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.laserpacker.device.ble.DeviceSettingFragment
import com.angcyo.library.ex.IntAction
import com.angcyo.library.ex._color
import com.angcyo.library.ex._string
import com.angcyo.library.toastQQ
import com.angcyo.widget.DslViewHolder
import com.angcyo.widget.base.clickIt

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/08/28
 *
 * 双红光校准界面
 */
class FocalDistanceAdjustFragment : BaseDslFragment() {

    init {
        fragmentTitle = _string(R.string.focal_distance_adjust_label)
        contentOverlayLayoutId = R.layout.layout_focal_distance_adjust_overlay
        fragmentConfig.isLightStyle = true
        fragmentConfig.showTitleLineView = true
    }

    override fun onCreateBackItem(): View? = null

    override fun onInitFragment(savedInstanceState: Bundle?) {
        fragmentConfig.fragmentBackgroundDrawable =
            ColorDrawable(_color(R.color.lib_theme_white_color))
        super.onInitFragment(savedInstanceState)
    }

    /**步骤共4步
     * 1:
     * 2:
     * 3:
     * 4:
     * */
    var _stepIndex = 1

    /**是否需要控制红光常亮*/
    var _needControlDst = false

    override fun initBaseView(savedInstanceState: Bundle?) {
        super.initBaseView(savedInstanceState)

        val settingParser = vmApp<LaserPeckerModel>().deviceSettingData.value
        _needControlDst = settingParser?.irDst == 0

        rightControl()?.appendIconItem {
            ImageViewCompat.setImageTintList(
                this, ColorStateList.valueOf(fragmentConfig.titleItemIconColor)
            )
            setImageResource(R.drawable.lib_close)
            scaleType = ImageView.ScaleType.CENTER_INSIDE
            clickIt {
                fContext().messageDialog {
                    dialogTitle = _string(R.string.exit_adjust_title)
                    dialogMessage = _string(R.string.exit_focal_distance_adjust_des)
                    positiveButton { dialog, dialogViewHolder ->
                        dialog.dismiss()
                        removeThis()
                    }
                    negativeButton { dialog, dialogViewHolder ->
                        dialog.dismiss()
                    }
                }
            }
        }

        //
        _vh.click(R.id.prev_button) {
            _stepIndex--
            updateStepLayout()
            checkExit()
        }
        updateStepLayout()
    }

    /**双红光校准
     * 根据不同步骤, 渲染不同界面*/
    fun updateStepLayout() {
        //--next
        _vh.enable(R.id.next_button, true)
        _vh.tv(R.id.next_button)?.text = _string(R.string.ui_next)
        _vh.click(R.id.next_button) {
            _stepIndex++
            updateStepLayout()
            checkExit()
        }
        //--
        when (_stepIndex) {
            1 -> {
                _vh.gone(R.id.prev_button)
                _vh.visible(R.id.next_button)

                renderDslAdapter(true) {
                    FocalDistanceAdjustStep1()()
                }
            }

            2 -> {
                _vh.visible(R.id.prev_button)
                _vh.visible(R.id.next_button)

                renderDslAdapter(true) {
                    FocalDistanceAdjustStep1()() {
                        itemLayoutId = R.layout.item_focal_distance_adjust_step2
                        if (isCoverConnect) {
                            itemBindOverride = { itemHolder, _, _, _ ->
                                itemHolder.img(R.id.image_view)
                                    ?.setImageResource(R.drawable.focal_distance_step2_cover)
                                itemHolder.tv(R.id.text_view)?.text =
                                    _string(R.string.focal_distance_adjust_step2_cover)
                            }
                        }
                    }
                }
            }

            3 -> {
                _vh.visible(R.id.prev_button)
                _vh.visible(R.id.next_button)
                _vh.enable(R.id.next_button, _engraveState != null || _isEngrave)

                renderDslAdapter(true) {
                    FocalDistanceAdjustStep1()() {
                        itemLayoutId = R.layout.item_focal_distance_adjust_step3
                        itemStateAction = this@FocalDistanceAdjustFragment::engraveStateAction
                        itemEngraveState = _engraveState
                        itemBindOverride = { itemHolder, _, _, _ ->
                            itemHolder.tv(R.id.state_view)?.text =
                                "${_string(R.string.engrave_state_ing)}..."
                        }
                    }
                }
            }

            4 -> {
                _vh.visible(R.id.prev_button)
                _vh.visible(R.id.next_button)
                _vh.tv(R.id.next_button)?.text = _string(R.string.ui_finish)
                _vh.click(R.id.next_button) {
                    removeThis {
                        remove(DeviceSettingFragment::class)
                    }
                }

                renderDslAdapter(true) {
                    FocalDistanceAdjustStep1()() {
                        itemLayoutId = R.layout.item_focal_distance_adjust_step4
                        if (isCoverConnect) {
                            itemBindOverride = { itemHolder, _, _, _ ->
                                itemHolder.img(R.id.image_view)
                                    ?.setImageResource(R.drawable.focal_distance_step4_cover)
                            }
                        }
                    }
                }
            }
        }
    }

    /**是否雕刻过*/
    var _isEngrave = false

    /**当前雕刻状态*/
    var _engraveState: Int? = null

    /**雕刻状态回调
     * [EngraveModel.ENGRAVE_STATE_FINISH]*/
    fun engraveStateAction(state: Int) {
        _isEngrave = true
        _engraveState = state
        updateStepLayout()
        if (state == 4) {
            //雕刻完成
            /*if (_stepIndex == 3) {
                _stepIndex++
                updateStepLayout()
            }*/
        }
    }

    /**检查是否要发送退出指令*/
    fun checkExit() {
        if (_engraveState != null) {
            ExitCmd().enqueue()
            _engraveState = null
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        ExitCmd().enqueue()
        if (_needControlDst) {
            updateSetting(false)
        }
    }

    override fun onFragmentFirstShow(bundle: Bundle?) {
        super.onFragmentFirstShow(bundle)
        if (_needControlDst) {
            updateSetting(true)
        }
    }

    /**红光常量设置
     * [enableDst] 是否激活红光常亮*/
    fun updateSetting(enableDst: Boolean) {
        val settingParser = vmApp<LaserPeckerModel>().deviceSettingData.value
        settingParser?.functionSetting()
        settingParser?.irDst = if (enableDst) 1 else 0
        settingParser?.updateSetting()
    }

    fun QuerySettingParser.updateSetting(
        action: IReceiveBeanAction = { bean: ReceivePacket?, error: Exception? ->
            error?.let {
                toastQQ(it.message)
                //失败之后, 重新初始化设备状态
                LaserPeckerHelper.initDeviceSetting()
            }
        }
    ) {
        enqueue(action = action)
    }
}

class FocalDistanceAdjustStep1 : DslAdapterItem() {

    companion object {
        /**雕刻双红光校准数据
         * 回调状态通过[IntAction]传递
         * */
        var engraveFocalDistanceAdjustAction: ((IntAction?) -> Unit)? = null
    }

    /**雕刻状态回调*/
    var itemStateAction: IntAction? = null

    /**当前雕刻状态*/
    var itemEngraveState: Int? = null

    init {
        itemLayoutId = R.layout.item_focal_distance_adjust_step1
    }

    override fun onItemBind(
        itemHolder: DslViewHolder,
        itemPosition: Int,
        adapterItem: DslAdapterItem,
        payloads: List<Any>
    ) {
        super.onItemBind(itemHolder, itemPosition, adapterItem, payloads)
        itemHolder.click(R.id.start_button) {
            itemStateAction?.invoke(0)
            engraveFocalDistanceAdjustAction?.invoke(itemStateAction)
        }

        if (itemEngraveState != null) {/*if (itemEngraveState == 0) {
                itemHolder.tv(R.id.state_view)?.text = _string(R.string.transmitting)
            } else {
                itemHolder.tv(R.id.state_view)?.text = _string(R.string.engrave_state_ing)
            }*/
            if (isCoverConnect) {
                itemHolder.img(R.id.image_view)
                    ?.setImageResource(R.drawable.focal_distance_step3_2_cover)
                itemHolder.tv(R.id.text_view)?.text =
                    _string(R.string.focal_distance_adjust_step3_2_cover)
            } else {

                itemHolder.img(R.id.image_view)?.setImageResource(R.drawable.focal_distance_step3_2)
                itemHolder.tv(R.id.text_view)?.text =
                    _string(R.string.focal_distance_adjust_step3_2)
            }
        }

        itemHolder.visible(R.id.state_view, itemEngraveState != null)
        itemHolder.visible(R.id.start_button, itemEngraveState == null)
    }
}