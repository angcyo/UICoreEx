package com.angcyo.laserpacker.device.wifi

import android.Manifest
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import com.angcyo.base.dslFHelper
import com.angcyo.base.hideSoftInput
import com.angcyo.base.removeThis
import com.angcyo.bluetooth.BluetoothModel
import com.angcyo.bluetooth.fsc.laserpacker.DeviceStateModel
import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.core.component.dslPermissions
import com.angcyo.core.fragment.BaseDslFragment
import com.angcyo.core.fragment.bigTitleLayout
import com.angcyo.core.vmApp
import com.angcyo.dsladapter.updateItem
import com.angcyo.dsladapter.updateItemByClass
import com.angcyo.getData
import com.angcyo.getDataParcelable
import com.angcyo.laserpacker.device.BuildConfig
import com.angcyo.laserpacker.device.R
import com.angcyo.laserpacker.device.wifi.dslitem.AddWifiConfigItem
import com.angcyo.laserpacker.device.wifi.dslitem.AddWifiConfigTipItem
import com.angcyo.library.ex._color
import com.angcyo.library.ex._string
import com.angcyo.library.ex.getWifiSSID
import com.angcyo.library.ex.hawkGet
import com.angcyo.library.ex.hawkPut
import com.angcyo.library.ex.isBuildDebug
import com.angcyo.library.toastQQ
import com.angcyo.putData
import com.angcyo.putDataParcelable
import com.clj.fastble.data.BleDevice

/**
 * wifi配网 wifi配置界面
 *
 * Email:angcyo@126.com
 * @author angcyo
 * @date 2023/07/30
 */
class AddWifiConfigFragment : BaseDslFragment() {

    companion object {
        const val KEY_IS_CONFIG_AP_DEVICE = "isConfigApDevice"
        const val KEY_IS_CONFIG_CAMERA_DEVICE = "isConfigCameraDevice"
    }

    /**是否是配置ap网络*/
    var isConfigApDevice: Boolean = false

    /**是否是配置camera网络*/
    var isConfigCameraDevice: Boolean = false

    /**选择的设备*/
    var bleDevice: BleDevice? = null

    init {
        fragmentTitle = _string(R.string.select_wifi_title)
        fragmentConfig.isLightStyle = true
        fragmentConfig.fragmentBackgroundDrawable =
            ColorDrawable(_color(R.color.lib_theme_white_bg_color))
        bigTitleLayout()
        enableSoftInput = true

        contentLayoutId = R.layout.layout_add_wifi_config
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isConfigApDevice = getData<Boolean>(KEY_IS_CONFIG_AP_DEVICE) ?: isConfigApDevice
        isConfigCameraDevice = getData<Boolean>(KEY_IS_CONFIG_CAMERA_DEVICE) ?: isConfigCameraDevice
        if (isConfigApDevice || isConfigCameraDevice) {
        } else {
            bleDevice = getDataParcelable()
            HawkEngraveKeys.forceUseWifi = true
        }
    }

    override fun initBaseView(savedInstanceState: Bundle?) {
        super.initBaseView(savedInstanceState)

        _vh.click(R.id.next_button) {
            _vh.focused(it)

            if (!isConfigApDevice && !isConfigCameraDevice && bleDevice?.name.isNullOrEmpty()) {
                //error
                toastQQ("device name error!")
                dslFHelper {
                    remove(AddWifiDeviceFragment::class)
                    removeThis()
                }
            } else {
                dslFHelper {
                    hideSoftInput()

                    //save info
                    _adapter.get<AddWifiConfigItem>().firstOrNull()?.let { item ->
                        HawkEngraveKeys.lastWifiPassword = item.itemWifiPassword
                        HawkEngraveKeys.lastWifiSSID = item.itemWifiName

                        if (HawkEngraveKeys.lastWifiSSID != null) {
                            //任意wifi都保存一次密码
                            HawkEngraveKeys.lastWifiSSID?.hawkPut(item.itemWifiPassword)
                        }
                    }

                    if (isConfigApDevice) {
                        show(AddHttpApConfigFragment::class)
                    } else if (isConfigCameraDevice) {
                        //2024-10-14 断开所有连接
                        DeviceStateModel.disconnectAllDevice()
                        show(AddHttpApConfigFragment::class) {
                            putData(true, KEY_IS_CONFIG_CAMERA_DEVICE)
                        }
                    } else {
                        show(AddWifiStateFragment::class) {
                            putDataParcelable(
                                WifiConfigBean(
                                    bleDevice!!,
                                    bleDevice!!.name,
                                    HawkEngraveKeys.lastWifiSSID ?: "",
                                    HawkEngraveKeys.lastWifiPassword ?: ""
                                )
                            )
                        }
                    }
                }
            }
        }

        renderDslAdapter(true) {
            if (!isConfigApDevice) {
                //ap 配网支持2.4G/5G
                //其他配网不支持, 所以需要提示仅支持2.4G
                AddWifiConfigTipItem()()
            }
            AddWifiConfigItem()() {
                val ssid = getWifiSSID()
                itemWifiName = ssid
                observeItemChange {
                    _vh.enable(R.id.next_button, !itemWifiName.isNullOrBlank())
                }
                _vh.enable(R.id.next_button, !itemWifiName.isNullOrBlank())
                if (HawkEngraveKeys.rememberWifiPassword) {
                    itemWifiPassword = ssid?.hawkGet<String>() ?: HawkEngraveKeys.lastWifiPassword
                            ?: if (BuildConfig.BUILD_TYPE.isBuildDebug()) "Hi@Yanfa123" else null
                    itemChanging = true
                } else if (BuildConfig.BUILD_TYPE.isBuildDebug()) {
                    itemWifiPassword = "Hi@Yanfa123"
                    itemChanging = true
                }
            }
        }

        if (isConfigApDevice || isConfigCameraDevice) {

        } else if (bleDevice == null) {
            removeThis()
            toastQQ(_string(R.string.core_thread_error_tip))
        }
    }

    /**[com.angcyo.laserpacker.device.ble.BluetoothSearchHelper.Companion.checkAndSearchDevice]*/
    override fun onFragmentFirstShow(bundle: Bundle?) {
        super.onFragmentFirstShow(bundle)
        dslPermissions(listOf(Manifest.permission.ACCESS_WIFI_STATE)) { allGranted, foreverDenied ->
            if (allGranted) {
                updateConfigItem(true)
            }
        }
    }

    val bleModel = vmApp<BluetoothModel>()
    override fun onFragmentShow(bundle: Bundle?) {
        super.onFragmentShow(bundle)
        bleDevice?.let {
            bleModel.disconnect(it)
        }
        updateConfigItem()
    }

    private fun updateConfigItem(updateSsid: Boolean? = null) {
        _adapter.updateItem { it is AddWifiConfigTipItem }
        _adapter.updateItemByClass(AddWifiConfigItem::class)?.apply {
            if (_selectWifiHappened || updateSsid == true) {
                val ssid = getWifiSSID()
                if (!ssid.isNullOrBlank()) {
                    itemWifiPassword = if (ssid != itemWifiName) {
                        //ssid改变后, 清空密码
                        ssid.hawkGet<String>(null)
                    } else {
                        //
                        ssid.hawkGet<String>(itemWifiPassword)
                    }
                    itemWifiName = ssid
                    _vh.enable(R.id.next_button, !itemWifiName.isNullOrBlank())
                }
            }
        }
    }
}