package com.angcyo.laserpacker.device

import android.graphics.RectF
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import com.angcyo.base.removeThis
import com.angcyo.bluetooth.fsc.enqueue
import com.angcyo.bluetooth.fsc.laserpacker.HawkEngraveKeys
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerHelper
import com.angcyo.bluetooth.fsc.laserpacker.LaserPeckerModel
import com.angcyo.bluetooth.fsc.laserpacker._deviceConfigBean
import com.angcyo.bluetooth.fsc.laserpacker.command.ExitCmd
import com.angcyo.core.fragment.BaseDslFragment
import com.angcyo.core.vmApp
import com.angcyo.dsladapter.DslAdapterItem
import com.angcyo.laserpacker.device.ble.DeviceSettingFragment
import com.angcyo.library.ex._color
import com.angcyo.library.ex._string

/**
 * @author <a href="mailto:angcyo@126.com">angcyo</a>
 * @since 2024/08/28
 *
 * 滑台校准帮助界面
 */
class SlideAdjustFragment : BaseDslFragment() {

    init {
        fragmentTitle = _string(R.string.srep_adjust_title)
        contentOverlayLayoutId = R.layout.layout_slide_adjust_overlay
        fragmentConfig.isLightStyle = true
        fragmentConfig.showTitleLineView = true
    }

    override fun onInitFragment(savedInstanceState: Bundle?) {
        fragmentConfig.fragmentBackgroundDrawable =
            ColorDrawable(_color(R.color.lib_theme_white_color))
        super.onInitFragment(savedInstanceState)
    }

    override fun initBaseView(savedInstanceState: Bundle?) {
        super.initBaseView(savedInstanceState)
        _vh.click(R.id.lib_button) {
            removeThis {
                remove(DeviceSettingFragment::class)
            }
        }
        renderDslAdapter {
            SlideAdjustTipItem()()
        }
    }

    override fun onFragmentFirstShow(bundle: Bundle?) {
        super.onFragmentFirstShow(bundle)
        //直接发送预览指令

        val bounds = RectF()
        if (LaserPeckerHelper.loadStringPath(_deviceConfigBean?.sRepAdjustPath, bounds) != null) {
            vmApp<LaserPeckerModel>().sendUpdatePreviewRange(
                bounds,
                bounds,
                null,
                null,
                null,
                HawkEngraveKeys.lastPwrProgress,
                async = true,
                zPause = false
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        ExitCmd().enqueue()
    }
}

class SlideAdjustTipItem : DslAdapterItem() {
    init {
        itemLayoutId = R.layout.item_slide_adjust_tip
    }
}